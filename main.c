//changed on 8th october 2019
//variables static and volatile

//////max+lis+otg authentication in NAND2Gbit
// if (delete_received_flag == 1) {
//      len = 20;
//      ecg_request_received =0;  // stop recording
//     ble_tx_buf[0]='B';
//     ble_tx_buf[1]='U';
//     ble_tx_buf[2]='C';
//     ble_tx_buf[19]='S';
//     ble_nus_data_send(&m_nus, (uint8_t *)ble_tx_buf, &len, m_conn_handle);
//     #ifdef DEBUG
//     NRF_LOG_INFO("deactivate acknowledgement sent\r\n");
//     #endif
//      delete_data();   // delete data
//      ble_tx_buf[0] = 'B';
//      ble_tx_buf[1] = 'U';
//      ble_tx_buf[2] = 'D';
//      ble_tx_buf[3] = 'O';
//      ble_tx_buf[19] = 'S';
//      ble_nus_data_send(&m_nus, (uint8_t *)ble_tx_buf, &len, m_conn_handle);
//      delete_received_flag = 0;
//    }
////////////////////////////////////////////////////////////battery
//  level = (((float)adc_cnt - 600) * 100 / 182);
////////////////////////////////////////////////////////////saadc
//channel_config.gain = NRF_SAADC_GAIN1_3;
////////////////////////////////////////////////////////////usb_rc_check()
//crc_ack_wait_count=0;

/**
 * Copyright (c) 2017 - 2018, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
 *
 * @defgroup usbd_ble_uart_example main.c
 * @{
 * @ingroup  usbd_ble_uart_example
 * @brief    USBD CDC ACM over BLE application main file.
 *
 * This file contains the source code for a sample application that uses the Nordic UART service
 * and USBD CDC ACM library.
 * This application uses the @ref srvlib_conn_params module.
 */

#include "app_timer.h"
#include "app_uart.h"
#include "app_util_platform.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "ble_hci.h"
#include "ble_nus.h"
#include "bsp_btn_ble.h"
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_ble_gatt.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include <stdint.h>
#include <string.h>

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "nrf_delay.h"
#include "nrf_drv_clock.h"
#include "nrf_drv_power.h"
#include "nrf_drv_usbd.h"
#include "nrf_gpio.h"

#include "app_error.h"
#include "app_usbd.h"
#include "app_usbd_cdc_acm.h"
#include "app_usbd_core.h"
#include "app_usbd_serial_num.h"
#include "app_usbd_string_desc.h"
#include "app_util.h"

#include "boards.h"
#include "fds.h"
#include "math.h"
#include "nrf_drv_gpiote.h"
#include "nrf_drv_ppi.h"
#include "nrf_drv_rtc.h"
#include "nrf_drv_saadc.h"
#include "nrf_drv_spi.h"
#include "nrf_drv_timer.h"
#include "nrf_pwr_mgmt.h"

#include "diskio.h"
#include "ff.h"
#include "nand.h"
///////////////////////////////////////////////////////////////////////////////////////////////////////// nrf functions and definitions
/**
 * @brief App timer handler for blinking the LEDs.
 *
 * @param p_context LED to blink.
 */

#define ENDLINE_STRING "\r\n"

#define CDC_ACM_COMM_INTERFACE 0
#define CDC_ACM_COMM_EPIN NRF_DRV_USBD_EPIN2

#define CDC_ACM_DATA_INTERFACE 1
#define CDC_ACM_DATA_EPIN NRF_DRV_USBD_EPIN1
#define CDC_ACM_DATA_EPOUT NRF_DRV_USBD_EPOUT1

static char m_cdc_data_array[30];

static void cdc_acm_user_ev_handler(app_usbd_class_inst_t const *p_inst,
    app_usbd_cdc_acm_user_event_t event);
/** @brief CDC_ACM class instance */
APP_USBD_CDC_ACM_GLOBAL_DEF(m_app_cdc_acm,
    cdc_acm_user_ev_handler,
    CDC_ACM_COMM_INTERFACE,
    CDC_ACM_DATA_INTERFACE,
    CDC_ACM_COMM_EPIN,
    CDC_ACM_DATA_EPIN,
    CDC_ACM_DATA_EPOUT,
    APP_USBD_CDC_COMM_PROTOCOL_AT_V250);

// USB DEFINES END

// BLE DEFINES START
#define APP_BLE_CONN_CFG_TAG 1 /**< A tag identifying the SoftDevice BLE configuration. */

#define APP_FEATURE_NOT_SUPPORTED BLE_GATT_STATUS_ATTERR_APP_BEGIN + 2 /**< Reply when unsupported features are requested. */

#define DEVICE_NAME "Biocalculus"                        /**< Name of device. Will be included in the advertising data. */
#define NUS_SERVICE_UUID_TYPE BLE_UUID_TYPE_VENDOR_BEGIN /**< UUID type for the Nordic UART Service (vendor specific). */

#define APP_BLE_OBSERVER_PRIO 3 /**< Application's BLE observer priority. You shouldn't need to modify this value. */

#define APP_ADV_INTERVAL 128   /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */
#define APP_ADV_DURATION 10000 /**< The advertising duration (180 seconds) in units of 10 milliseconds.1000 */

#define MIN_CONN_INTERVAL MSEC_TO_UNITS(10, UNIT_1_25_MS)    /**< Minimum acceptable connection interval (20 ms). Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL MSEC_TO_UNITS(50, UNIT_1_25_MS)    /**< Maximum acceptable connection interval (75 ms). Connection interval uses 1.25 ms units. */
#define SLAVE_LATENCY 0                                      /**< Slave latency. */
#define SLAVE_LATENCY 0                                      /**< Slave latency. */
#define CONN_SUP_TIMEOUT MSEC_TO_UNITS(4000, UNIT_10_MS)     /**< Connection supervisory timeout (4 seconds). Supervision Timeout uses 10 ms units. */
#define FIRST_CONN_PARAMS_UPDATE_DELAY APP_TIMER_TICKS(5000) /**< Time from initiating an event (connect or start of notification) to the first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY APP_TIMER_TICKS(30000) /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT 3                       /**< Number of attempts before giving up the connection parameter negotiation. */

#define DEAD_BEEF 0xDEADBEEF /**< Value used as error code on stack dump. Can be used to identify stack location on stack unwind. */

#define UART_TX_BUF_SIZE 256 /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE 256 /**< UART RX buffer size. */
#define SAADC_SAMPLES_IN_BUFFER 2

APP_TIMER_DEF(m_sensor_id);
BLE_NUS_DEF(m_nus, NRF_SDH_BLE_TOTAL_LINK_COUNT);                      /**< BLE NUS service instance. */
NRF_BLE_GATT_DEF(m_gatt);                                              /**< GATT module instance. */
BLE_ADVERTISING_DEF(m_advertising);                                    /**< Advertising module instance. */
                                                                       /**< Handle of the current connection. */
static uint16_t m_ble_nus_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - 3; /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */
static ble_uuid_t m_adv_uuids[] =                                      /**< Universally unique service identifier. */
    {
        {BLE_UUID_NUS_SERVICE, NUS_SERVICE_UUID_TYPE}};
static char m_nus_data_array[BLE_NUS_MAX_DATA_LEN];
uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID;
static const nrf_drv_timer_t m_timer = NRF_DRV_TIMER_INSTANCE(1);

////////////////////////////////////////////////////////////////////////////////// nrf functions and definitions

////////////////////////////////////////////// subscription and authentication
// defines
#define NREGISTERED 0
#define REGISTERED 1
#define FULL 2
///otg
#define OTG_PCKT_RPT_COUNT 6 // number of times device sends packet and waits for acknowledgment

#define CRC16 0x8005
uint16_t crc16(const unsigned char *buffer, uint32_t length);
uint16_t gen_crc16(const uint8_t *data, uint16_t size);
uint32_t total_sample_size(void);
uint8_t usb_crc_check(void);
uint8_t send_be(void);
uint8_t send_file_terminate(void);
///otg
///////////////////////////////////////////////////////// flash
#define NUMBER_OF_DATA_FILES 400
#define FLASH_READ_BYTES 4096
#define FILE_DISCARD_SIZE 20000
volatile uint8_t number_of_files = 0;
static char const *fs_file_name[] =
    {
        "1.txt",
        "2.txt",
        "3.txt",
        "4.txt",
        "5.txt",
        "6.txt",
        "7.txt",
        "8.txt",
        "9.txt",
        "10.txt",
        "11.txt",
        "12.txt",
        "13.txt",
        "14.txt",
        "15.txt",
        "16.txt",
        "17.txt",
        "18.txt",
        "19.txt",
        "20.txt",
        "21.txt",
        "22.txt",
        "23.txt",
        "24.txt",
        "25.txt",
        "26.txt",
        "27.txt",
        "28.txt",
        "29.txt",
        "30.txt",
        "31.txt",
        "32.txt",
        "33.txt",
        "34.txt",
        "35.txt",
        "36.txt",
        "37.txt",
        "38.txt",
        "39.txt",
        "40.txt",
        "41.txt",
        "42.txt",
        "43.txt",
        "44.txt",
        "45.txt",
        "46.txt",
        "47.txt",
        "48.txt",
        "49.txt",
        "50.txt",
        "51.txt",
        "52.txt",
        "53.txt",
        "54.txt",
        "55.txt",
        "56.txt",
        "57.txt",
        "58.txt",
        "59.txt",
        "60.txt",
        "61.txt",
        "62.txt",
        "63.txt",
        "64.txt",
        "65.txt",
        "66.txt",
        "67.txt",
        "68.txt",
        "69.txt",
        "70.txt",
        "71.txt",
        "72.txt",
        "73.txt",
        "74.txt",
        "75.txt",
        "76.txt",
        "77.txt",
        "78.txt",
        "79.txt",
        "80.txt",
        "81.txt",
        "82.txt",
        "83.txt",
        "84.txt",
        "85.txt",
        "86.txt",
        "87.txt",
        "88.txt",
        "89.txt",
        "90.txt",
        "91.txt",
        "92.txt",
        "93.txt",
        "94.txt",
        "95.txt",
        "96.txt",
        "97.txt",
        "98.txt",
        "99.txt",
        "100.txt",
        "101.txt",
        "102.txt",
        "103.txt",
        "104.txt",
        "105.txt",
        "106.txt",
        "107.txt",
        "108.txt",
        "109.txt",
        "110.txt",
        "111.txt",
        "112.txt",
        "113.txt",
        "114.txt",
        "115.txt",
        "116.txt",
        "117.txt",
        "118.txt",
        "119.txt",
        "120.txt",
        "121.txt",
        "122.txt",
        "123.txt",
        "124.txt",
        "125.txt",
        "126.txt",
        "127.txt",
        "128.txt",
        "129.txt",
        "130.txt",
        "131.txt",
        "132.txt",
        "133.txt",
        "134.txt",
        "135.txt",
        "136.txt",
        "137.txt",
        "138.txt",
        "139.txt",
        
        "140.txt",
        "141.txt",
        "142.txt",
        "143.txt",
        "144.txt",
        "145.txt",
        "146.txt",
        "147.txt",
        "148.txt",
        "149.txt",
        "150.txt",
        "151.txt",
        "152.txt",
        "153.txt",
        "154.txt",
        "155.txt",
        "156.txt",
        "157.txt",
        "158.txt",
        "159.txt",
        "160.txt",
        "161.txt",
        "162.txt",
        "163.txt",
        "164.txt",
        "165.txt",
        "166.txt",
        "167.txt",
        "168.txt",
        "169.txt",
        "170.txt",
        "171.txt",
        "172.txt",
        "173.txt",
        "174.txt",
        "175.txt",
        "176.txt",
        "177.txt",
        "178.txt",
        "179.txt",
        "180.txt",
        "181.txt",
        "182.txt",
        "183.txt",
        "184.txt",
        "185.txt",
        "186.txt",
        "187.txt",
        "188.txt",
        "189.txt",
        "190.txt",
        "191.txt",
        "192.txt",
        "193.txt",
        "194.txt",
        "195.txt",
        "196.txt",
        "197.txt",
        "198.txt",
        "199.txt",
        "200.txt",
        "201.txt",
        "202.txt",
        "203.txt",
        "204.txt",
        "205.txt",
        "206.txt",
        "207.txt",
        "208.txt",
        "209.txt",
        "210.txt",
        "211.txt",
        "212.txt",
        "213.txt",
        "214.txt",
        "215.txt",
        "216.txt",
        "217.txt",
        "218.txt",
        "219.txt",
        "220.txt",
        "221.txt",
        "222.txt",
        "223.txt",
        "224.txt",
        "225.txt",
        "226.txt",
        "227.txt",
        "228.txt",
        "229.txt",
        "230.txt",
        "231.txt",
        "232.txt",
        "233.txt",
        "234.txt",
        "235.txt",
        "236.txt",
        "237.txt",
        "238.txt",
        "239.txt",
        "240.txt",
        "241.txt",
        "242.txt",
        "243.txt",
        "244.txt",
        "245.txt",
        "246.txt",
        "247.txt",
        "248.txt",
        "249.txt",
        "250.txt",
        "251.txt",
        "252.txt",
        "253.txt",
        "254.txt",
        "255.txt",
        "256.txt",
        "257.txt",
        "258.txt",
        "259.txt",
        "260.txt",
        "261.txt",
        "262.txt",
        "263.txt",
        "264.txt",
        "265.txt",
        "266.txt",
        "267.txt",
        "268.txt",
        "269.txt",
        "270.txt",
        "271.txt",
        "272.txt",
        "273.txt",
        "274.txt",
        "275.txt",
        "276.txt",
        "277.txt",
        "278.txt",
        "279.txt",
        "280.txt",
        "281.txt",
        "282.txt",
        "283.txt",
        "284.txt",
        "285.txt",
        "286.txt",
        "287.txt",
        "288.txt",
        "289.txt",
        "290.txt",
        "291.txt",
        "292.txt",
        "293.txt",
        "294.txt",
        "295.txt",
        "296.txt",
        "297.txt",
        "298.txt",
        "299.txt",
        "300.txt",
        "301.txt",
        "302.txt",
        "303.txt",
        "304.txt",
        "305.txt",
        "306.txt",
        "307.txt",
        "308.txt",
        "309.txt",
        "310.txt",
        "311.txt",
        "312.txt",
        "313.txt",
        "314.txt",
        "315.txt",
        "316.txt",
        "317.txt",
        "318.txt",
        "319.txt",
        "320.txt",
        "321.txt",
        "322.txt",
        "323.txt",
        "324.txt",
        "325.txt",
        "326.txt",
        "327.txt",
        "328.txt",
        "329.txt",
        "330.txt",
        "331.txt",
        "332.txt",
        "333.txt",
        "334.txt",
        "335.txt",
        "336.txt",
        "337.txt",
        "338.txt",
        "339.txt",
        "340.txt",
        "341.txt",
        "342.txt",
        "343.txt",
        "344.txt",
        "345.txt",
        "346.txt",
        "347.txt",
        "348.txt",
        "349.txt",
        "350.txt",
        "351.txt",
        "352.txt",
        "353.txt",
        "354.txt",
        "355.txt",
        "356.txt",
        "357.txt",
        "358.txt",
        "359.txt",
        "360.txt",
        "361.txt",
        "362.txt",
        "363.txt",
        "364.txt",
        "365.txt",
        "366.txt",
        "367.txt",
        "368.txt",
        "369.txt",
        "370.txt",
        "371.txt",
        "372.txt",
        "373.txt",
        "374.txt",
        "375.txt",
        "376.txt",
        "377.txt",
        "378.txt",
        "379.txt",
        "380.txt",
        "381.txt",
        "382.txt",
        "383.txt",
        "384.txt",
        "385.txt",
        "386.txt",
        "387.txt",
        "388.txt",
        "389.txt",
        "390.txt",
        "391.txt",
        "392.txt",
        "393.txt",
        "394.txt",
        "395.txt",
        "396.txt",
        "397.txt",
        "398.txt",
        "399.txt",
        "400.txt",

};
volatile uint8_t fs_file_num = 0;
volatile uint16_t fs_file_num_record = 0;
uint8_t save_user_nand(void);
static uint8_t check_user_nand(void);
uint8_t check_date_nand(void);
uint8_t save_date_nand(void);
void store_sps_subscription(void);
uint8_t delete_data(void);
uint8_t delete_all_data(void);
uint8_t find_file_number(void);
void set_otg_upload_status(uint8_t status);
///////////////////////////////////////////////////////// flash

// function prototype
static void response_calc(const uint8_t *p_key, const uint8_t *p_nonce, uint8_t *p_response);
static uint8_t calculate_difference(void);
static uint8_t check_user(void);
uint8_t save_user(void);
uint32_t delete_user(uint8_t userNum);
uint8_t check_time(void);
uint8_t save_date(void);
#define gpio_1_8 NRF_GPIO_PIN_MAP(1, 8)
#define gpio_1_9 NRF_GPIO_PIN_MAP(1, 9)
#define gpio_1_12 NRF_GPIO_PIN_MAP(1, 12)
#define gpio_1_14 NRF_GPIO_PIN_MAP(1, 14)
#define gpio_1_15 NRF_GPIO_PIN_MAP(1, 15)
#define gpio_1_1 NRF_GPIO_PIN_MAP(1, 1)
#define gpio_1_2 NRF_GPIO_PIN_MAP(1, 2)
#define gpio_1_3 NRF_GPIO_PIN_MAP(1, 3)
#define gpio_1_4 NRF_GPIO_PIN_MAP(1, 4)
// variables

///////////////////////////////////////////////////////bluetooth
volatile uint8_t lis_working_flag = 0;
volatile uint8_t max_working_flag = 0;
volatile uint8_t flash_working_flag = 0;
volatile uint8_t test_request_flag = 0;
///////////////////////////////////////////////////////bluetooth
static uint16_t user_number = 0;
static uint16_t number = 0;
static uint8_t appDay = 0;
static uint8_t appMonth = 0;
static uint8_t appYear = 0;
static uint8_t day_saved = 0;
static uint8_t month_saved = 0;
static uint8_t year_saved = 0;
volatile uint8_t leap_year_flag = 0;
volatile uint8_t user_status = 0;

volatile uint8_t check_user_flag = 0;
volatile uint8_t date_success_flag = 0;
volatile uint8_t delete_received_flag = 0;
volatile uint8_t delete_on_progress_flag = 0;

static uint8_t appPkey[16] = {0xf8, 0x1b, 0xe1, 0x34, 0xea, 0x83, 0x89, 0x87, 0xe0, 0xff, 0x77, 0xca, 0x05, 0xfb, 0xfa, 0x6f}; // dummy key
static uint8_t cloudActPkey[16] = {0xdd, 0xd1, 0x39, 0xae, 0xda, 0x81, 0x32, 0x51, 0x07, 0x56, 0xc0, 0x2c, 0x8c, 0xbf, 0xbe, 0xed};
static uint8_t cloudDeactPkey[16] = {0xdd, 0xd1, 0x39, 0xae, 0xda, 0x81, 0x32, 0x51, 0x07, 0x56, 0xc0, 0x2c, 0x8c, 0xbf, 0xbe, 0xed};
static uint8_t usb_key[16] = {0xed, 0xd1, 0x50, 0xae, 0xaa, 0x81, 0x02, 0x51, 0x07, 0x56, 0x20, 0x7c, 0x8c, 0xbf, 0x8e, 0xed};
static uint8_t data_buffer[750];
static uint16_t rand_val;
static uint16_t rand_val_high;
static uint16_t rand_val_low;
static uint8_t aes_data_in[16];
static uint8_t aes_data_out[16];
static uint8_t aes_received[16];
static uint8_t user_id[16];
static uint8_t set_max_sps = 0;
static uint8_t set_sps_subscription[3];
static uint32_t subscription_date = 0;
volatile uint8_t user_registered = 0;
volatile uint8_t cipher_error_flag = 0;
volatile uint8_t cloud_cipher_error_flag = 0;
volatile uint8_t app_cipher_error_flag = 0;
volatile uint8_t app_cipher_error_flag_usb = 0;

volatile uint8_t cloud_cipher_received_flag = 0;
volatile uint8_t cipher_received_flag = 0;
volatile uint8_t packet_flag = 0;
volatile uint8_t app_cipher_received_flag = 0;
volatile uint8_t app_cipher_received_flag_usb = 0;
volatile uint8_t cloud_deactive_cipher_received_flag = 0;
volatile uint8_t aesCheckFlag = 0;
volatile uint16_t aes_success_flag = 0;
volatile uint8_t app_aes_success_flag = 0;
volatile uint8_t cloud_aes_success_flag = 0;
volatile uint8_t cloud_aes_flag = 0;
volatile uint8_t dateCheckFlag = 0;
volatile uint8_t record_time_check_success = 0;

////////////////////////////////////////////// subscription and authentication

//////////////////////////////////////////////////////////////////////////////////////////////////// AFE
const nrf_drv_spi_t spi_max = NRF_DRV_SPI_INSTANCE(2);
static uint32_t ecg_flash_buf_count = 0;
static uint32_t ecg_ble_buf_count = 0;
static uint8_t ecg_ble_buf_0[20];    // split ecg_ble_tx to four 20 bytes data
static uint8_t ecg_ble_buf_1[20];    //
static uint8_t ecg_ble_buf_2[20];    //
static uint8_t ecg_ble_buf_3[20];    //
static uint8_t send_packet_flag = 0; // set this when send buffer gets full
static uint8_t ecg_flash_buf[255];   // buffer which holds ecg values to store into flash
static uint8_t ecg_ble_tx[255];      // buffer holds data for sending over BLE
static uint8_t spi_setup_max_flag = 0;
volatile uint8_t ecg_lead_off_flag = 0;
volatile uint8_t ecg_lead_on_flag = 0;
volatile uint8_t lead_off_record_stop_flag = 0;
volatile uint8_t ecg_lead_on_status_flag = 0;
volatile uint8_t max30003init_flag = 0;
volatile uint8_t ecg_view_flag = 0;
volatile bool ecg_live_view_flag = false;
volatile bool ecg_start_recording_flag = false; ///quick
volatile uint8_t ecg_flag = 0;

void Max30003_Wreg(uint8_t reg, uint32_t data);
uint32_t Max30003_Rreg(int reg);
void Max30003Setup(void);
void read_ecg_params(void);
void max_clear_fifo(void);
void led_blink(uint32_t pin_number1, uint32_t pin_number2, uint32_t times);

/////////////////////////////////////////////////////////////////////////////////////////////////////// AFE
////////////////////////////////////////////////////////////////// LIS

const nrf_drv_spi_t spi_lis = NRF_DRV_SPI_INSTANCE(1);
//#define SPI_CS_PIN_LIS 20 //20

#define OUT_X 0x29 // output in x axis
#define OUT_Y 0x2B // output in y axis
#define OUT_Z 0x2D // output in z axis
// CONTROL REGISTERS
#define CTRL_REG1 0x20 //control registers
#define CTRL_REG2 0x21 //control registers
#define CTRL_REG3 0x22 //control registers
#define CTRL_REG4 0x23 //control registers
#define CTRL_REG5 0x24 //control registers
#define CTRL_REG6 0x25 //control registers
#define REFERENCE 0x26 //control registers
// INTERRUPT 1 REGISTER
#define IG1_CFG 0x30
#define IG1_SOURCE 0x31
#define IG1_THS 0x32
#define IG1_DURATION 0x33
// CLICK FUNCTION REGISTERS
#define CLICK_CFG 0x38
#define CLICK_SRC 0x39
#define CLICK_THS 0x3A

#define TIME_LIMIT 0x3B
#define TIME_LATENCY 0x3C
#define TIME_WINDOW 0x3D
#define RREG 0x80
#define WREG 0x00

// activity calculation
#define REST 0
#define LOW 1
#define MODERATE 2
#define INTENSE 3
/*
function prototypes
*/
void calibrate_LIS(void);
void lis_setup(void);
void lis_read_xyz(void);
int lis_rreg(int reg);
void lis_wreg(int reg, int val);
float lis_filt(float x);
/*
variables
*/
static uint16_t lis_read_xyz_flag = 0;
static float x_avg;
static float y_avg;
static float z_avg;
static uint32_t x_value;
static uint32_t y_value;
static uint32_t z_value;
static float movement;
static uint32_t timeout_count;
static uint8_t activity_mode;

//////////////////////////////////////////////////////////////// LIS

//////////////////////////////////////////////////////// RTC
static uint8_t update_date_flag = 0;
static uint8_t record_day = 0;
static uint8_t record_month = 0;
static uint8_t record_year = 0;
void update_date(void);

const nrf_drv_rtc_t rtc = NRF_DRV_RTC_INSTANCE(2);
#define ADVERTISE_AFTER_LEAD_ON_SECONDS 2
#define DISCONNECT_AFTER_START_RECORD_SECONDS 5
volatile bool ble_advertise_flag = false;
static bool led_seq_flag = false;
static bool disconnect_after_recording_flag = false;
volatile bool ble_adv_stop_flag = false;
static uint32_t ble_advertise_count = 0;
static uint32_t disconnect_after_recording_count = 0;
static uint32_t blink_speed = 0;
static uint32_t turn_off_device_count = 0;
#define SECONDS_TO_POWER_OFF_WITHOUT_ADVERTISING 30
static bool turn_off_device_flag = false;
static uint32_t led_indic_cnt = 0;
#define LED_ADVERTISE_SECONDS 1
#define BLINK_SPEED_0 300
#define BLINK_SPEED_1 200
#define BLINK_SPEED_2 150
#define BLINK_SPEED_3 100
#define BLINK_SPEED_4 60
#define BLINK_SPEED_5 10
#define BLINK_SPEED_BLE 20

/////////////////////////////////////////////////////////////////////LISTEST
#define LIS_CLK NRF_GPIO_PIN_MAP(1, 5)
#define LIS_MISO NRF_GPIO_PIN_MAP(1, 7)
//#define LIS_MOSI NRF_GPIO_PIN_MAP(1, 8)
#define LIS_MOSI 26
#define LIS_VCC NRF_GPIO_PIN_MAP(1, 6)
#define SPI_CS_PIN_LIS 27 //20LISCS
/////////////////////////////////////////////////////////////////////LISTEST

/////////////////////////////////////////////MAXTEST_NEW

#define PG NRF_GPIO_PIN_MAP(1, 10)
#define BAT_STATUS NRF_GPIO_PIN_MAP(1, 13)

#define MAX_CLK 22 //18
#define MAX_MISO 20
#define MAX_MOSI 19
#define MAX_INT1 17
#define MAX_INT2 21
#define SPI_CS_PIN_MAX 24 //22 //16
#define LED_PIN_BLUE 23
#define LED_PIN_GREEN 13
#define LED_PIN_RED 15

/////////////////////////////////////////////MAXTEST_NEW
///////////////////////////////////////////////MAXTEST

//#define PG NRF_GPIO_PIN_MAP(1, 10)
//#define MAX_CLK 18//23//18
//#define MAX_MISO 20
//#define MAX_MOSI 19
//#define MAX_INT1 17
//#define MAX_INT2 21
//#define SPI_CS_PIN_MAX 22 //16
//#define LED_PIN 23
///////////////////////////////////////////////MAXTEST

static bool led_on_flag = true;
static bool otg_led_on_flag = true;
static bool delete_led_on_flag = true;
static bool led_off_flag = false;
static bool delete_led_off_flag = true;
static bool otg_led_off_flag = false;
static bool lead_led_off_flag = false;
static bool led_blink_on_flag = false;
static bool led_blink_off_flag = false;
static bool initial_flag = false;
static bool red_led_flag = false;
static uint32_t lead_led_on_count = 0;
static uint32_t lead_led_off_count = 0;
static uint32_t lead_record_led_count = 0;

static uint32_t led_on_count = 0;
static uint32_t delete_led_on_count = 0;
static uint32_t otg_led_on_count = 0;
static uint32_t led_off_count = 0;
static uint32_t delete_led_off_count = 0;
static uint32_t otg_led_off_count = 0;
static uint32_t led_blink_on_count = 0;
static uint32_t led_blink_off_count = 0;
static uint32_t cnt = 0;
volatile uint16_t rtc_counter = 0;

static uint32_t hour_counter = 0;
#define HOUR_COUNT_MAX_THRESHOLD 30000
#define ONE_HOUR_COUNT 28800

volatile uint8_t rtc_flag = 0;
static uint32_t rtc_hrs;
static uint32_t rtc_mins;
static uint32_t rtc_secs;
#define COMPARE_COUNTERTIME (3UL)

///////////////////////////////////////////////////////////////// RTC
//////////////////////////////////////////////////////////////////////////////////////////////////////////////FDS
#define MAX_NVM_STORAGE_SIZE 20

volatile bool fds_evt_success;

#define FLASH_CONFIG_FILE_ID (uint16_t)(0xF010)
#define FLASH_CONFIG_USER_NUM (uint16_t)(0x7010) // number of users
#define FLASH_CONFIG_USER_ID (uint16_t)(0x7020)  //// user id
#define FLASH_CONFIG_DATE (uint16_t)(0x7030)     // date
#define FLASH_CONFIG_REC_TIME (uint16_t)(0x7040) // date

void flash_write_init(void);
void flash_reset(void);
void flash_update(uint16_t configFileId, uint16_t configKey, uint8_t *ptr, uint8_t len);
static void flash_write(uint16_t configFileId, uint16_t configKey, uint8_t *ptr, uint8_t len);
uint8_t flash_read(uint16_t configFileId, uint16_t configKey, uint8_t len);
static char fds_read_buf[MAX_NVM_STORAGE_SIZE];
static char fds_write_buf[MAX_NVM_STORAGE_SIZE];

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////FDS

///////////////////////////////////////////////////////////////// NAND Flash
#define NAND_POWER NRF_GPIO_PIN_MAP(1, 0)
volatile uint32_t store_to_flash_flag = 0;
volatile uint32_t sample_counter = 0;
//volatile uint8_t fs_data_buf[3000];
static uint8_t fs_data_buf[3000];
volatile uint32_t fs_buf_count;
volatile uint32_t ecg_sample_size = 0;
volatile uint32_t one_hour_sample = 0;
#define ONE_HOUR_SAMPLE_COUNT_250 900000
#define ONE_HOUR_SAMPLE_COUNT_500 1800000
#define DEFAULT_MAX_SPS 1 // 250:1, 500:0
#define DEFAULT_SUBSCRIPTION_DAYS 7

volatile bool f_open_flag = false;
volatile bool f_write_flag = false;
volatile bool flash_storage_full_flag = false;
volatile uint8_t open_next_file_flag = 0;

///////////////////////////////////////////////////////////////// NAND Flash

////////////////////////////////////////////////////////////////////////// OTG
volatile bool m_usb_connected = false;
volatile uint16_t usb_tx_done_flag = 0;
volatile uint16_t check_user_flag_OTG = 0;
volatile uint32_t crc_acknowledgment_flag = 0;
volatile uint32_t crc_ack_wait_count = 0;
///////////////////////////////////////////////////////////////////////// OTG

///////////////////////////////////////////////////////////////////////// BLE Tx/Rx
#define ECG_MODE 1
#define SLEEP_MODE 3
#define SEND_NOTHING 0
volatile uint8_t ble_tx_buf[30];
volatile bool otg_data_collection_flag = false;
static uint16_t otg_data_upload_flag = 0;
volatile uint16_t ecg_request_received = 0;
volatile uint8_t imei_received_flag = 0;
volatile uint16_t send_RR_and_Steps_flag = 0;
volatile uint8_t device_mode = 0;
uint16_t rr_value = 0;
volatile uint8_t date_received_flag = 0;
volatile uint8_t test_flag = 0;

/////////////////////////////////////////////////////////////////////////BLE Tx/Rx
//0b0000000001000000
/////////////////////////////////////////////////////////////////////////////////NEW

static uint16_t mcp_status = 0;
static bool red_led_on = false;
static bool green_led_on = false;
static bool red_led_on_flag = true;
static bool red_led_off_flag = false;
static bool red_led_blink_on_flag = false;
static bool red_led_blink_off_flag = false;
static bool otg_led_blink_flag = false;
static bool deactivation_led_flag = false;
static bool lead_led_on_flag = true;
volatile bool lead_led_turn_on_flag = false;
static uint32_t red_led_on_count = 0;
static uint32_t red_led_off_count = 0;
static uint32_t red_led_blink_on_count = 0;
static uint32_t red_led_blink_off_count = 0;
#define THRESHOLD_LED 200
#define BATTERY_CHANNEL 0
#define LED_CHANNEL 1

/////////////////////////////////////////////////////////////////////////////////NEW
//#define SAMPLES_IN_BUFFER 5

static uint16_t saadc_value_count;
static uint16_t perc_led = 0;

static uint16_t saadc_irq_count = 0;
static uint16_t irq_cnt = 0;
static uint16_t saadc_value_count_temp = 0;

//static const nrf_drv_spi_t spi_flash = NRF_DRV_SPI_INSTANCE(0); /**< SPI instance. */

static char m_nus_data_array[BLE_NUS_MAX_DATA_LEN];
static uint32_t saadc_value_count_acc = 0;
static uint32_t saadc_value_count_acc_led = 0;

//uint8_t temp = 0;
static nrf_saadc_value_t m_buffer_pool[2][SAADC_SAMPLES_IN_BUFFER];
static nrf_ppi_channel_t m_ppi_channel;
volatile uint8_t read_ecg_param_flag = 0;

uint32_t rr_step_send_counter = 0;
uint8_t m_rx_buf[200];

////////////////////////////////////////// ble connection flags
volatile uint8_t ble_connected_flag = 0;
////////////////////////////////////////// ble connection flags

static void gpio_uninit(void);
static void application_timers_start(void);

static uint16_t len = 20;
#define WALKING '2'
//#define REST '1'
#define RUNNING '3'
static float movement_max_value;

volatile bool transfer_success_flag;
uint8_t battery_level(uint16_t adc_cnt);

static void power_manage(void) {
  uint32_t err_code = sd_app_evt_wait();
  APP_ERROR_CHECK(err_code);
}

/**
 * @brief Function for assert macro callback.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of an assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num    Line number of the failing ASSERT call.
 * @param[in] p_file_name File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t *p_file_name) {
  app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

static void application_timers_start(void) {
  uint32_t err_code;
  err_code = app_timer_start(m_sensor_id,
      APP_TIMER_TICKS(100), NULL);
  APP_ERROR_CHECK(err_code);
}
/** @brief Function for initializing the timer module. */

void timer_handler(nrf_timer_event_t event_type, void *p_context) {
}

void saadc_sampling_event_init(void) {
  ret_code_t err_code;

  err_code = nrf_drv_ppi_init();
  APP_ERROR_CHECK(err_code);

  nrf_drv_timer_config_t timer_cfg = NRF_DRV_TIMER_DEFAULT_CONFIG;
  timer_cfg.bit_width = NRF_TIMER_BIT_WIDTH_32;
  err_code = nrf_drv_timer_init(&m_timer, &timer_cfg, timer_handler);
  APP_ERROR_CHECK(err_code);
  uint32_t ticks = nrf_drv_timer_ms_to_ticks(&m_timer, 40);
  /* setup m_timer for compare event every 400ms */
  nrf_drv_timer_extended_compare(&m_timer,
      NRF_TIMER_CC_CHANNEL0,
      ticks,
      NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK,
      false);
  nrf_drv_timer_enable(&m_timer);

  uint32_t timer_compare_event_addr = nrf_drv_timer_compare_event_address_get(&m_timer,
      NRF_TIMER_CC_CHANNEL0);
  uint32_t saadc_sample_task_addr = nrf_drv_saadc_sample_task_get();

  /* setup ppi channel so that timer compare event is triggering sample task in SAADC */
  err_code = nrf_drv_ppi_channel_alloc(&m_ppi_channel);
  APP_ERROR_CHECK(err_code);

  err_code = nrf_drv_ppi_channel_assign(m_ppi_channel,
      timer_compare_event_addr,
      saadc_sample_task_addr);
  APP_ERROR_CHECK(err_code);
}

void saadc_sampling_event_enable(void) {
  ret_code_t err_code = nrf_drv_ppi_channel_enable(m_ppi_channel);

  APP_ERROR_CHECK(err_code);
}

void saadc_callback(nrf_drv_saadc_evt_t const *p_event) {
  if (p_event->type == NRF_DRV_SAADC_EVT_DONE) {

    {
      ret_code_t err_code;
      uint8_t value[SAADC_SAMPLES_IN_BUFFER * 2];
      uint8_t bytes_to_send;
      err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer,
          SAADC_SAMPLES_IN_BUFFER);
      APP_ERROR_CHECK(err_code);

      ///////////////////////////////////////////////////////////////////////////////NEW
      //      mcp_status = p_event->data.done.p_buffer[LED_CHANNEL];
      //      saadc_value_count_acc_led += (uint32_t)mcp_status;
      //      irq_cnt++;
      //      if ((irq_cnt > 100) && (m_usb_connected == true)) {
      //        perc_led = saadc_value_count_acc_led / irq_cnt;
      //    #ifdef DEBUG
      //        NRF_LOG_INFO("perc led= %d\r\n", perc_led);
      //    #endif
      //
      //        if ((perc_led > THRESHOLD_LED) && (m_usb_connected == true))  {
      //          green_led_on = true;
      //        } else {
      //          red_led_on = true;
      //        }
      //        saadc_value_count_acc_led = 0;
      //        irq_cnt = 0;
      //        perc_led = 0;
      //        mcp_status = 0;
      //      }

      ///////////////////////////////////////////////////////////////////////////////NEW

      saadc_value_count_temp = p_event->data.done.p_buffer[BATTERY_CHANNEL];
      saadc_value_count_acc += (uint32_t)saadc_value_count_temp;
      saadc_irq_count++;

      if (saadc_irq_count > 100) {
        saadc_value_count = saadc_value_count_acc / saadc_irq_count;
        uint8_t bs_temp = battery_level(saadc_value_count); //ssssssssssssss
        if ((bs_temp < 100) && (m_usb_connected == true)) {
          red_led_on = true;
#ifdef DEBUG
          NRF_LOG_INFO("RED LED ON");
#endif
        } else if ((bs_temp == 100) && (m_usb_connected == true)) {
          green_led_on = true;
#ifdef DEBUG
          NRF_LOG_INFO("GREEN LED ON");
#endif
        }
#ifdef DEBUG
        NRF_LOG_INFO("battery= %d\r\n", bs_temp);
#endif
        if (bs_temp > 90)
          blink_speed = BLINK_SPEED_4;
        else if (bs_temp > 70)
          blink_speed = BLINK_SPEED_3;
        else if (bs_temp > 50)
          blink_speed = BLINK_SPEED_2;
        else if (bs_temp > 30)
          blink_speed = BLINK_SPEED_1;
        else if (bs_temp > 10)
          blink_speed = BLINK_SPEED_0;
        else
          blink_speed = BLINK_SPEED_5;
        saadc_irq_count = 0;
        saadc_value_count_acc = 0;
      }
    }
  }
}

void saadc_init(void) {
  ret_code_t err_code;
  nrf_drv_saadc_config_t saadc_config = NRF_DRV_SAADC_DEFAULT_CONFIG;
  saadc_config.resolution = NRF_SAADC_RESOLUTION_12BIT;

  nrf_saadc_channel_config_t channel_config_battery =
      NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN1);
  channel_config_battery.gain = NRF_SAADC_GAIN1_3;

  ///////////////////////////////////////////////////////////////////////////////////NEW
  //  nrf_saadc_channel_config_t channel_config_led =
  //      NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN7);
  //  channel_config_led.gain = NRF_SAADC_GAIN1_3;
  ///////////////////////////////////////////////////////////////////////////////////NEW

  err_code = nrf_drv_saadc_init(NULL, saadc_callback);
  APP_ERROR_CHECK(err_code);

  err_code = nrf_drv_saadc_channel_init(BATTERY_CHANNEL, &channel_config_battery);
  APP_ERROR_CHECK(err_code);

  ///////////////////////////////////////////////////////////////////////////////////NEW
  //  err_code = nrf_drv_saadc_channel_init(LED_CHANNEL, &channel_config_led);
  //  APP_ERROR_CHECK(err_code);
  ///////////////////////////////////////////////////////////////////////////////////NEW

  err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[0], SAADC_SAMPLES_IN_BUFFER);
  APP_ERROR_CHECK(err_code);

  err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[1], SAADC_SAMPLES_IN_BUFFER);
  APP_ERROR_CHECK(err_code);
}

//////////////////////////////////////////////////////////////////////////////////
////////////////////        flash code end           /////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
uint8_t save_user(void) {
  static uint8_t user_count = 0;
  static uint32_t vvv = 0;
  flash_read(FLASH_CONFIG_FILE_ID, FLASH_CONFIG_USER_NUM, 1);

  if (fds_read_buf[0] == 4) { //to check whether the user count exceeds 4. If YES return 0.

    return 0;
  }
  if (fds_read_buf[0] == 0 || fds_read_buf[0] == 0xff) { // save to user id of 1st user and update user count to 1

    for (uint8_t i = 0; i < 6; i++)
      fds_write_buf[i] = user_id[i];

    flash_update(FLASH_CONFIG_FILE_ID, FLASH_CONFIG_USER_ID, fds_write_buf, 6);

    //update user count to 1;
    fds_write_buf[0] = 1;
    flash_update(FLASH_CONFIG_FILE_ID, FLASH_CONFIG_USER_NUM, fds_write_buf, 1);

    user_number = 1;
    return 1;
  }
  return 1;
  if (fds_read_buf[0] == 1) { // save to user id of 2nd user and update user count to 2

    for (uint8_t i = 0; i < 6; i++)
      fds_write_buf[i] = user_id[i];

    flash_update(FLASH_CONFIG_FILE_ID, FLASH_CONFIG_USER_ID, fds_write_buf, 6);

    //update user count to 1;
    fds_write_buf[0] = 2;
    //   flash_update(FLASH_CONFIG_FILE_ID, FLASH_CONFIG_USER_NUM, fds_write_buf, 1);
    user_number = 2;
    return 1;
  }

  if (fds_read_buf[0] == 2) { // save to user id of 2nd user and update user count to 2

    for (uint8_t i = 0; i < 6; i++)
      fds_write_buf[i] = user_id[i];

    flash_update(FLASH_CONFIG_FILE_ID, FLASH_CONFIG_USER_ID, fds_write_buf, 6);

    //update user count to 1;
    fds_write_buf[0] = 3;
    // flash_update(FLASH_CONFIG_FILE_ID, FLASH_CONFIG_USER_NUM, fds_write_buf, 1);
    user_number = 3;
    return 1;
  }

  if (fds_read_buf[0] == 3) { // save to user id of 4th user and update user count to 2
    for (uint8_t i = 0; i < 6; i++)
      fds_write_buf[i] = user_id[i];

    flash_update(FLASH_CONFIG_FILE_ID, FLASH_CONFIG_USER_ID, fds_write_buf, 6);

    //update user count to 1;
    fds_write_buf[0] = 4;
    // flash_update(FLASH_CONFIG_FILE_ID, FLASH_CONFIG_USER_NUM, fds_write_buf, 1);
    user_number = 4;
    return 1;
  }

  return 0; //return 1 if user number is less than 4
}

uint8_t save_user_nand(void) {
  static uint8_t user_count = 0;
  static FIL file;
  uint32_t bw = 0;
  char read_str[20];
  char write_str[20];
  FRESULT ff_result;

#ifdef DEBUG
  NRF_LOG_INFO("save user id info to nand\r\n");
#endif
  ff_result = f_open(&file, "user.txt", FA_READ);
  if (ff_result == FR_OK)
    ff_result = f_read(&file, read_str, 20, &bw); // read 174 bytes of data from file
  ff_result = f_close(&file);
  ff_result = f_lseek(&file, 0);
#ifdef DEBUG
  NRF_LOG_INFO("save user id, read nand= %s\r\n", read_str);
#endif
  /*
  if (fds_read_buf[0] == 4) { //to check whether the user count exceeds 4. If YES return 0.

    return 0;
  }
  */
  if (read_str[0] == 0xff) { // save to user id of 1st user and update user count to 1
    for (uint32_t i = 0; i < 20; i++)
      write_str[i] = read_str[i]; // copying all data read from flash to write_str
    for (uint32_t i = 0; i < 6; i++) {
      write_str[i + 1] = user_id[i]; // replacing user id
    }
    write_str[0] = 1; // updating user number
#ifdef DEBUG
    NRF_LOG_INFO("save user id, write nand= %s\r\n", write_str);
#endif

    ff_result = f_unlink("user.txt");
    ff_result = f_open(&file, "user.txt", FA_CREATE_NEW | FA_WRITE);
    ff_result = f_write(&file, write_str, 20, &bw);
    ff_result = f_close(&file);
#ifdef DEBUG
    NRF_LOG_INFO("save user id, ff_result= %d\r\n", ff_result);
#endif
    user_number = 1;
    return 1;
  } else
    return 0;
  return 0;
}

static uint8_t check_user_nand(void) {
  static uint8_t user_count = 0;
  user_number = 0;
  static FIL file;
  uint32_t bw = 0;
  char read_str[20];
  char write_str[20];
  FRESULT ff_result;

  // no users registered. send response with no user registered command
  ff_result = f_open(&file, "user.txt", FA_READ);
  ff_result = f_read(&file, read_str, 20, &bw); // read 174 bytes of data from file
  ff_result = f_close(&file);
  ff_result = f_lseek(&file, 0);

  if (read_str[0] == 0 || read_str[0] == 0xff) {
    return NREGISTERED;
  }

  if (read_str[1] == user_id[0] && read_str[2] == user_id[1] && read_str[3] == user_id[2] && read_str[4] == user_id[3] && read_str[5] == user_id[4] && read_str[6] == user_id[5]) {
    user_number = 1;
    if (read_str[19] == 'Y') {
      otg_data_upload_flag = 1;
    } else {
      otg_data_upload_flag = 0;
    }
    return REGISTERED;
  }
  ///////////////////////////////////////////////change-Oct23
  if (read_str[0] == 1) {
    user_count = 1;
    return FULL;
  }
  ///////////////////////////////////////////////change-Oct23

  //check the userid which is read match with 2nd user's id

  //check the userid which is read match with 3rd user's id

  //check the userid which is read match with 4th user's id
  return NREGISTERED;
}

static uint8_t check_user(void) {
  static uint8_t user_count = 0;
  user_number = 0;

  // no users registered. send response with no user registered command
  flash_read(FLASH_CONFIG_FILE_ID, FLASH_CONFIG_USER_NUM, 1);
  if (fds_read_buf[0] == 0 || fds_read_buf[0] == 0xff) {
    return NREGISTERED;
  }
  if (fds_read_buf[0] == 4) {
    user_count = 4;
    return FULL;
  }

  //check the userid which is read match with 1st user's id
  flash_read(FLASH_CONFIG_FILE_ID, FLASH_CONFIG_USER_ID, 6);
  if (fds_read_buf[0] == user_id[0] && fds_read_buf[1] == user_id[1] && fds_read_buf[2] == user_id[2] && fds_read_buf[3] == user_id[3] && fds_read_buf[4] == user_id[4] && fds_read_buf[5] == user_id[5]) {
    user_number = 1;
    return REGISTERED;
  }
  //check the userid which is read match with 2nd user's id

  flash_read(FLASH_CONFIG_FILE_ID, FLASH_CONFIG_USER_ID, 6);
  if (fds_read_buf[0] == user_id[0] && fds_read_buf[1] == user_id[1] && fds_read_buf[2] == user_id[2] && fds_read_buf[3] == user_id[3] && fds_read_buf[4] == user_id[4] && fds_read_buf[5] == user_id[5]) {
    user_number = 2;
    return REGISTERED;
  }
  //check the userid which is read match with 3rd user's id

  flash_read(FLASH_CONFIG_FILE_ID, FLASH_CONFIG_USER_ID, 6);
  if (fds_read_buf[0] == user_id[0] && fds_read_buf[1] == user_id[1] && fds_read_buf[2] == user_id[2] && fds_read_buf[3] == user_id[3] && fds_read_buf[4] == user_id[4] && fds_read_buf[5] == user_id[5]) {
    user_number = 3;
    return REGISTERED;
  }
  //check the userid which is read match with 4th user's id

  flash_read(FLASH_CONFIG_FILE_ID, FLASH_CONFIG_USER_ID, 6);
  if (fds_read_buf[0] == user_id[0] && fds_read_buf[1] == user_id[1] && fds_read_buf[2] == user_id[2] && fds_read_buf[3] == user_id[3] && fds_read_buf[4] == user_id[4] && fds_read_buf[5] == user_id[5]) {
    user_number = 4;
    return REGISTERED;
  }

  if (user_count == 4)
    return FULL;
  else
    return NREGISTERED;
}

uint8_t save_date(void) {

  static uint8_t dateArray[16];

  fds_write_buf[0] = appDay;
  fds_write_buf[1] = appMonth;
  fds_write_buf[2] = appYear;
  /*
  if (user_number == 0) { //if there is no user function returns 0
    return 0;
  }
  if (user_number == 1) {56825268
//    col_address_userdate = 6; //assign column address of first user
  } else if (user_number == 2) {
 //   col_address_userdate = 15; //assign column address of second user
  } else if (user_number == 3) {
//    col_address_userdate = 24; //assign column address of third user
  } else if (user_number == 4) {
 //   col_address_userdate = 33; //assign column address of fourth user
  } else
    return 0;
    */
  flash_update(FLASH_CONFIG_FILE_ID, FLASH_CONFIG_DATE, fds_write_buf, 3);
  return 1;
}
uint8_t save_record_date_time_nand(void) { //;;;;;;;;;

  static uint8_t user_count = 0;
  static FIL file;
  uint32_t bw = 0;
  char read_str[20];
  char write_str[20];
  FRESULT ff_result;

  ff_result = f_open(&file, "user.txt", FA_READ);
  ff_result = f_read(&file, read_str, 20, &bw); // read 174 bytes of data from file
  ff_result = f_close(&file);
  ff_result = f_lseek(&file, 0);
  if (read_str[19] == 'N') {
#ifdef DEBUG
    NRF_LOG_INFO("file upload status=N\r\n");
#endif
  } else {
#ifdef DEBUG
    NRF_LOG_INFO("file upload status=%d\r\n", read_str[19]);
#endif

    for (uint32_t i = 0; i < 20; i++) {
      write_str[i] = read_str[i];
    }
    write_str[10] = rtc_hrs;
    write_str[11] = rtc_mins;
    write_str[12] = rtc_secs;

    write_str[16] = appDay;
    write_str[17] = appMonth;
    write_str[18] = appYear;

    ff_result = f_unlink("user.txt");
    ff_result = f_open(&file, "user.txt", FA_CREATE_NEW | FA_WRITE);
    ff_result = f_write(&file, write_str, 20, &bw);
    ff_result = f_close(&file);
    set_otg_upload_status(0);
  }
  return 1;
}

uint8_t save_date_nand(void) {

  static uint8_t user_count = 0;
  static FIL file;
  uint32_t bw = 0;
  char read_str[20];
  char write_str[20];
  FRESULT ff_result;
#ifdef DEBUG
  NRF_LOG_INFO("saving date info to nand\r\n");
#endif

  ff_result = f_open(&file, "user.txt", FA_READ);
  ff_result = f_read(&file, read_str, 20, &bw); // read 20 bytes of data from file
  ff_result = f_close(&file);
  ff_result = f_lseek(&file, 0);
#ifdef DEBUG
  NRF_LOG_INFO("date received, read from flash= %s\r\n", read_str);
#endif
  for (uint32_t i = 0; i < 20; i++) {
    write_str[i] = read_str[i];
  }

  write_str[7] = appDay;
  write_str[8] = appMonth;
  write_str[9] = appYear;
#ifdef DEBUG
  NRF_LOG_INFO("date received, write to flash= %s\r\n", write_str);
#endif

  ff_result = f_unlink("user.txt");
  ff_result = f_open(&file, "user.txt", FA_CREATE_NEW | FA_WRITE);
  ff_result = f_write(&file, write_str, 20, &bw);
  ff_result = f_close(&file);
#ifdef DEBUG
  NRF_LOG_INFO("date received, ff_result= %d\r\n", ff_result);
#endif

  return 1;
}
uint8_t check_time(void) {
  static uint8_t date_diff;
  static uint16_t date;
  date_diff = 0;
  /*
  if (user_number == 0) { //checks if there exists old users,if NO returns 0.
    return 0;
  }

  if (user_number == 1) {
   // col_address_userdate = 6; //if user number is 1, column address is assigned 6
  } else if (user_number == 2) {
  //  col_address_userdate = 15; //if user number is 2, column address is assigned 15
  } else if (user_number == 3) {
  //  col_address_userdate = 24; //if user number is 3, column address is assigned 24
  } else if (user_number == 4) {
  //  col_address_userdate = 33; //if user number is 4, column address is assigned 33
  } else
    return 0;
    */

  flash_read(FLASH_CONFIG_FILE_ID, FLASH_CONFIG_DATE, 3);

  // page_read_data(block_address_user, page_address_user, col_address_userdate, 3); //read the stored date,month and year
  day_saved = fds_read_buf[0];
  month_saved = fds_read_buf[1];
  year_saved = fds_read_buf[2];
  date_diff = calculate_difference();  //find date difference
  if (date_diff > subscription_date) { //if the date exceeds the allowed limit(here 31 days),return 0.
    return 0;
  } else
    return 1;
}

uint8_t check_date_nand(void) {
  static uint8_t date_diff;
  static uint16_t date;
  date_diff = 0;
  static FIL file;
  uint32_t bw = 0;
  char read_str[20];
  char write_str[20];
  FRESULT ff_result;
#ifdef DEBUG
  NRF_LOG_INFO("check subscription\r\n");
#endif

  ff_result = f_open(&file, "user.txt", FA_READ);
  ff_result = f_read(&file, read_str, 20, &bw); // read 174 bytes of data from file
  ff_result = f_close(&file);
  ff_result = f_lseek(&file, 0);
#ifdef DEBUG
  NRF_LOG_INFO("check subscription, read from flash= %s\r\n", read_str);
#endif
  if (read_str[0] == 0xff)
    return 0;

  // page_read_data(block_address_user, page_address_user, col_address_userdate, 3); //read the stored date,month and year
  day_saved = read_str[7];
  month_saved = read_str[8];
  year_saved = read_str[9];
#ifdef DEBUG
  NRF_LOG_INFO("check subscription, day saved=%d, month=%d, year=%d\r\n", day_saved, month_saved, year_saved);
#endif

  date_diff = calculate_difference(); //find date difference
#ifdef DEBUG
  NRF_LOG_INFO("check subscription, difference=%d\r\n", date_diff);
#endif

  if (date_diff > subscription_date) { //if the date exceeds the allowed limit(here 31 days),return 0.
#ifdef DEBUG
    NRF_LOG_INFO(" subscription expired\r\n");
#endif

    return 0;
  } else {
#ifdef DEBUG
    NRF_LOG_INFO(" subscription ok\r\n");
#endif

    return 1;
  }
}

static uint8_t calculate_difference(void) {
  if (appYear == year_saved) {
    if ((appYear + 2000) % 4 == 0) {
      leap_year_flag = 1;

    } else {
      leap_year_flag = 0;
    }
    if (month_saved == appMonth) // if same month. simply calculate the difference
    {

      return (appDay - day_saved);
    } else if (appMonth == (month_saved + 1)) // if one month difference
    {

      if (month_saved == 2 && leap_year_flag == 1) {
        return ((appDay + 29) - day_saved);

      } else if (month_saved == 2 && leap_year_flag == 0) {

        return ((appDay + 28) - day_saved);
      }

      if (month_saved == 1 || month_saved == 3 || month_saved == 5 || month_saved == 7 || month_saved == 8 || month_saved == 10 || month_saved == 12) {

        return ((appDay + 31) - day_saved);
      }
      if (month_saved == 4 || month_saved == 6 || month_saved == 9 || month_saved == 11) {

        return ((appDay + 30) - day_saved);
      }
    } else if (appMonth == (month_saved + 2)) // if two months difference
    {
      if ((month_saved == 1 || month_saved == 2) && leap_year_flag == 1) {

        return ((appDay + 31 + 29) - day_saved);
      } else if ((month_saved == 1 || month_saved == 2) && leap_year_flag == 0) {

        return ((appDay + 31 + 28) - day_saved);
      }
      if (month_saved == 7) {

        return ((appDay + 31 + 31) - day_saved);
      } else if (month_saved == 3 || month_saved == 4 || month_saved == 5 || month_saved == 6 || month_saved == 8 || month_saved == 9 || month_saved == 10) {

        return ((appDay + 31 + 30) - day_saved);
      }
    }

  } else if (appYear == year_saved + 1) {
    if (month_saved == 12) {

      return (appDay + 31 - day_saved);
    } else {

      return 100;
    }
  } else {

    return 100;
  }

  return 100;
}

static void response_calc(const uint8_t *p_key, const uint8_t *p_nonce, uint8_t *p_response) {
  uint32_t err_code;

  nrf_ecb_hal_data_t m_ecb_data;
  uint8_t key[16] = {0xf8, 0x1b, 0xe1, 0x34, 0xea, 0x83, 0x89, 0x87, 0xe0, 0xff, 0x77, 0xca, 0x05, 0xfb, 0xfa, 0x6f}; // impossible2crack,waferchips(key)

  memcpy(&m_ecb_data.key[0], p_key, SOC_ECB_KEY_LENGTH);
  memcpy(&m_ecb_data.cleartext[0], p_nonce, SOC_ECB_KEY_LENGTH);

  err_code = sd_ecb_block_encrypt(&m_ecb_data);

  memcpy(p_response, &m_ecb_data.ciphertext[0], SOC_ECB_KEY_LENGTH);
  err_code = 0;
}
/**
 * @brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of
 *          the device. It also sets the permissions and appearance.
 */
static void gap_params_init(void) {
  uint32_t err_code;
  ble_gap_conn_params_t gap_conn_params;
  ble_gap_conn_sec_mode_t sec_mode;

  BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

  err_code = sd_ble_gap_device_name_set(&sec_mode,
      (const uint8_t *)DEVICE_NAME,
      strlen(DEVICE_NAME));
  APP_ERROR_CHECK(err_code);

  memset(&gap_conn_params, 0, sizeof(gap_conn_params));

  gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
  gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
  gap_conn_params.slave_latency = SLAVE_LATENCY;
  gap_conn_params.conn_sup_timeout = CONN_SUP_TIMEOUT;

  err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
  APP_ERROR_CHECK(err_code);
}

static void sleep_mode_enter(void) {
  ble_advertise_flag = false;
  nrf_gpio_pin_clear(LED_PIN_BLUE);  //Turn off blue indication
  nrf_gpio_pin_clear(LED_PIN_GREEN); //Turn off blue indication
  nrf_gpio_pin_clear(LED_PIN_RED);   //Turn off blue indication
                                     // gpio_uninit();
  uint32_t err_code = sd_power_system_off();
  APP_ERROR_CHECK(err_code);
}
void spi_event_handler_max(nrf_drv_spi_evt_t const *p_event,
    void *p_context) {
  transfer_success_flag = true;
}
void spi_setup_max(void) {
  nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
  spi_config.ss_pin = NULL;
  spi_config.miso_pin = MAX_MOSI; //13;19
  spi_config.mosi_pin = MAX_MISO; //14;20
  spi_config.sck_pin = MAX_CLK;   //15;18
  ret_code_t uc = nrf_drv_spi_init(&spi_max, &spi_config, spi_event_handler_max, NULL);
}

/**
 * @brief Function for putting the chip into sleep mode.
 *
 * @note This function does not return.
 */

/** @brief Function for starting advertising. */
static void advertising_start(void) {
  uint32_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
  APP_ERROR_CHECK(err_code);
}
/**
 * @brief Function for handling the data from the Nordic UART Service.
 *
 * @details This function processes the data received from the Nordic UART BLE Service and sends
 *          it to the USBD CDC ACM module.
 *
 * @param[in] p_evt Nordic UART Service event.
 */
static void nus_data_handler(ble_nus_evt_t *p_evt) { //1 ticks

  if (p_evt->type == BLE_NUS_EVT_RX_DATA) {
    memcpy(m_nus_data_array, p_evt->params.rx_data.p_data, p_evt->params.rx_data.length);

    static uint8_t tempI = 0;

    if (m_nus_data_array[0] == 'B' && m_nus_data_array[19] == 'S') { //check whether the packet starts with 'B' and end with 'S'

      if (m_nus_data_array[1] == 'I') {
        for (tempI = 0; tempI < 6; tempI++) {
          user_id[tempI] = m_nus_data_array[tempI + 2]; // saving 15 byte user id(imei number)
        }
        set_sps_subscription[0] = m_nus_data_array[8]; //ssssssssss
        set_sps_subscription[1] = m_nus_data_array[9];
        set_sps_subscription[2] = m_nus_data_array[10];
#ifdef DEBUG
        NRF_LOG_INFO("max sps=%d\r\n", set_sps_subscription[0]);
#endif

        for (tempI = 6; tempI < 16; tempI++) {
          user_id[tempI] = '0';
        }
        check_user_flag = 1;
        NRF_LOG_INFO("CHECK_USER_FLAG =1");
      } else if (m_nus_data_array[1] == 'C' && imei_received_flag == 1) // packet for authentication
      {
        imei_received_flag = 0;
        if (m_nus_data_array[2] == 'A') // encrypted value from android application
        {

          for (tempI = 0; tempI < 16; tempI++)
            aes_received[tempI] = m_nus_data_array[3 + tempI];
          for (tempI = 0; tempI < 14; tempI++)
            aes_data_in[tempI] = user_id[tempI];             // copying IMEI value
          aes_data_in[14] = rand_val_high;                   // replacing  with MSB of random value
          aes_data_in[15] = rand_val_low;                    // replacing with LSB of random value
          response_calc(appPkey, aes_data_in, aes_data_out); //getting AES encrypted value for the imei number and random value
          for (tempI = 0; tempI < 16; tempI++) {
            if (aes_received[tempI] != aes_data_out[tempI]) {
              app_cipher_error_flag = 1; //check whether the received and encrypted values are same.
              NRF_LOG_INFO("APP_CIPHER_ERROR_FLAG =1");
              break;
            }
          }
          app_cipher_received_flag = 1;
          NRF_LOG_INFO("APP_CIPHER_RECIEVED_FLAG =1");
        } else if (m_nus_data_array[2] == 'C') // checking activation of device: authenticating encrypted value from cloud  5 ticks
        {
          for (tempI = 0; tempI < 16; tempI++)
            aes_received[tempI] = m_nus_data_array[3 + tempI];
          for (tempI = 0; tempI < 15; tempI++)
            aes_data_in[tempI] = user_id[tempI];                  // copying IMEI value
          aes_data_in[0] = rand_val_high;                         // replacing  with MSB of random value
          aes_data_in[15] = rand_val_low;                         // replacing with LSB of random value
          response_calc(cloudActPkey, aes_data_in, aes_data_out); //getting AES encrypted value for the imei number and random value
          for (tempI = 0; tempI < 16; tempI++) {
            if (aes_received[tempI] != aes_data_out[tempI]) {
              cloud_cipher_error_flag = 1; //check whether the recieved and encrypted values are same.
              NRF_LOG_INFO("cloud_cipher_error_flag =1");
              break;
            }
          }
          cloud_cipher_received_flag = 1;
          NRF_LOG_INFO("cloud_cipher_received_flag =1");

        } else if (m_nus_data_array[2] == 'X') // checking activation of device: authenticating encrypted value from cloud
        {
          for (tempI = 0; tempI < 16; tempI++)
            aes_received[tempI] = m_nus_data_array[3 + tempI];
          for (tempI = 0; tempI < 15; tempI++)
            aes_data_in[tempI] = user_id[tempI];                  // copying IMEI value
          aes_data_in[0] = rand_val_high;                         // replacing  with MSB of random value
          aes_data_in[15] = rand_val_low;                         // replacing with LSB of random value
          response_calc(cloudActPkey, aes_data_in, aes_data_out); //getting AES encrypted value for the imei number and random value
          for (tempI = 0; tempI < 16; tempI++) {
            if (aes_received[tempI] != aes_data_out[tempI]) {
              cloud_cipher_error_flag = 1; //check whether the recieved and encrypted values are same.
              break;
            }
          }
          cloud_deactive_cipher_received_flag = 1;
        }
      } else if (m_nus_data_array[1] == 'D') // packet with date
      {

        test_flag = 1;
        appDay = m_nus_data_array[2]; //getting date,month and year
        appMonth = m_nus_data_array[3];
        appYear = m_nus_data_array[4];
        record_year = appYear;
        record_month = appMonth;
        record_day = appDay;

      }

      else if (m_nus_data_array[1] == 'T' && dateCheckFlag == 1) // packet with time
      {
        rtc_hrs = m_nus_data_array[2]; //getting seconds,minutes, and hours
        rtc_mins = m_nus_data_array[3];
        rtc_secs = m_nus_data_array[4];
        rtc_flag = 1;
        date_success_flag = 1;
      } else if (m_nus_data_array[1] == 'S' && dateCheckFlag == 1) // packet for viewing ECG graph
      {
        ecg_live_view_flag = true;

      } else if (m_nus_data_array[1] == 'R' && dateCheckFlag == 1) // packet for ECG data request
      {
        if (ecg_request_received == 0) {
          ecg_start_recording_flag = true;
        }
      } else if (m_nus_data_array[1] == 'P' && dateCheckFlag == 1) // packet for stopping the ECG viewing
      {
        ecg_live_view_flag = false;
        ecg_view_flag = 0;
      } else if (m_nus_data_array[1] == 'X' && dateCheckFlag == 1) // packet for stop recording
      {
        ecg_request_received = 0;
        ecg_start_recording_flag = false;
      } else if ((m_nus_data_array[1] == 'F' && dateCheckFlag == 1)) {
        sleep_mode_enter();
      } else if ((m_nus_data_array[1] == 'U' && m_nus_data_array[2] == 'D' && dateCheckFlag == 1)) {
        delete_received_flag = 1;
      }
    }

    else if (m_nus_data_array[0] == 'E' || m_nus_data_array[0] == 'e') { //to recieve ECG values
      if (ecg_request_received == 0)
        ecg_start_recording_flag = true;
    } else if (m_nus_data_array[0] == 'S' || m_nus_data_array[0] == 's') { //to make the device to enter SLEEP MODE
      ecg_live_view_flag = true;
    } else if (m_nus_data_array[0] == 'F' || m_nus_data_array[0] == 'f') { //to turn OFF the device
      sleep_mode_enter();
    } else if (m_nus_data_array[0] == 'g' || m_nus_data_array[0] == 'G') { //to turn OFF the device
      ecg_view_flag = 0;
      ecg_request_received = 0;
      delete_data();
    } else if (m_nus_data_array[0] == 'D' || m_nus_data_array[0] == 'd') { //to turn OFF the device
      delete_all_data();
    }

    else if (m_nus_data_array[0] == 'W' || m_nus_data_array[0] == 'w') {
      static uint8_t read[20];
      char read_str[20];
      static uint16_t len = 20;
      static uint16_t len_d = 3;
      FRESULT ff_result;
      static FIL file;
      uint32_t bw = 0;
      static uint16_t len1 = 11;
      static uint16_t len2 = 16;
      static uint16_t len3 = 17;
      static uint16_t len4 = 14;
      static uint16_t len5 = 21;
      static uint16_t len6 = 24;
      static uint8_t bat_str[20];
      static uint8_t bat_str1[20];
      static uint8_t bat_str2[20];
      static uint8_t bat_str3[20];
      static uint8_t bat_str4[20];
      static uint8_t bat_str5[20];
      static uint8_t bat_str6[20];
      static uint8_t bat_str7[20];
      static uint8_t bat_str8[20];
      static uint8_t bat_str9[20];
      static uint8_t bat_str10[20];
      static uint8_t bat_str11[20];
      static uint8_t bat_str12[20];
      static uint8_t temp_st[20];
      static uint8_t subscribed_date[20];
      static uint16_t subscribed_date1;
      static uint16_t subscribed_date2;
      static uint16_t subscribed_date3;
      static uint16_t subscribed_time1;
      static uint16_t subscribed_time2;
      static uint16_t subscribed_time3;
      static uint16_t recording_date1;
      static uint16_t recording_date2;
      static uint16_t recording_date3;
      static uint16_t number_of_days;
      static uint16_t otg_status;
      static uint16_t samples_per_sec;
      ff_result = f_open(&file, "user.txt", FA_READ);
      ff_result = f_read(&file, read_str, 20, &bw); // read 174 bytes of data from file
      ff_result = f_close(&file);
      ff_result = f_lseek(&file, 0);
      NRF_LOG_INFO("w_user_number :-%d", read_str[0]);
      NRF_LOG_INFO("w_user_id :-%d,%d,%d,%d,%d,%d", read_str[1], read_str[2], read_str[3], read_str[4], read_str[5], read_str[6]);
      NRF_LOG_INFO("w_subscription_day :-%d,%d,%d", read_str[7], read_str[8], read_str[9]);
      NRF_LOG_INFO("w_subscription_time :-%d,%d,%d", read_str[10], read_str[11], read_str[12]);
      NRF_LOG_INFO("w_samples_per_second :-%d", read_str[13]);
      NRF_LOG_INFO("w_subscribed_days :-%d,%d", read_str[14], read_str[15]);
      NRF_LOG_INFO("w_recording_date :-%d,%d,%d", read_str[16], read_str[17], read_str[18]);
      NRF_LOG_INFO("w_otg_upload :-%d", read_str[19]);

      for (uint8_t i = 0; i < 6; i++) {
        temp_st[i] = read_str[i + 1];
      }
      nrf_delay_ms(100);
      ble_nus_data_send(&m_nus, "USER_ID :- ", &len1, m_conn_handle);
      ble_nus_data_send(&m_nus, (uint8_t *)temp_st, &len, m_conn_handle);

      subscribed_date1 = read_str[7] & 0xff;
      subscribed_date2 = read_str[8] & 0xff;
      subscribed_date3 = read_str[9] & 0xff;
      sprintf(bat_str1, "%d\r\n", subscribed_date1);
      sprintf(bat_str2, "%d\r\n", subscribed_date2);
      sprintf(bat_str3, "%d\r\n", subscribed_date3);
      ble_nus_data_send(&m_nus, "SUBSCRIPTION_DAY", &len2, m_conn_handle);
      ble_nus_data_send(&m_nus, (uint8_t *)bat_str1, &len_d, m_conn_handle);
      ble_nus_data_send(&m_nus, (uint8_t *)bat_str2, &len_d, m_conn_handle);
      ble_nus_data_send(&m_nus, (uint8_t *)bat_str3, &len_d, m_conn_handle);

      subscribed_time1 = read_str[10] & 0xff;
      subscribed_time2 = read_str[11] & 0xff;
      subscribed_time3 = read_str[12] & 0xff;
      sprintf(bat_str4, "%d\r\n", subscribed_time1);
      sprintf(bat_str5, "%d\r\n", subscribed_time2);
      sprintf(bat_str6, "%d\r\n", subscribed_time3);
      ble_nus_data_send(&m_nus, "SUBSCRIPTION_TIME", &len3, m_conn_handle);
      ble_nus_data_send(&m_nus, (uint8_t *)bat_str4, &len_d, m_conn_handle);
      ble_nus_data_send(&m_nus, (uint8_t *)bat_str5, &len_d, m_conn_handle);
      ble_nus_data_send(&m_nus, (uint8_t *)bat_str6, &len_d, m_conn_handle);

      ble_nus_data_send(&m_nus, "SAMPLES_PER_SECS=", &len3, m_conn_handle);
      samples_per_sec = read_str[13] & 0xff;
      sprintf(bat_str7, "%d\r\n", samples_per_sec);
      ble_nus_data_send(&m_nus, (uint8_t *)bat_str7, &len_d, m_conn_handle);

      ble_nus_data_send(&m_nus, "SUBSCRIBED_DATE=", &len2, m_conn_handle);
      number_of_days = read_str[15] & 0xff;
      sprintf(bat_str8, "%d\r\n", number_of_days);
      ble_nus_data_send(&m_nus, (uint8_t *)bat_str8, &len_d, m_conn_handle);

      recording_date1 = read_str[16] & 0xff;
      recording_date2 = read_str[17] & 0xff;
      recording_date3 = read_str[18] & 0xff;
      sprintf(bat_str9, "%d\r\n", recording_date1);
      sprintf(bat_str10, "%d\r\n", recording_date2);
      sprintf(bat_str11, "%d\r\n", recording_date3);
      ble_nus_data_send(&m_nus, "RECORDING_DATE", &len2, m_conn_handle);
      ble_nus_data_send(&m_nus, (uint8_t *)bat_str9, &len_d, m_conn_handle);
      ble_nus_data_send(&m_nus, (uint8_t *)bat_str10, &len_d, m_conn_handle);
      ble_nus_data_send(&m_nus, (uint8_t *)bat_str11, &len_d, m_conn_handle);

      otg_status = read_str[19];
      //        ble_nus_data_send(&m_nus,"OTG_STATUS=" ,  &len1, m_conn_handle);
      sprintf(bat_str12, "%d\r\n", otg_status);
      if (otg_status == 78) {
        ble_nus_data_send(&m_nus, "OTG_TRANSFER_NOT_DONE", &len5, m_conn_handle);
      } else {
        ble_nus_data_send(&m_nus, "OTG_TRANSFER_DONE_BEFORE", &len6, m_conn_handle);
      }

      //  ble_nus_data_send(&m_nus,(uint8_t *)bat_str12,  &len_d, m_conn_handle);

      nrf_delay_us(1);
    }
    /////////////////////////////////////////////////////////bluetooth

    else if (m_nus_data_array[0] == 'Q' || m_nus_data_array[0] == 'q') //to turn OFF the device
    {
      DSTATUS disk_state = STA_NOINIT;

      test_request_flag = 1;
      lis_setup();
      Max30003Setup();
      // fs_init();
      disk_state = disk_initialize(DEV_NAND);
      nrf_delay_ms(100);
      if (disk_state) {
#ifdef DEBUG
        NRF_LOG_INFO("Disk initialization failed.");
#endif
        while (1) {
          nrf_gpio_pin_toggle(LED_PIN_BLUE);
          nrf_delay_ms(100);
        }
        //return;
      } else {
        flash_working_flag = 1;
#ifdef DEBUG
        NRF_LOG_INFO("Disk initialization Success.");
#endif
        //return;
      }
    }

    /////////////////////////////////////////////////////////bluetooth
  }
}

/////////////////////////////////////////////////////////////////////////////////////////
///////////////////                                             /////////////////////////
///////////////////                                             /////////////////////////
///////////////////   RTC FUNCTIONS                             /////////////////////////
///////////////////                                             /////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
static uint32_t second_count = 0;
static void rtc_handler(nrf_drv_rtc_int_type_t int_type) {
  if (int_type == NRF_DRV_RTC_INT_COMPARE0) {
  } else if (int_type == NRF_DRV_RTC_INT_TICK) {
    rtc_counter++;
    if (rtc_counter >= 8) ///////////////////reached 1 second//////////////////////
    {
      rtc_counter = 0;
      if (rtc_flag == 1) {
        rtc_secs++;
        if (rtc_secs > 59) {
          rtc_secs = 0;
          rtc_mins++;
          if (rtc_mins > 59) {
            rtc_mins = 0;
            rtc_hrs++;
            if (rtc_hrs > 23) {
              rtc_hrs = 0;
              update_date_flag = 1;
            }
          }
        }
      }
    }

    if (disconnect_after_recording_flag == true) {
      disconnect_after_recording_count++;
      if (disconnect_after_recording_count >= DISCONNECT_AFTER_START_RECORD_SECONDS * 8) {
        disconnect_after_recording_count = 0;
        sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        disconnect_after_recording_flag = false;
      }
    }

    if (ble_advertise_flag == false && m_usb_connected == false) {
      if (ecg_lead_on_status_flag == 1) {
        ble_advertise_count++;
        if (ble_advertise_count >= (ADVERTISE_AFTER_LEAD_ON_SECONDS * 8)) //qqqqqqqqqqqqqqqqqq
        {
          nrf_gpio_pin_set(LED_PIN_GREEN);
          nrf_gpio_pin_set(LED_PIN_RED);
          nrf_gpio_pin_clear(LED_PIN_BLUE);
#ifdef DEBUG
          NRF_LOG_INFO(" advertising start \r\n");
#endif
          advertising_start(); //////////////////lllllllllllllllllllll
          ble_advertise_flag = true;
          ble_advertise_count = 0;
          led_seq_flag = false;
          turn_off_device_count = 0;
        }
      } else {
        if (ecg_request_received == 0 && otg_data_collection_flag == 0) {
          turn_off_device_count++;
          if (turn_off_device_count >= (SECONDS_TO_POWER_OFF_WITHOUT_ADVERTISING * 8)) {
            turn_off_device_count = 0;
            turn_off_device_flag = true;
          }
        }
      }
    }

    if (led_seq_flag == false && ble_advertise_flag == true) {
      led_indic_cnt++;
      if (led_indic_cnt >= LED_ADVERTISE_SECONDS * 8) {
        led_indic_cnt = 0;
        led_seq_flag = true;
        nrf_gpio_pin_clear(LED_PIN_BLUE);
        nrf_gpio_pin_clear(LED_PIN_GREEN);
        nrf_gpio_pin_clear(LED_PIN_RED);
      }
    }

    if ((lead_led_turn_on_flag == true) && (m_usb_connected == false)) //mmmmmmmmmmmm
    {
      if (lead_led_on_flag == true) {
        lead_led_on_count++;
        if (lead_led_on_count > 1) {
          lead_led_on_count = 0;
          nrf_gpio_pin_clear(LED_PIN_GREEN);
          if (ecg_request_received == 1 && ecg_lead_on_status_flag == 1) {
            nrf_gpio_pin_set(LED_PIN_BLUE);
            nrf_gpio_pin_set(LED_PIN_RED);
          } else if (ecg_request_received == 1 && ecg_lead_on_status_flag == 0) {
            nrf_gpio_pin_clear(LED_PIN_BLUE);
            nrf_gpio_pin_set(LED_PIN_RED);
          } else
            nrf_gpio_pin_set(LED_PIN_BLUE);

          //           #ifdef DEBUG
          //          NRF_LOG_INFO(" lead : blue led ON \r\n");
          //           #endif
          lead_led_on_flag = false;
          lead_led_off_flag = true;
        }
      }
      if (lead_led_off_flag == true) {
        lead_led_off_count++;
        if (lead_led_off_count > 1) {
          nrf_gpio_pin_clear(LED_PIN_RED);
          nrf_gpio_pin_clear(LED_PIN_GREEN);
          nrf_gpio_pin_clear(LED_PIN_BLUE);
          //                       #ifdef DEBUG
          //          NRF_LOG_INFO(" lead : blue led OFF \r\n");
          //           #endif
          lead_led_off_count = 0;
          lead_led_off_flag = false;
          lead_led_on_flag = true;
          if (ecg_request_received == 1) {
            lead_record_led_count++;
            if (lead_record_led_count > 5) {
              lead_record_led_count = 0;
              lead_led_turn_on_flag = false;
            }
          } else {
            lead_led_turn_on_flag = false;
          }
        }
      }

      //        if(number <= 1)
      //        {
      //            nrf_gpio_pin_set(LED_PIN_BLUE);
      //            number++;
      //        }
      //        else if(number > 2)
      //        {
      //            number=0;
      //            nrf_gpio_pin_clear(LED_PIN_BLUE);
      //        }
    }

    if (deactivation_led_flag == true) {
      if (delete_led_on_flag == true) {
        delete_led_on_count++;
        if (delete_led_on_count >= 15) {
          delete_led_on_count = 0;
          nrf_gpio_pin_clear(LED_PIN_GREEN);
          nrf_gpio_pin_set(LED_PIN_BLUE);
          nrf_gpio_pin_clear(LED_PIN_RED);
          delete_led_on_flag = false;
          delete_led_off_flag = true;
        }
      }
      if (delete_led_off_flag == true) {
        delete_led_off_count++;
        if (delete_led_off_count > 1) {
          nrf_gpio_pin_clear(LED_PIN_RED);
          nrf_gpio_pin_clear(LED_PIN_GREEN);
          nrf_gpio_pin_clear(LED_PIN_BLUE);
          delete_led_off_count = 0;
          delete_led_off_flag = false;
          delete_led_on_flag = true;
        }
      }
    }
    //      if(deactivation_led_flag == false)
    //      {
    //         nrf_gpio_pin_clear(LED_PIN_RED);
    //            nrf_gpio_pin_clear(LED_PIN_GREEN);
    //            nrf_gpio_pin_clear(LED_PIN_BLUE);
    //      }

    if (m_usb_connected == false && delete_on_progress_flag == 0) {
      if (ble_connected_flag == 0) { // if ble not connected
        if (led_on_flag == true) {
          led_on_count++;
          if (led_on_count >= ((blink_speed * 8) + 8)) {
            led_on_count = 0;
            nrf_gpio_pin_set(LED_PIN_BLUE);
            led_on_flag = false;
            led_off_flag = true;
          }
        }
        if (led_off_flag == true) {
          led_off_count++;
          if (led_off_count > 1) {
            nrf_gpio_pin_clear(LED_PIN_BLUE);
            led_off_count = 0;
            led_off_flag = false;
            led_on_flag = true;
          }
        }
      } else { // if ble is connected
        if (led_on_flag == true) {
          led_on_count++;
          if (led_on_count >= ((BLINK_SPEED_BLE * 8) + 8)) {
            led_on_count = 0;
            nrf_gpio_pin_set(LED_PIN_BLUE);
            led_on_flag = false;
            led_off_flag = true;
          }
        }
        if (led_off_flag == true) {
          led_off_count++;
          if (led_off_count > 1) {
            nrf_gpio_pin_clear(LED_PIN_BLUE);
            led_off_count = 0;
            led_off_flag = false;
            led_blink_on_flag = true;
          }
        }
        if (led_blink_on_flag == true) {
          led_blink_on_count++;
          if (led_blink_on_count > 1) {
            nrf_gpio_pin_set(LED_PIN_BLUE);
            led_blink_on_count = 0;
            led_blink_on_flag = false;
            led_blink_off_flag = true;
          }
        }
        if (led_blink_off_flag == true) {
          led_blink_off_count++;
          if (led_blink_off_count > 1) {
            nrf_gpio_pin_clear(LED_PIN_BLUE);
            led_blink_off_count = 0;
            led_blink_off_flag = false;
            led_on_flag = true;
          }
        }
      }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////NEW
    else if (m_usb_connected == true) {
      if (red_led_on == true) {
        //      green_led_on = false;
        nrf_gpio_pin_clear(LED_PIN_GREEN);
        nrf_gpio_pin_clear(LED_PIN_BLUE);
        //nrf_gpio_pin_set(LED_PIN_RED);

        if (red_led_on_flag == true) {
          red_led_on_count++;
          if (red_led_on_count >= 30) {
            nrf_gpio_pin_clear(LED_PIN_BLUE);
            nrf_gpio_pin_set(LED_PIN_RED);

            if (otg_led_blink_flag == true) {
              nrf_gpio_pin_set(LED_PIN_GREEN);
            } else if (otg_led_blink_flag == false) {
              nrf_gpio_pin_clear(LED_PIN_GREEN);
              red_led_on = true;
            }

            red_led_on_flag = false;
            red_led_off_flag = true;
            red_led_on_count = 0;
          }
        }
        if (red_led_off_flag == true) {
          red_led_off_count++;
          if (red_led_off_count > 1) {
            nrf_gpio_pin_clear(LED_PIN_RED);
            red_led_off_count = 0;
            red_led_off_flag = false;
            red_led_on_flag = true;
          }
        }

        ////////////////////////////////////////////////////////////////////////////blinking red led
      }
      if (green_led_on == true) {
        //       red_led_on = false;
        nrf_gpio_pin_clear(LED_PIN_BLUE);
        nrf_gpio_pin_clear(LED_PIN_RED);
        nrf_gpio_pin_set(LED_PIN_GREEN);
        if (otg_led_blink_flag == true) {
          nrf_gpio_pin_set(LED_PIN_RED);
        } else if (otg_led_blink_flag == false) {
          nrf_gpio_pin_clear(LED_PIN_RED);
          green_led_on = true;
        }
      }

      //
      //     if(otg_led_blink_flag == true)
      //      {
      //           if (otg_led_on_flag == true) {
      //          otg_led_on_count++;
      //          if (otg_led_on_count >= 10) {
      //            otg_led_on_count = 0;
      //            nrf_gpio_pin_set(LED_PIN_GREEN);
      //            nrf_gpio_pin_clear(LED_PIN_BLUE);
      //            nrf_gpio_pin_set(LED_PIN_RED);
      //            otg_led_on_flag = false;
      //            otg_led_off_flag = true;
      //          }
      //        }
      //          if (otg_led_off_flag == true) {
      //          otg_led_off_count++;
      //          if (otg_led_off_count > 1) {
      //            nrf_gpio_pin_clear(LED_PIN_RED);
      //            nrf_gpio_pin_clear(LED_PIN_GREEN);
      //            nrf_gpio_pin_clear(LED_PIN_BLUE);
      //            otg_led_off_count = 0;
      //            otg_led_off_flag = false;
      //            otg_led_on_flag = true;
      //          }
      //        }
      //
      //      }
      ////m_isb_connetced = true;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////NEW

    if (dateCheckFlag == 1 || ecg_request_received == 1) {
      rr_step_send_counter++;
      if (rr_step_send_counter >= 8) { //2 ticks
        rr_step_send_counter = 0;
        send_RR_and_Steps_flag = 1;
#ifdef DEBUG
//    NRF_LOG_INFO("seconds=%d\r\n",second_count++);
#endif
      }
      if (lis_read_xyz_flag == 0)
        lis_read_xyz_flag = 1;
    }
  }
}

static void lfclk_config(void) {
  ret_code_t err_code = nrf_drv_clock_init();
  APP_ERROR_CHECK(err_code);

  nrf_drv_clock_lfclk_request(NULL);
}

/** @brief Function initialization and configuration of RTC driver instance.
 */
static void rtc_config(void)

{

  uint32_t err_code = 0;

  //Initialize RTC instance
  nrf_drv_rtc_config_t config = NRF_DRV_RTC_DEFAULT_CONFIG;
  config.prescaler = 4095;

  err_code = nrf_drv_rtc_init(&rtc, &config, rtc_handler);
  APP_ERROR_CHECK(err_code);

  //Enable tick event & interrupt
  nrf_drv_rtc_tick_enable(&rtc, true);

  //Set compare channel to trigger interrupt after COMPARE_COUNTERTIME seconds
  err_code = nrf_drv_rtc_cc_set(&rtc, 0, COMPARE_COUNTERTIME * 8, true);
  APP_ERROR_CHECK(err_code);

  //Power on RTC instance
  nrf_drv_rtc_enable(&rtc);
}

/** @brief Function for initializing services that will be used by the application. */
static void services_init(void) {
  uint32_t err_code;
  ble_nus_init_t nus_init;

  memset(&nus_init, 0, sizeof(nus_init));

  nus_init.data_handler = nus_data_handler;

  err_code = ble_nus_init(&m_nus, &nus_init);
  APP_ERROR_CHECK(err_code);
}

/**
 * @brief Function for handling errors from the Connection Parameters module.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error) {
  APP_ERROR_HANDLER(nrf_error);
}

/** @brief Function for initializing the Connection Parameters module. */
static void conn_params_init(void) {
  uint32_t err_code;
  ble_conn_params_init_t cp_init;

  memset(&cp_init, 0, sizeof(cp_init));

  cp_init.p_conn_params = NULL;
  cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
  cp_init.next_conn_params_update_delay = NEXT_CONN_PARAMS_UPDATE_DELAY;
  cp_init.max_conn_params_update_count = MAX_CONN_PARAMS_UPDATE_COUNT;
  cp_init.start_on_notify_cccd_handle = BLE_GATT_HANDLE_INVALID;
  cp_init.disconnect_on_fail = true;
  cp_init.evt_handler = NULL;
  cp_init.error_handler = conn_params_error_handler;

  err_code = ble_conn_params_init(&cp_init);
  APP_ERROR_CHECK(err_code);
}

/**
 * @brief Function for handling advertising events.
 *
 * @details This function is called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt) {
  uint32_t err_code;

  switch (ble_adv_evt) {
  case BLE_ADV_EVT_FAST:

    break;
  case BLE_ADV_EVT_IDLE:
    if (ecg_request_received == 1) {
      nrf_gpio_pin_clear(LED_PIN_BLUE);
      advertising_start();
      sd_ble_gap_adv_stop(m_advertising.adv_handle);
      ble_adv_stop_flag = true;
#ifdef DEBUG
      NRF_LOG_INFO(" advertising stop \r\n");
#endif
    } else {
      if (otg_data_collection_flag == false && delete_on_progress_flag == 0) {
#ifndef DEBUG
        sleep_mode_enter();
#endif
      } else {
        nrf_gpio_pin_clear(LED_PIN_BLUE);
        advertising_start();
      }
    }
    break;
  default:
    break;
  }
}

/**
 * @brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const *p_ble_evt, void *p_context) {
  uint32_t err_code;

  switch (p_ble_evt->header.evt_id) {
  case BLE_GAP_EVT_CONNECTED:
    ble_connected_flag = 1;
    blink_speed = BLINK_SPEED_0;
    m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
    break;

  case BLE_GAP_EVT_DISCONNECTED:
    dateCheckFlag = 0;
    ble_connected_flag = 0;
    ecg_view_flag = 0;
    device_mode = SEND_NOTHING;
    nrf_gpio_pin_clear(LED_PIN_BLUE);
    blink_speed = BLINK_SPEED_4;
    m_conn_handle = BLE_CONN_HANDLE_INVALID;
    break;

  case BLE_GAP_EVT_PHY_UPDATE_REQUEST: {
    ble_gap_phys_t const phys =
        {
            .rx_phys = BLE_GAP_PHY_AUTO,
            .tx_phys = BLE_GAP_PHY_AUTO,
        };
    err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
    APP_ERROR_CHECK(err_code);
  } break;

  case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
    // Pairing not supported.
    err_code = sd_ble_gap_sec_params_reply(m_conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
    APP_ERROR_CHECK(err_code);
    break;

  case BLE_GAP_EVT_DATA_LENGTH_UPDATE_REQUEST: {
    ble_gap_data_length_params_t dl_params;

    // Clearing the struct will effectively set members to @ref BLE_GAP_DATA_LENGTH_AUTO.
    memset(&dl_params, 0, sizeof(ble_gap_data_length_params_t));
    err_code = sd_ble_gap_data_length_update(p_ble_evt->evt.gap_evt.conn_handle, &dl_params, NULL);
    APP_ERROR_CHECK(err_code);
  } break;

  case BLE_GATTS_EVT_SYS_ATTR_MISSING:
    // No system attributes have been stored.
    err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
    APP_ERROR_CHECK(err_code);
    break;

  case BLE_GATTC_EVT_TIMEOUT:
    // Disconnect on GATT Client timeout event.
    err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
        BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
    APP_ERROR_CHECK(err_code);
    break;

  case BLE_GATTS_EVT_TIMEOUT:
    // Disconnect on GATT Server timeout event.
    err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
        BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
    APP_ERROR_CHECK(err_code);
    break;

  case BLE_EVT_USER_MEM_REQUEST:
    err_code = sd_ble_user_mem_reply(p_ble_evt->evt.gattc_evt.conn_handle, NULL);
    APP_ERROR_CHECK(err_code);
    break;

  case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST: {
    ble_gatts_evt_rw_authorize_request_t req;
    ble_gatts_rw_authorize_reply_params_t auth_reply;

    req = p_ble_evt->evt.gatts_evt.params.authorize_request;

    if (req.type != BLE_GATTS_AUTHORIZE_TYPE_INVALID) {
      if ((req.request.write.op == BLE_GATTS_OP_PREP_WRITE_REQ) ||
          (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_NOW) ||
          (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_CANCEL)) {
        if (req.type == BLE_GATTS_AUTHORIZE_TYPE_WRITE) {
          auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
        } else {
          auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
        }
        auth_reply.params.write.gatt_status = APP_FEATURE_NOT_SUPPORTED;
        err_code = sd_ble_gatts_rw_authorize_reply(p_ble_evt->evt.gatts_evt.conn_handle,
            &auth_reply);
        APP_ERROR_CHECK(err_code);
      }
    }
  } break; // BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST

  default:
    // No implementation needed.
    break;
  }
}

uint8_t battery_level(uint16_t adc_cnt) {
  static uint8_t adc[2];
  adc[0] = adc_cnt >> 8 & 0xff;
  adc[1] = adc_cnt & 0xff;
  float vol, level;
  uint8_t bat_level = 0;
  // vol= (3*(float)adc_cnt)/4095;
  // level = ((float)adc_cnt - 879) * 100 / 439;
  level = (((float)adc_cnt - 600) * 100 / 182);
  if (level > 100)
    level = 100;
  // app_usbd_cdc_acm_write(&m_app_cdc_acm,adc,2);

  return (uint8_t)level;
}

void send_RR_and_Steps(void) ////////////////////////////////////////////////////////////////////LEAD ON in 5th bit
{

  static uint16_t len = 0; //20;
  static uint16_t bat = 0; //20;
  static uint32_t activity_cnt_temp = 0;
  static uint8_t p_tx[20];
  uint8_t rr_value_1 = 0;
  uint8_t rr_value_2 = 0;
  len = 20;
  bat = 20;
  rr_value_1 = ((rr_value >> 8) & 0xff);
  rr_value_2 = (rr_value & 0xff);
  //  NRF_LOG_INFO("RR_VALUE %d",rr_value);
  memset(p_tx, 0, sizeof(p_tx));
  p_tx[0] = 'R';
  p_tx[1] = rr_value_1;
  p_tx[2] = rr_value_2;
  p_tx[3] = activity_mode;

  bat = battery_level(saadc_value_count); //&0xFF;
  p_tx[4] = bat;
  if (ecg_lead_on_status_flag == 1) {
    p_tx[5] = '1';
    if (ecg_request_received == 1 && m_usb_connected == false) {
      fs_data_buf[fs_buf_count++] = '*';
      fs_data_buf[fs_buf_count++] = 'A';
      fs_data_buf[fs_buf_count++] = activity_mode;
      fs_data_buf[fs_buf_count++] = bat;
      fs_data_buf[fs_buf_count++] = '*';
    } else
      activity_cnt_temp = 0;
  } else {
    p_tx[5] = '0';
  }
  p_tx[6] = m_usb_connected;
  p_tx[7] = ecg_request_received;
  p_tx[8] = flash_storage_full_flag;
  p_tx[9] = otg_data_upload_flag;

  p_tx[19] = 'E';
  if (p_tx[4] < 20) {
    nrf_gpio_pin_toggle(LED_PIN_BLUE); ///////////////////////////led 1 blinking///////////
  }

  if (ble_connected_flag == 1 && dateCheckFlag == 1) {
    ble_nus_data_send(&m_nus, (uint8_t *)p_tx, &len, m_conn_handle);
  }
}

/**
 * @brief Function for the SoftDevice initialization.
 *
 * @details This function initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void) {
  ret_code_t err_code;

  err_code = nrf_sdh_enable_request();
  APP_ERROR_CHECK(err_code);

  // Configure the BLE stack using the default settings.
  // Fetch the start address of the application RAM.
  uint32_t ram_start = 0;
  err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
  APP_ERROR_CHECK(err_code);

  // Enable BLE stack.
  err_code = nrf_sdh_ble_enable(&ram_start);
  APP_ERROR_CHECK(err_code);

  // Register a handler for BLE events.
  NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}

/** @brief Function for handling events from the GATT library. */
void gatt_evt_handler(nrf_ble_gatt_t *p_gatt, nrf_ble_gatt_evt_t const *p_evt) {
  if ((m_conn_handle == p_evt->conn_handle) && (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED)) {
    m_ble_nus_max_data_len = p_evt->params.att_mtu_effective - OPCODE_LENGTH - HANDLE_LENGTH;
#ifdef DEBUG
//  //NRF_LOG_INFO("Data len is set to 0x%X(%d)", m_ble_nus_max_data_len, m_ble_nus_max_data_len);
#endif
  }
  //  NRF_LOG_DEBUG("ATT MTU exchange completed. central 0x%x peripheral 0x%x",
  //      p_gatt->att_mtu_desired_central,
  //      p_gatt->att_mtu_desired_periph);
}

/** @brief Function for initializing the GATT library. */
void gatt_init(void) {
  ret_code_t err_code;

  err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
  APP_ERROR_CHECK(err_code);

  err_code = nrf_ble_gatt_att_mtu_periph_set(&m_gatt, 64);
  APP_ERROR_CHECK(err_code);
}

/**
 * @brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */
void bsp_event_handler(bsp_event_t event) {
  uint32_t err_code;
  switch (event) {
  case BSP_EVENT_SLEEP:
    sleep_mode_enter();
    break;

  case BSP_EVENT_DISCONNECT:
    err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
    if (err_code != NRF_ERROR_INVALID_STATE) {
      APP_ERROR_CHECK(err_code);
    }
    break;

  case BSP_EVENT_WHITELIST_OFF:
    if (m_conn_handle == BLE_CONN_HANDLE_INVALID) {
      err_code = ble_advertising_restart_without_whitelist(&m_advertising);
      if (err_code != NRF_ERROR_INVALID_STATE) {
        APP_ERROR_CHECK(err_code);
      }
    }
    break;

  default:
    break;
  }
}

/** @brief Function for initializing the Advertising functionality. */
static void advertising_init(void) {
  uint32_t err_code;
  ble_advertising_init_t init;

  memset(&init, 0, sizeof(init));

  init.advdata.name_type = BLE_ADVDATA_FULL_NAME;
  init.advdata.include_appearance = false;
  init.advdata.flags = BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE; // BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE; // BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE;

  init.srdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
  init.srdata.uuids_complete.p_uuids = m_adv_uuids;

  init.config.ble_adv_fast_enabled = true;
  init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
  init.config.ble_adv_fast_timeout = APP_ADV_DURATION;

  init.evt_handler = on_adv_evt;

  err_code = ble_advertising_init(&m_advertising, &init);
  APP_ERROR_CHECK(err_code);

  ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}

/** @brief Function for initializing buttons and LEDs. */
static void buttons_leds_init(void) {
  uint32_t err_code = bsp_init(BSP_INIT_LEDS, bsp_event_handler);
  APP_ERROR_CHECK(err_code);
}

/** @brief Function for initializing the nrf_log module. */
static void log_init(void) {
  ret_code_t err_code = NRF_LOG_INIT(NULL);
  APP_ERROR_CHECK(err_code);

  NRF_LOG_DEFAULT_BACKENDS_INIT();
}

uint16_t crc16(const unsigned char *buffer, uint32_t length) {
  uint16_t crc = 0xFFFF;

  if (buffer && length)
    while (length--) {
      crc = (crc >> 8) | (crc << 8);
      crc ^= *buffer++;
      crc ^= ((unsigned char)crc) >> 4;
      crc ^= crc << 12;
      crc ^= (crc & 0xFF) << 5;
    }

  return crc;
}

uint16_t gen_crc16(const uint8_t *data, uint16_t size) {
  uint16_t out = 0;
  int bits_read = 0, bit_flag;

  /* Sanity check: */
  if (data == NULL)
    return 0;

  while (size > 0) {
    bit_flag = out >> 15;

    /* Get next bit: */
    out <<= 1;
    out |= (*data >> bits_read) & 1; // item a) work from the least significant bits

    /* Increment bit counter: */
    bits_read++;
    if (bits_read > 7) {
      bits_read = 0;
      data++;
      size--;
    }

    /* Cycle check: */
    if (bit_flag)
      out ^= CRC16;
  }

  // item b) "push out" the last 16 bits
  int i;
  for (i = 0; i < 16; ++i) {
    bit_flag = out >> 15;
    out <<= 1;
    if (bit_flag)
      out ^= CRC16;
  }

  // item c) reverse the bits
  uint16_t crc = 0;
  i = 0x8000;
  int j = 0x0001;
  for (; i != 0; i >>= 1, j <<= 1) {
    if (i & out)
      crc |= j;
  }

  return crc;
}
float step_filt(float x) //class II
{
  static float v[5] = {0, 0, 0, 0, 0};
  v[0] = v[1];
  v[1] = v[2];
  v[2] = v[3];
  v[3] = v[4];
  v[4] = (8.083795703448470871e-2 * x) + (-0.27944665508478666593 * v[0]) + (1.49991358069046909485 * v[1]) + (-3.13327444106936470192 * v[2]) + (2.91188742858357940690 * v[3]);
  return (v[0] + v[4]) - 2 * v[2];
}
void read_ecg_params(void) {

  static uint32_t ccc = 0;
  static uint32_t test = 0;
  static uint32_t etag;
  static uint32_t RtoR = 0;
  static uint8_t aaaa[20];
  static uint32_t eint_val = 0;
  static uint32_t status = 0;
  static uint32_t lead_on_status = 0;
  static uint32_t read_ecg_samples = 0;
  static uint32_t samp = 0;
  static uint32_t newInt = 0;
  static uint32_t samples[32];
  static uint32_t etag_ecg[32];
  uint32_t ecgFIFO = 0;
  status = Max30003_Rreg(0x01); //reading status register       //6 ticks

  static uint32_t rint_val = 0;
  rint_val = status;
  eint_val = status & 0x800000; // EINT
  rint_val &= 0x400;
  static uint32_t lead_off_status = 0;
  lead_off_status = status & 0x100000;
  static uint8_t templength = 0;
  if (lead_off_status == 0x100000) { //IF lead off is detected    //16 ticks

    if (ecg_lead_off_flag == 0) {
      //    templength = 8;
      //          ble_nus_data_send(&m_nus, "Lead off", &templength, m_conn_handle);

      ecg_lead_on_status_flag = 0;
      ecg_lead_off_flag = 1;
      ecg_lead_on_flag = 0;
      lead_led_turn_on_flag = true;
      lead_record_led_count = 0;

      if (ecg_request_received == 1 && m_usb_connected == false) {
        fs_data_buf[fs_buf_count++] = '*';
        fs_data_buf[fs_buf_count++] = 'F';
        fs_data_buf[fs_buf_count++] = rtc_hrs;
        fs_data_buf[fs_buf_count++] = rtc_mins;
        fs_data_buf[fs_buf_count++] = rtc_secs;
        fs_data_buf[fs_buf_count++] = '*';
      }
#ifdef DEBUG
      NRF_LOG_INFO("Lead on status = %d\r\n", ecg_lead_on_status_flag);
#endif
      if (ble_adv_stop_flag == true) {
        ble_adv_stop_flag = false;
        ble_advertise_flag = false;
#ifdef DEBUG
        NRF_LOG_INFO("start count for advertising\r\n");
#endif
      }
      if (ecg_request_received == 1) {
        lead_off_record_stop_flag = 1;
      }
    }
    ecgFIFO = Max30003_Rreg(0x21);
    Max30003_Wreg(0x0A, 0x00);
    return;
  } else {

    //if lead ON is detected                  //0 ticks
    if (ecg_lead_on_flag == 0 || ecg_lead_off_flag == 1) {
      //    templength =7;
      //       ble_nus_data_send(&m_nus, "Lead on", &templength, m_conn_handle);

      ecg_lead_off_flag = 0;
      Max30003Setup(); // to sync and lock pll
                       //  max_clear_fifo();
      ecg_lead_on_flag = 1;
      ecgFIFO = Max30003_Rreg(0x21);
      Max30003_Wreg(0x0A, 0x00);
      lead_led_turn_on_flag = true;
      lead_record_led_count = 0;

      if (ecg_request_received == 1 && m_usb_connected == false) {
        fs_data_buf[fs_buf_count++] = '*';
        fs_data_buf[fs_buf_count++] = 'N';
        fs_data_buf[fs_buf_count++] = rtc_hrs;
        fs_data_buf[fs_buf_count++] = rtc_mins;
        fs_data_buf[fs_buf_count++] = rtc_secs;
        fs_data_buf[fs_buf_count++] = '*';
      }
#ifdef DEBUG
      NRF_LOG_INFO("Lead on status = %d\r\n", ecg_lead_on_flag);
#endif
      return;
    }
  }
  ecg_lead_on_status_flag = 1;
  //  lead_led_turn_on_flag = true;
  //  if ((ecg_request_received == 1) || (ecg_view_flag == 1)) {

  if (eint_val == 0x800000) { //if  data samples are available in max     //7 ticks
    read_ecg_samples = 0;

    do {
      ecgFIFO = Max30003_Rreg(0x21); // ECG
#ifdef DEBUG
//    //NRF_LOG_INFO("ecgFIFO: %d \r\n", ecgFIFO);
#endif

      float ecgf = 0;
      newInt = (ecgFIFO >> 6) & 0x3FFFF;
      // newInt = (newInt ^ 0x20000);  //wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
      /*
     if((newInt & 0x20000)== 0x20000)
     {
     ecgf = (float)((~newInt +1)*(-1));
     ecgf-= 262144;
     }
     else{
     ecgf = (float) newInt;
     }
     ecgf = ecgf/1000;
    // ecgf=step_filt(ecgf);
     
      sprintf(aaaa,"%f\r\n",ecgf);
      app_usbd_cdc_acm_write(&m_app_cdc_acm,aaaa,20); 
      */
      if (ecg_view_flag == 1 && m_usb_connected == false) { //send ecg values for viewing the graph

        ecg_ble_tx[ecg_ble_buf_count++] = (uint8_t)(newInt >> 16) & 0x03;
        ecg_ble_tx[ecg_ble_buf_count++] = (uint8_t)(newInt >> 8) & 0xFF;
        ecg_ble_tx[ecg_ble_buf_count++] = (uint8_t)(newInt)&0xFF;
        if (ecg_ble_buf_count > 51) {
          send_packet_flag = 1;
          ecg_ble_buf_count = 0;
        }
      }
      if (ecg_request_received == 1 && m_usb_connected == false) { //store ecg values to external flash
        fs_data_buf[fs_buf_count++] = (uint8_t)(newInt >> 16) & 0x03;
        fs_data_buf[fs_buf_count++] = (uint8_t)(newInt >> 8) & 0xFF;
        fs_data_buf[fs_buf_count++] = (uint8_t)(newInt)&0xFF;
        ecg_sample_size++; // for checking one hour size
      }
      etag_ecg[read_ecg_samples] = (ecgFIFO >> 3) & 0x07;
      read_ecg_samples++;
    } while (etag_ecg[read_ecg_samples - 1] == 0); // || etag_ecg[read_ecg_samples - 1] == 1);

    if (etag_ecg[read_ecg_samples - 1] == 7) {
      Max30003_Wreg(0x0A, 0x00); //FIFO reset
    }
  }
  if (rint_val == 0x400) { //send RR         //6 ticks
    RtoR = Max30003_Rreg(0x25);
    RtoR = (RtoR >> 10) & 0x3FFF;
    rr_value = (uint16_t)RtoR * 8;
  }
  // }
}

uint32_t Max30003_Rreg(int reg) {
  uint8_t out = 0;
  uint32_t ret_data;
  static uint8_t tempReadBuf[4];
  static uint8_t tempWriteBuf[4];
  tempWriteBuf[0] = (((reg << 1) & 0xff) | 0x01); //read GPIO reg

  transfer_success_flag = false;
  nrf_gpio_pin_clear(SPI_CS_PIN_MAX);
  APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi_max, tempWriteBuf, 1, tempReadBuf, 4)); // read reg
  while (!transfer_success_flag) {
    __WFE();
  }
  nrf_gpio_pin_set(SPI_CS_PIN_MAX);
  ret_data = (uint32_t)(tempReadBuf[1] << 16) +
             (tempReadBuf[2] << 8) + tempReadBuf[3];
  return ret_data;
}

uint32_t millis(void) {
  float count;
  uint32_t ms = 0;
  count = (NRF_RTC0->COUNTER) & 0x00FFFFFF;
  count = count / 30.29967;
  ms = (uint32_t)count;
  return ms;
}

static void sensor_timeout_handler(void *p_context) {
  UNUSED_PARAMETER(p_context);
}

static void timers_init(void) {
  ret_code_t err_code = app_timer_init();
  APP_ERROR_CHECK(err_code);
  // err_code = app_timer_create(&m_sensor_id,APP_TIMER_MODE_SINGLE_SHOT, sensor_timeout_handler); // APP_TIMER_MODE_REPEATED
  // APP_ERROR_CHECK(err_code);
}

/**
 * @brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation, then sleep until next the next event occurs.
 */
static void idle_state_handle(void) {
  UNUSED_RETURN_VALUE(NRF_LOG_PROCESS());
  power_manage();
}

// USB CODE START

/** @brief User event handler @ref app_usbd_cdc_acm_user_ev_handler_t */
static void cdc_acm_user_ev_handler(app_usbd_class_inst_t const *p_inst,
    app_usbd_cdc_acm_user_event_t event) {
  app_usbd_cdc_acm_t const *p_cdc_acm = app_usbd_cdc_acm_class_get(p_inst);

  uint8_t tempI = 0;

  switch (event) {
  case APP_USBD_CDC_ACM_USER_EVT_PORT_OPEN: {
    /*Set up the first transfer*/
    ret_code_t ret = app_usbd_cdc_acm_read(&m_app_cdc_acm,
        m_cdc_data_array,
        1);
    UNUSED_VARIABLE(ret);
    break;
  }

  case APP_USBD_CDC_ACM_USER_EVT_PORT_CLOSE:
    if (m_usb_connected) {
    }
    break;

  case APP_USBD_CDC_ACM_USER_EVT_TX_DONE:
    usb_tx_done_flag = 1;
    break;

  case APP_USBD_CDC_ACM_USER_EVT_RX_DONE: {
    ret_code_t ret;
    static uint8_t index = 0;
    index++;

    do {
      if ((m_cdc_data_array[index - 1] == '\n') ||
          (m_cdc_data_array[index - 1] == '\r') ||
          (index >= (m_ble_nus_max_data_len))) {
        if (index > 1) {
          if (m_cdc_data_array[0] == 'e') {
            //  page_read_flag=1;
          }

          if (m_cdc_data_array[0] == 'B' && m_cdc_data_array[19] == 'Z') { //packet starts with 'B' and ends with 'Z'
            if (m_cdc_data_array[1] == 'U') {
              for (tempI = 0; tempI < 15; tempI++) {
                user_id[tempI] = m_cdc_data_array[tempI + 2]; // saving 15 byte imei number
              }
              check_user_flag_OTG = 1;
            }
            if (m_cdc_data_array[1] == 'C') { //getting response frm app
              for (tempI = 0; tempI < 16; tempI++)
                aes_received[tempI] = m_cdc_data_array[2 + tempI];
              for (tempI = 0; tempI < 16; tempI++)
                aes_data_in[tempI] = user_id[tempI];             // copying IMEI value
              aes_data_in[0] = rand_val_high;                    // replacing with MSB of the random value
              aes_data_in[15] = rand_val_low;                    // replacing with LSB of the random value
              response_calc(usb_key, aes_data_in, aes_data_out); //finding encrypted value
              for (tempI = 0; tempI < 16; tempI++) {
                if (aes_received[tempI] != aes_data_out[tempI]) {
                  cipher_error_flag = 1;
                  break;
                }
              }
              cipher_received_flag = 1;
            }
            if (m_cdc_data_array[1] == 'R' && m_cdc_data_array[2] == 'K') { //CRC acknowledgement

              crc_acknowledgment_flag = 1;
#ifdef DEBUG
              NRF_LOG_INFO("OK received\r\n");
#endif
            }
            if (m_cdc_data_array[1] == 'R' && m_cdc_data_array[2] == 'E') { // to wait for CRC acknowledgement

              crc_acknowledgment_flag = 0;
#ifdef DEBUG
              NRF_LOG_INFO("CRC error \r\n");
#endif
              crc_ack_wait_count = 2500000;
            }

            if (m_cdc_data_array[1] == 0X0B) {

              packet_flag = 1;
            }
            if (m_cdc_data_array[1] == 0x0E) {
              ecg_flag = 1;
            }
          }

          while (ret == NRF_ERROR_BUSY)
            ;
        }
        index = 0;
      }

      /* Fetch data until internal buffer is empty */
      ret = app_usbd_cdc_acm_read(&m_app_cdc_acm,
          &m_cdc_data_array[index],
          1);
      if (ret == NRF_SUCCESS) {
        index++;
      }
    } while (ret == NRF_SUCCESS);

    break;
  }
  default:
    break;
  }
}

volatile bool usb_on = false;
volatile bool usb_off = false;
static void usbd_user_ev_handler(app_usbd_event_type_t event) {
  switch (event) {
  case APP_USBD_EVT_DRV_SUSPEND:
    break;

  case APP_USBD_EVT_DRV_RESUME:
    break;

  case APP_USBD_EVT_STARTED:
    break;

  case APP_USBD_EVT_STOPPED:
    app_usbd_disable();
    break;

  case APP_USBD_EVT_POWER_DETECTED:

    if (!nrf_drv_usbd_is_enabled()) {
      app_usbd_enable();
    }
    break;

  case APP_USBD_EVT_POWER_REMOVED: {
    nrf_gpio_pin_clear(LED_PIN_GREEN);
    nrf_gpio_pin_clear(LED_PIN_RED);
    m_usb_connected = false;
    red_led_on = false;
    green_led_on = false;

    app_usbd_stop();
    usb_off = true;
    cnt = 0;
    perc_led = 0;
  } break;
  case APP_USBD_EVT_POWER_READY: {
    m_usb_connected = true;
    app_usbd_start();
    usb_on = true;
    perc_led = 0;
    nrf_gpio_pin_set(LED_PIN_RED);

    cnt++;
    if (cnt > 2) {
      cnt = 0;
      nrf_gpio_pin_clear(LED_PIN_RED);
    }
    red_led_on_flag = true;

  } break;

  default:
    break;
  }
}
void spi_event_handler_lis(nrf_drv_spi_evt_t const *p_event,
    void *p_context) {
  transfer_success_flag = true;
}

void spi_setup_lis(void) {
  nrf_drv_spi_config_t spi_config_lis = NRF_DRV_SPI_DEFAULT_CONFIG;
  spi_config_lis.ss_pin = NULL;
  spi_config_lis.miso_pin = LIS_MOSI; //22;
  spi_config_lis.mosi_pin = LIS_MISO; //23;
  spi_config_lis.sck_pin = LIS_CLK;   //24;
  APP_ERROR_CHECK(nrf_drv_spi_init(&spi_lis, &spi_config_lis, spi_event_handler_lis, NULL));
}

void in_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action) {
  //nrf_drv_gpiote_out_toggle(PIN_OUT);
  //app_usbd_cdc_acm_write(&m_app_cdc_acm,"pin\r\n",5);
  read_ecg_param_flag = 1;
}
void in_pin_handler_bat_status(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action) {
  //nrf_drv_gpiote_out_toggle(PIN_OUT);
  //app_usbd_cdc_acm_write(&m_app_cdc_acm,"pin\r\n",5);
  //bat_status
}
/**
* @brief Function for configuring: PIN_IN pin for input, PIN_OUT pin for output,
* and configures GPIOTE to give an interrupt on pin change.
*/
static void gpio_init(void) {
  ret_code_t err_code;

  err_code = nrf_drv_gpiote_init();
  APP_ERROR_CHECK(err_code);

  nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(true);
  in_config.pull = NRF_GPIO_PIN_PULLDOWN;

  err_code = nrf_drv_gpiote_in_init(17, &in_config, in_pin_handler);
  APP_ERROR_CHECK(err_code);

  //  err_code = nrf_drv_gpiote_in_init(BAT_STATUS, &in_config, in_pin_handler_bat_status);
  //  APP_ERROR_CHECK(err_code);

  nrf_drv_gpiote_in_event_enable(17, true);
  //  nrf_drv_gpiote_in_event_enable(BAT_STATUS, true);
}

static void gpio_uninit(void) {
  nrf_drv_gpiote_in_event_disable(17);
  nrf_drv_gpiote_in_uninit(17);
  nrf_drv_gpiote_uninit();
  //  nrf_drv_gpiote_in_event_disable(BAT_STATUS);
  //  nrf_drv_gpiote_in_uninit(BAT_STATUS);
  //  nrf_drv_gpiote_uninit();
}

void led_start_seq(void) {
  nrf_gpio_cfg_output(LED_PIN_BLUE);
  nrf_gpio_pin_set(LED_PIN_BLUE);
}
void afe_init(void) {
  gpio_init();
  spi_setup_max(); // SPI peripheral setup
  Max30003Setup(); // ECG configuration //Calibration
  ecg_ble_buf_0[0] = 'S';
  ecg_ble_buf_1[0] = 'S';
  ecg_ble_buf_2[0] = 'S';
  ecg_ble_buf_0[19] = 'E';
  ecg_ble_buf_1[19] = 'E';
  ecg_ble_buf_2[19] = 'E';
}

void accelerometer_init(void) {
  nrf_gpio_pin_set(LIS_VCC);
  NRF_LOG_INFO(" SET LIS VCC PIN ");
  spi_setup_lis(); // SPI peripheral setup
  lis_setup();
  calibrate_LIS();
}
void gpio_pins_init(void) {              /////////gggggggggggggg
  nrf_gpio_cfg_output(LIS_VCC);          //(21);
  nrf_gpio_cfg_output(SPI_CS_PIN_LIS);   ////(20);LIS CHIPSELECT            //  nrf_gpio_pin_set(LIS_VCC);             //(21);
  nrf_gpio_cfg_output(30);               //CTRL
  nrf_gpio_cfg_output(SPI_CS_PIN_MAX);   //MAX CHIPSELECT
  nrf_gpio_pin_set(30);                  //CTRL
  nrf_gpio_pin_set(SPI_CS_PIN_MAX);      //CTRL
  nrf_gpio_cfg_output(FLASH_SPI_SS_PIN); ////chip select NAND
  nrf_gpio_pin_set(FLASH_SPI_SS_PIN);    ////chip select NAND
  nrf_gpio_cfg_input(PG, GPIO_PIN_CNF_PULL_Disabled);
  nrf_gpio_cfg_output(LED_PIN_BLUE);
  nrf_gpio_cfg_output(LED_PIN_GREEN);
  nrf_gpio_cfg_output(LED_PIN_RED);
  nrf_gpio_pin_set(LED_PIN_BLUE);
  nrf_gpio_pin_clear(LED_PIN_GREEN);
  nrf_gpio_pin_clear(LED_PIN_RED);
  nrf_gpio_cfg_sense_input(28, NRF_GPIO_PIN_PULLDOWN, NRF_GPIO_PIN_SENSE_HIGH);
  nrf_gpio_cfg_sense_set(28, NRF_GPIO_PIN_SENSE_HIGH); // double tap detection
                                                       //  nrf_gpio_cfg_sense_input(BAT_STATUS, NRF_GPIO_PIN_PULLDOWN, NRF_GPIO_PIN_SENSE_HIGH);
                                                       //  nrf_gpio_cfg_sense_set(BAT_STATUS, NRF_GPIO_PIN_SENSE_HIGH); // double tap detection

  nrf_gpio_cfg_output(25);
  nrf_gpio_cfg_output(31);
  nrf_gpio_cfg_output(5);
  nrf_gpio_cfg_output(14);
  nrf_gpio_cfg_output(16);
  nrf_gpio_cfg_output(gpio_1_8);
  nrf_gpio_cfg_output(gpio_1_9);
  nrf_gpio_cfg_output(gpio_1_12);
  nrf_gpio_cfg_output(gpio_1_14);
  nrf_gpio_cfg_output(gpio_1_15);
  nrf_gpio_cfg_output(gpio_1_1);
  nrf_gpio_cfg_output(gpio_1_2);
  nrf_gpio_cfg_output(gpio_1_3);
  nrf_gpio_cfg_output(gpio_1_4);
  nrf_gpio_pin_clear(25);
  nrf_gpio_pin_clear(25);
  nrf_gpio_pin_clear(31);
  nrf_gpio_pin_clear(5);
  nrf_gpio_pin_clear(14);
  nrf_gpio_pin_clear(16);
  nrf_gpio_pin_clear(BAT_STATUS);
  nrf_gpio_pin_clear(gpio_1_8);
  nrf_gpio_pin_clear(gpio_1_9);
  nrf_gpio_pin_clear(gpio_1_12);
  nrf_gpio_pin_clear(gpio_1_14);
  nrf_gpio_pin_clear(gpio_1_15);
  nrf_gpio_pin_clear(gpio_1_1);
  nrf_gpio_pin_clear(gpio_1_2);
  nrf_gpio_pin_clear(gpio_1_3);
  nrf_gpio_pin_clear(gpio_1_4);
  nrf_gpio_pin_clear(BAT_STATUS);
  nrf_delay_ms(100);
  nrf_gpio_pin_clear(LED_PIN_BLUE);
}
void battery_init(void) {
  saadc_init();
  saadc_sampling_event_init();
  saadc_sampling_event_enable();
}

void ble_init(void) {
  ble_stack_init();
  gap_params_init();
  gatt_init();
  services_init();
  advertising_init();
  conn_params_init();
}
void usbd_init(void) {
  uint16_t err_code;
  ret_code_t ret, rc;
  static const app_usbd_config_t usbd_config = {
      .ev_state_proc = usbd_user_ev_handler};
  app_usbd_serial_num_generate();
  ret = app_usbd_init(&usbd_config);
  APP_ERROR_CHECK(ret);

  app_usbd_class_inst_t const *class_cdc_acm = app_usbd_cdc_acm_class_inst_get(&m_app_cdc_acm);
  ret = app_usbd_class_append(class_cdc_acm);
  APP_ERROR_CHECK(ret);
  ret = app_usbd_power_events_enable();
  APP_ERROR_CHECK(ret);
}

NAND_CHIP toshiba;
struct dhara_nand dhara_toshiba;
dhara_error_t err;
void fs_init(void) {
  int status;
  char tx_buf[10];
  char rx_buf[10];
#ifdef DEBUG
  NRF_LOG_INFO("SPI Flash dhara Test Started");
  NRF_LOG_INFO("SPI INIT");
#endif
  //  nrf_gpio_pin_set(LED_PIN_BLUE);
  spi_flash_init();
  nand_reset(&toshiba);
  DSTATUS disk_state = STA_NOINIT;
  static FATFS fs;
  static DIR dir;
  static FILINFO info;
  static FIL file, file1;
  BYTE work[_MAX_SS];
  FRESULT ff_result;
  uint32_t bw, br, bw1;
  char test_str[200]; // = {0xff};//,0xff,0x01,0x32,0x00,0xff};
  char test_str1[6] = {0xea, 0x03, 0x00, 0x00, 0x00, 0x01};
  char read_str[4600];
  uint8_t ch = 'a';
  uint8_t user_flag = 0;
  memset(test_str, ch, sizeof(test_str));
  nrf_delay_ms(100);
  for (uint32_t retries = 3; retries && disk_state; --retries) {
    if (nand_sw_init(&toshiba) == NAND_ID_NOT_MATCH) {
#ifdef DEBUG
      NRF_LOG_INFO("!!!! ID Not Match !!!");
#endif
      //        while (1)
      //        {
      //            nrf_gpio_pin_toggle(LED_PIN_BLUE);
      //            nrf_delay_ms(100);
      //        }
    }
    nand_scan_bad_block(&toshiba);
    disk_state = disk_initialize(DEV_NAND);
    nrf_delay_ms(100);
  }
  if (disk_state) {
#ifdef DEBUG
    NRF_LOG_INFO("Disk initialization failed.");
#endif
    while (1) {
      nrf_gpio_pin_toggle(LED_PIN_BLUE);
      nrf_delay_ms(100);
    }
    //return;
  } else {
#ifdef DEBUG
    NRF_LOG_INFO("Disk initialization Success.");
#endif
    //return;
  }
  ff_result = f_mount(&fs, "", 0);
  int i = 0;
  uint32_t fs_size_bytes = 0;

  ff_result = f_open(&file, "user.txt", FA_READ);
  if (ff_result == FR_NO_FILESYSTEM) {
    ff_result = f_mkfs("", FM_ANY, 0, work, sizeof(work));
    if (ff_result != FR_OK) {
      NRF_LOG_ERROR("Make FS Fail");
    }
  } else if (ff_result == FR_OK) {
    ff_result = f_read(&file, read_str, 20, &bw); // read 16 bytes of data from file
#ifdef DEBUG
    NRF_LOG_INFO("file number= %d\r\n", read_str[0]);
    NRF_LOG_INFO("username=%c%c%c%c%c%c\r\n", read_str[1], read_str[2], read_str[3], read_str[4], read_str[5], read_str[6]);
    NRF_LOG_INFO("subscription date,year=%d, month=%d, day= %d\r\n", read_str[7], read_str[8], read_str[9]);
    NRF_LOG_INFO("time,hrs=%d, mins=%d, secs= %d\r\n", read_str[10], read_str[11], read_str[12]);
    NRF_LOG_INFO("SPS=%d\r\n", read_str[13]);
    NRF_LOG_INFO("Subscription days=%d\r\n", ((read_str[14] << 8) | (read_str[15] & 0xff)));
    NRF_LOG_INFO("recording date,year=%d, month=%d, day= %d\r\n", read_str[16], read_str[17], read_str[18]);
    NRF_LOG_INFO("OTG upload status=%c\r\n", read_str[19]);
#endif
    user_flag = 1;
  }
  ff_result = f_close(&file);
  ff_result = f_lseek(&file, 0);

  if (user_flag == 0) { // create a file
    memset(test_str, 0xff, 20);
    ff_result = f_open(&file, "user.txt", FA_CREATE_NEW | FA_WRITE);
    ff_result = f_write(&file, test_str, 20, &bw);
    ff_result = f_close(&file);

    set_max_sps = DEFAULT_MAX_SPS;
    subscription_date = DEFAULT_SUBSCRIPTION_DAYS;
  }

  else {
    set_max_sps = read_str[13];
    if (set_max_sps == 0xff) {
      set_max_sps = DEFAULT_MAX_SPS;
    }

    subscription_date = ((read_str[14] & 0xff) << 8) | (read_str[15] & 0xff);
    if (subscription_date = 0xffff) {
      subscription_date = DEFAULT_SUBSCRIPTION_DAYS;
    }
    if (read_str[19] == 'Y') {
      otg_data_upload_flag = 1;
#ifdef DEBUG
      NRF_LOG_INFO("OTG upload flag=1r\n");
#endif
    }
  }

  //============================================================= initialization
  uint8_t set_file_num_flag = 0;
CREATE_FILES:
  for (i = 0; i < NUMBER_OF_DATA_FILES; i++) { // create all files if not initialised and write file number
    ff_result = f_open(&file, fs_file_name[i], FA_CREATE_NEW | FA_WRITE);

    if (ff_result == FR_OK) {
      test_str[0] = i;
      ff_result = f_write(&file, test_str, 1, &bw);
      ff_result = f_close(&file);

    } else if (ff_result == FR_EXIST) {
      ff_result = f_close(&file);
      set_file_num_flag = 1;
      break;
    }
    nrf_delay_ms(1);
  }
  if (set_file_num_flag == 1) // find the existing file
  {
    for (i = 0; i < NUMBER_OF_DATA_FILES; i++) {
      ff_result = f_open(&file, fs_file_name[i], FA_READ);
      ff_result = f_close(&file);
      fs_size_bytes = f_size(&file);
      if (fs_size_bytes < 2) // if file not having any data other than file number, select this file to start writing data
      {
        fs_file_num = i; // set the file to write data
        break;
      }
      nrf_delay_ms(1);
    }
  } else {
    fs_file_num = 0; // point to first file
  }
  fs_file_num_record = fs_file_num;
#ifdef DEBUG
  NRF_LOG_INFO("file num=%d\r\n", fs_file_num_record);
#endif

  if (fs_file_num >= NUMBER_OF_DATA_FILES) {
    flash_storage_full_flag = true;
  }
}

void max_pin(void) {
  nrf_gpio_cfg_output(MAX_CLK);        //18
  nrf_gpio_cfg_output(MAX_MISO);       //20
  nrf_gpio_cfg_output(MAX_MOSI);       //19
  nrf_gpio_cfg_output(SPI_CS_PIN_MAX); //22
  nrf_gpio_cfg_output(MAX_INT1);       //17
  nrf_gpio_cfg_output(MAX_INT2);       //21
  nrf_gpio_cfg_input(PG, GPIO_PIN_CNF_PULL_Pulldown);
  nrf_gpio_pin_set(MAX_CLK);
  nrf_gpio_pin_set(MAX_MISO);
  nrf_gpio_pin_set(MAX_MOSI);
  nrf_gpio_pin_set(SPI_CS_PIN_MAX);
  nrf_gpio_pin_set(MAX_INT1);
  nrf_gpio_pin_set(MAX_INT2);

  nrf_gpio_cfg_output(LIS_CLK);        //18
  nrf_gpio_cfg_output(LIS_MISO);       //20
  nrf_gpio_cfg_output(LIS_MOSI);       //19
  nrf_gpio_cfg_output(SPI_CS_PIN_LIS); //19
  nrf_gpio_pin_set(LIS_CLK);
  nrf_gpio_pin_set(LIS_MISO);
  nrf_gpio_pin_set(LIS_MOSI);
  nrf_gpio_pin_set(SPI_CS_PIN_LIS);

  nrf_gpio_cfg_output(FLASH_SPI_SS_PIN);   //18
  nrf_gpio_cfg_output(FLASH_SPI_MISO_PIN); //20
  nrf_gpio_cfg_output(FLASH_SPI_MOSI_PIN); //19
  nrf_gpio_cfg_output(FLASH_SPI_SCK_PIN);  //19
  nrf_gpio_cfg_output(FLASH_HOLD);         //19
  nrf_gpio_cfg_output(FLASH_SO2);          //19
  nrf_gpio_pin_set(FLASH_SPI_SS_PIN);
  nrf_gpio_pin_set(FLASH_SPI_MISO_PIN);
  nrf_gpio_pin_set(FLASH_SPI_MOSI_PIN);
  nrf_gpio_pin_set(FLASH_SPI_SCK_PIN);
  nrf_gpio_pin_set(FLASH_HOLD);
  nrf_gpio_pin_set(FLASH_HOLD);
  nrf_gpio_pin_set(FLASH_SO2);
}

/** @brief Application main function. */ //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

int main(void) {
 //     while(1){
  //   __WFE();
  //   }
  static uint8_t device_mode_temp = 0;
  uint8_t fs_date_time[20];
  blink_speed = BLINK_SPEED_4;
  static uint8_t uuu = 0;
  uint16_t err_code;
  ret_code_t ret, rc;
  static FIL fs_file, usb_file;
  FRESULT ff_result;
  uint32_t write_size;
  uint8_t tx_temp_data[9000];
  set_max_sps = DEFAULT_MAX_SPS;
#ifdef DEBUG
  APP_ERROR_CHECK(NRF_LOG_INIT(NULL));
  NRF_LOG_DEFAULT_BACKENDS_INIT();
#endif

  // Initialize.

  gpio_pins_init();
  //   while(1){
  //   __WFE();
  //   }
  //   led_start_seq(); // blinking when device power up
  timers_init();  // initialfize timer
  lfclk_config(); // initialise clock for soft device
  rtc_config();   // initialise RTC
  usbd_init();    // usb initialisation
  ble_init();     // ble initialisation
                //  advertising_start();
  battery_init(); //saadc initialisation for battery measurement
//advertising_start();
//max_pin();
//while(1);

  accelerometer_init(); // acclerometer initailisation
  afe_init();           // Max initilisation
 // while(1);
  fs_init();            // flash init

  //   while(1){
  //   __WFE();
  //   }

  fs_buf_count = 0;
  memset(fs_data_buf, 0, sizeof(fs_data_buf));
  ble_advertise_flag = false;
  led_seq_flag = false;
  while (1) {

    if (test_request_flag == 1)

    {
      /////////////////////////////////////////////////////////bluetooth

      if (lis_working_flag == 1) {
        static uint16_t len = 11;
        nrf_delay_ms(100);
        ble_nus_data_send(&m_nus, "LIS working", &len, m_conn_handle);
        nrf_delay_us(1);
        lis_working_flag = 0;
      } else if (lis_working_flag = 0) {
        static uint16_t len = 15;
        nrf_delay_ms(100);
        ble_nus_data_send(&m_nus, "LIS not working", &len, m_conn_handle);
        nrf_delay_us(1);
      }

      if (max_working_flag == 1) {
        static uint16_t len = 11;
        nrf_delay_ms(100);
        ble_nus_data_send(&m_nus, "MAX working", &len, m_conn_handle);
        nrf_delay_us(1);
        max_working_flag = 0;
      } else if (max_working_flag = 0) {
        static uint16_t len = 15;
        nrf_delay_ms(100);
        ble_nus_data_send(&m_nus, "MAX not working", &len, m_conn_handle);
        nrf_delay_us(1);
      }
      if (flash_working_flag == 1) {
        static uint16_t len = 13;
        nrf_delay_ms(100);
        ble_nus_data_send(&m_nus, "FLASH working", &len, m_conn_handle);
        nrf_delay_us(1);
        flash_working_flag = 0;
      } else if (flash_working_flag = 0) {
        static uint16_t len = 17;
        nrf_delay_ms(100);
        ble_nus_data_send(&m_nus, "FLASH not working", &len, m_conn_handle);
        nrf_delay_us(1);
      }
      /////////////////////////////////////////////////////////bluetooth
      test_request_flag = 0;
    }
    if (usb_on == true) {
      usb_on = false;
      nrf_gpio_pin_clear(LED_PIN_BLUE);
      nrf_gpio_pin_clear(30); // turn off MAX
      if (ble_connected_flag == 1) {
        ble_connected_flag = 0;
        sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
      }
    }
    if (usb_off == true) {
      usb_off = false;
      nrf_gpio_pin_clear(LED_PIN_BLUE);
      nrf_gpio_pin_set(30); // turn on MAX
      if (ble_advertise_flag == false)
        ble_adv_stop_flag = true;
      nrf_delay_ms(100);
      Max30003Setup();
    }

    if (turn_off_device_flag == true && otg_data_collection_flag == false) // if device is not connected and is not advertised, putting device to sleep mode
    {
      turn_off_device_flag = false;
#ifndef DEBUG
      sleep_mode_enter();
#endif
    }
    while (app_usbd_event_queue_process()) {
      //Nothing to do
    }
    //    idle_state_handle();
    __WFE();

    ////////////////////
    if (update_date_flag == 1) {
      update_date_flag = 0;
      update_date(); // updating date every 24 hours
    }
    ////////////

    //////////////////////// live view init
    if (ecg_live_view_flag == true) {
#ifdef DEBUG
      NRF_LOG_INFO("ECG live view start\r\n");
#endif
      ecg_live_view_flag = false;
      ecg_view_flag = 1;
      ecg_request_received = 0;
      device_mode = ECG_MODE;
      //   max30003init_flag = 1;
      Max30003Setup();
      //   max_clear_fifo();
    }
    //////////////////////// live view init

    ////////////////////////////// ecg recording start init
    if (ecg_start_recording_flag == true && ecg_lead_on_status_flag == 1) {
#ifdef DEBUG
      NRF_LOG_INFO("ECG recording start\r\n");
#endif
      led_blink(LED_PIN_BLUE, LED_PIN_RED, 5);
      ecg_start_recording_flag = false;
      disconnect_after_recording_flag = true;
      fs_buf_count = 0;
      memset(fs_data_buf, 0, sizeof(fs_data_buf));

      fs_file_num_record = find_file_number();
#ifdef DEBUG
      NRF_LOG_INFO("ECG recording start, file_num=%d\r\n", fs_file_num_record);
#endif
      fs_file_num = fs_file_num_record;

      ecg_view_flag = 0;
      ecg_sample_size = 0;
      fs_date_time[0] = '*';
      fs_date_time[1] = 'S';
      fs_date_time[2] = record_year;
      fs_date_time[3] = record_month;
      fs_date_time[4] = record_day;
      fs_date_time[5] = rtc_hrs;
      fs_date_time[6] = rtc_mins;
      fs_date_time[7] = rtc_secs;
      fs_date_time[8] = '*';
      ff_result = f_open(&fs_file, fs_file_name[fs_file_num], FA_OPEN_APPEND | FA_WRITE);
      ff_result = f_write(&fs_file, fs_date_time, 9, &write_size);
      ff_result = f_close(&fs_file);
      save_record_date_time_nand();
      Max30003Setup();
      //    max_clear_fifo();
      ecg_request_received = 1;
      device_mode = ECG_MODE;
      //   hour_counter=0;

#ifdef DEBUG
      NRF_LOG_INFO("record yy=%d , mm= %d , dd = %d \r\n hrs= %d, mins= %d, secs= %d\r\n", record_year, record_month, record_day, rtc_hrs, rtc_mins, rtc_secs);
#endif
    }

    ////////////////////////////// ecg recording start init

    if (ecg_sample_size > one_hour_sample || open_next_file_flag == 1)

    {
#ifdef DEBUG
      NRF_LOG_INFO("Open new file=%d\r\n", open_next_file_flag);
      NRF_LOG_INFO("ECG sample=%d\r\n", ecg_sample_size);
      NRF_LOG_INFO("rtc mins=%d, rtc secs=%d\r\n", rtc_mins, rtc_secs);
#endif
      open_next_file_flag = 0;
      ecg_sample_size = 0;
      fs_file_num++;
      if (fs_file_num >= NUMBER_OF_DATA_FILES) {
        flash_storage_full_flag = true;
        ecg_request_received = 0;
      } else {
        fs_file_num_record = fs_file_num;
        fs_date_time[0] = '*';
        fs_date_time[1] = 'S';
        fs_date_time[2] = record_year;
        fs_date_time[3] = record_month;
        fs_date_time[4] = record_day;
        fs_date_time[5] = rtc_hrs;
        fs_date_time[6] = rtc_mins;
        fs_date_time[7] = rtc_secs;
        fs_date_time[8] = '*';
        ff_result = f_open(&fs_file, fs_file_name[fs_file_num], FA_OPEN_APPEND | FA_WRITE);
        ff_result = f_write(&fs_file, fs_date_time, 9, &write_size);
        ff_result = f_close(&fs_file);
#ifdef DEBUG
        NRF_LOG_INFO("One hour,record yy=%d , mm= %d , dd = %d \r\n hrs= %d, mins= %d, secs= %d\r\n", record_year, record_month, record_day, rtc_hrs, rtc_mins, rtc_secs);
#endif
        //   Max30003Setup();
      }
    }
    if (m_usb_connected == false) {
      if (ecg_request_received == 1 && ((fs_buf_count > 1875 && f_open_flag == false) || lead_off_record_stop_flag == 1)) // if buffer is 3/4 th full. open the file to write
      {
#ifdef DEBUG
        NRF_LOG_INFO("file open for storing ecg values=%d\r\n", fs_file_num);
#endif
        f_open_flag = true;
        ff_result = f_open(&fs_file, fs_file_name[fs_file_num], FA_OPEN_APPEND | FA_WRITE);
      }
      if (f_open_flag == true && ((fs_buf_count > 2500 && f_write_flag == false) || lead_off_record_stop_flag == 1)) {
#ifdef DEBUG
        NRF_LOG_INFO("write recording into flash,buf_cnt=%d\r\n", fs_buf_count);
#endif
        f_write_flag = true;
        ff_result = f_write(&fs_file, fs_data_buf, fs_buf_count, &write_size);

        if (lead_off_record_stop_flag == 1) {
          lead_off_record_stop_flag = 0;
#ifdef DEBUG
          NRF_LOG_INFO("close file,lead off\r\n");
#endif
          f_open_flag = false;
          f_write_flag = false;
          ff_result = f_close(&fs_file);
          open_next_file_flag = 1;
        }
        fs_buf_count = 0;
        memset(fs_data_buf, 0, sizeof(fs_data_buf));
      }
      if (f_write_flag == true && fs_buf_count > 625) {
#ifdef DEBUG
        NRF_LOG_INFO("close file\r\n");
#endif
        f_open_flag = false;
        f_write_flag = false;
        ff_result = f_close(&fs_file);
      }
    }

    if (lis_read_xyz_flag == 1) //22 TICKS
    {

      lis_read_xyz_flag = 0;
      lis_read_xyz();
    }
    if (send_RR_and_Steps_flag == 1) ///2 ticks
    {
      send_RR_and_Steps_flag = 0;
      send_RR_and_Steps();
    }

    if (read_ecg_param_flag == 1) {
      read_ecg_param_flag = 0;
      read_ecg_params();
    }

    if (send_packet_flag == 1) {
      send_packet_flag = 0;
      len = 20;
      if (m_usb_connected == false) {
        for (int yy = 0; yy < 18; yy++) {
          ecg_ble_buf_0[yy + 1] = ecg_ble_tx[yy];
          ecg_ble_buf_1[yy + 1] = ecg_ble_tx[yy + 18];
          ecg_ble_buf_2[yy + 1] = ecg_ble_tx[yy + 36];
        }

        ble_nus_data_send(&m_nus, (uint8_t *)ecg_ble_buf_0, &len, m_conn_handle);
        ble_nus_data_send(&m_nus, (uint8_t *)ecg_ble_buf_1, &len, m_conn_handle);
        ble_nus_data_send(&m_nus, (uint8_t *)ecg_ble_buf_2, &len, m_conn_handle);
      }
    }

    if (check_user_flag == 1) { // BLE
      uint16_t len = 20;
      user_status = check_user_nand();
#ifdef DEBUG
      NRF_LOG_INFO("check user, user status= %d\r\n", user_status);
#endif
      rand_val = rand() + (uint16_t)millis();
      rand_val_high = (rand_val >> 8) & 0xff;
      rand_val_low = rand_val & 0xff;
      ble_tx_buf[0] = 'B';
      ble_tx_buf[1] = 'I';
      if (user_status == NREGISTERED)
        ble_tx_buf[2] = 'N'; // registe-, not registered, full      ////  R //N
      else if (user_status == REGISTERED) {
        ble_tx_buf[2] = 'R'; // registered, not registered, full
        user_registered = 1;
      } else if (user_status == FULL)
        ble_tx_buf[2] = 'F'; ///F
      ble_tx_buf[3] = rand_val_high;
      ble_tx_buf[4] = rand_val_low;
      ble_tx_buf[5] = ecg_request_received;            // to indicate recording in progress
      ble_tx_buf[6] = flash_storage_full_flag;         // to indicate recording in progress
      ble_tx_buf[7] = otg_data_upload_flag;            // to indicate recording in progress
      ble_tx_buf[8] = set_max_sps;                     // to indicate recording in progress
      ble_tx_buf[9] = (subscription_date >> 8) & 0xff; // to indicate recording in progress
      ble_tx_buf[10] = subscription_date & 0xff;       // to indicate recording in progress
      ble_tx_buf[19] = 'S';
      nrf_delay_ms(100);
      ble_nus_data_send(&m_nus, (uint8_t *)ble_tx_buf, &len, m_conn_handle);
#ifdef DEBUG
      NRF_LOG_INFO("check user, ecg record status= %d\r\n", ecg_request_received);
      NRF_LOG_INFO("check user, ble packet= %s\r\n", ble_tx_buf);
#endif
      imei_received_flag = 1; //allows to wait for next packet
      check_user_flag = 0;
    }

    if (cloud_cipher_received_flag == 1) {
#ifdef DEBUG
      NRF_LOG_INFO("cloud cipher received\r\n");
#endif
      ble_tx_buf[0] = 'B';
      ble_tx_buf[1] = 'C';
      ble_tx_buf[2] = 'C'; // From app, or from web for activation and deactivation
      if (cloud_cipher_error_flag == 1) {
#ifdef DEBUG
        NRF_LOG_INFO("cloud cipher error\r\n");
#endif
        cloud_cipher_error_flag = 0;
        ble_tx_buf[3] = 'N';
      } else {
#ifdef DEBUG
        NRF_LOG_INFO("cloud cipher,  saving user info to nand\r\n");
#endif
        user_status = save_user_nand();
        store_sps_subscription();
        ble_tx_buf[3] = 'O';
        cloud_aes_success_flag = 1;
      }
      ble_tx_buf[19] = 'S';
      ble_nus_data_send(&m_nus, (uint8_t *)ble_tx_buf, &len, m_conn_handle);
      cloud_cipher_received_flag = 0;
    }

    if (app_cipher_received_flag == 1) {
#ifdef DEBUG
      NRF_LOG_INFO("app cipher received\r\n");
#endif
      ble_tx_buf[0] = 'B';
      ble_tx_buf[1] = 'C';
      ble_tx_buf[2] = 'A'; // From app, or from web for activation and deactivation
      if (app_cipher_error_flag == 1) {
#ifdef DEBUG
        NRF_LOG_INFO("app cipher error\r\n");
#endif
        app_cipher_error_flag = 0;
        ble_tx_buf[3] = 'N'; //N
      } else {
#ifdef DEBUG
        NRF_LOG_INFO("app cipher success\r\n");
#endif
        ble_tx_buf[3] = 'O';
        app_aes_success_flag = 1;
      }
      ble_tx_buf[19] = 'S';
      ble_nus_data_send(&m_nus, (uint8_t *)ble_tx_buf, &len, m_conn_handle);
      app_cipher_received_flag = 0;
    }

    if (cloud_deactive_cipher_received_flag == 1) {
      ble_tx_buf[0] = 'B';
      ble_tx_buf[1] = 'C';
      ble_tx_buf[2] = 'X'; // From ap, or from web for activation and deactivation
      if (cloud_cipher_error_flag == 0 && user_registered == 1) {
#ifdef DEBUG
        NRF_LOG_INFO("cloud cipher deactivate\r\n");
#endif
        ecg_request_received = 0; // stop recording
        err_code = delete_all_data();
        if (err_code == 1)
          ble_tx_buf[3] = 'O';
        else
          ble_tx_buf[3] = 'N'; ///N
        aesCheckFlag = 1;
      } else {
        cloud_cipher_error_flag = 0;
        user_registered = 0;
        ble_tx_buf[3] = 'N';
      }
      ble_tx_buf[19] = 'S';
      ble_nus_data_send(&m_nus, (uint8_t *)ble_tx_buf, &len, m_conn_handle);
      cloud_deactive_cipher_received_flag = 0;
    }

    if (test_flag == 1 && (cloud_aes_success_flag == 1 || app_aes_success_flag == 1)) {
      test_flag = 0;
#ifdef DEBUG
      NRF_LOG_INFO("date received\r\n");
#endif

      if (cloud_aes_success_flag == 1) // save date when user register for first time//
      {
        uuu = save_date_nand();
        cloud_aes_success_flag = 0;
      } else if (app_aes_success_flag == 1) {
        //uuu = check_date_nand();
        uuu = 1;
        app_aes_success_flag = 0;
      } else
        uuu = 0;
      ble_tx_buf[0] = 'B';
      ble_tx_buf[1] = 'D';
      if (uuu == 1) {
#ifdef DEBUG
        NRF_LOG_INFO("date operation success\r\n");
#endif
        ble_tx_buf[2] = 'O';
        uuu = 0;
        dateCheckFlag = 1;
      } else {
#ifdef DEBUG
        NRF_LOG_INFO("date operation failed\r\n");
#endif
        ble_tx_buf[2] = 'E';
      }
      ble_tx_buf[19] = 'S';
      ble_nus_data_send(&m_nus, (uint8_t *)ble_tx_buf, &len, m_conn_handle);
    }
    if (date_success_flag == 1) { // implement later
      len = 20;
      ble_tx_buf[0] = 'B';
      ble_tx_buf[1] = 'T';
      ble_tx_buf[2] = 'O';
      ble_tx_buf[3] = ecg_request_received;
      ble_tx_buf[19] = 'S';
      dateCheckFlag = 1;
      ble_nus_data_send(&m_nus, (uint8_t *)ble_tx_buf, &len, m_conn_handle);
      date_success_flag = 0;
      Max30003Setup();
    }

    if (delete_received_flag == 1) {
      len = 20;
      ecg_request_received = 0; // stop recording
      ble_tx_buf[0] = 'B';
      ble_tx_buf[1] = 'U';
      ble_tx_buf[2] = 'C';
      ble_tx_buf[19] = 'S';
      ble_nus_data_send(&m_nus, (uint8_t *)ble_tx_buf, &len, m_conn_handle);
#ifdef DEBUG
      NRF_LOG_INFO("deactivate acknowledgement sent\r\n");
#endif
      delete_data(); // delete data
      ble_tx_buf[0] = 'B';
      ble_tx_buf[1] = 'U';
      ble_tx_buf[2] = 'D';
      ble_tx_buf[3] = 'O';
      ble_tx_buf[19] = 'S';
      ble_nus_data_send(&m_nus, (uint8_t *)ble_tx_buf, &len, m_conn_handle);
      delete_received_flag = 0;
    }

    /////////////////////////////////////////////////////// otg
    if (m_usb_connected == true) {
      if (check_user_flag_OTG == 1) {
#ifdef DEBUG
        NRF_LOG_INFO("OTG: check user\r\n");
#endif
        otg_data_collection_flag = true;
        device_mode = 0;
        ecg_request_received = 0;
        spi_setup_max_flag = 0;
        check_user_flag_OTG = 0;
        uint16_t length = 20;
        user_status = check_user_nand();
        rand_val = rand() + (uint16_t)millis();
        rand_val_high = (rand_val >> 8) & 0xff;
        rand_val_low = rand_val & 0xff;
        tx_temp_data[0] = 'B';
        tx_temp_data[1] = 'U';
        if (user_status == NREGISTERED) {
#ifdef DEBUG
          NRF_LOG_INFO("OTG:not registererd\r\n");
#endif

          tx_temp_data[2] = 'N';
        } else if (user_status == REGISTERED) {
#ifdef DEBUG
          NRF_LOG_INFO("OTG:registererd\r\n");
#endif
          tx_temp_data[2] = 'O';
        }
        tx_temp_data[3] = rand_val_high;
        tx_temp_data[4] = rand_val_low;
        tx_temp_data[19] = 'Z';
        app_usbd_cdc_acm_write(&m_app_cdc_acm,
            tx_temp_data,
            20);
      }
      if (cipher_received_flag == 1) {
#ifdef DEBUG
        NRF_LOG_INFO("OTG:cipher received\r\n");
#endif
        uint16_t tempI = 0;
        uint16_t len = 20;
        tx_temp_data[0] = 'B';
        tx_temp_data[1] = 'C';
        if (cipher_error_flag == 1) {
#ifdef DEBUG
          NRF_LOG_INFO("OTG:cipher error\r\n");
#endif
          cipher_error_flag = 0;
          tx_temp_data[2] = 'E'; // 'E'
        } else {
#ifdef DEBUG
          NRF_LOG_INFO("OTG: cipher success\r\n");
#endif
          tx_temp_data[2] = 'O';
          aes_success_flag = 1;
        }
        tx_temp_data[3] = 'B';
        tx_temp_data[4] = 0;
        tx_temp_data[5] = 0;
        tx_temp_data[6] = 0;
        tx_temp_data[7] = 0;
        tx_temp_data[8] = 0;
        for (tempI = 0; tempI < 10; tempI++)
          tx_temp_data[tempI + 9] = aes_received[tempI + 6];
        tx_temp_data[18] = 0;
        tx_temp_data[19] = 'Z';
        ret_code_t ret = app_usbd_cdc_acm_write(&m_app_cdc_acm, tx_temp_data, 20);
        cipher_received_flag = 0;
      }
      if (packet_flag == 1) {
#ifdef DEBUG
        NRF_LOG_INFO("OTG:packet flag received\r\n");
#endif
        otg_data_collection_flag = true;

        uint16_t temp = 0;
        uint16_t count = 0;
        uint16_t num = 0;
        uint16_t len = 0;
        tx_temp_data[0] = 'B';
        tx_temp_data[1] = 0x0B;
        uint32_t packet_num = total_sample_size();
//         NRF_LOG_INFO("OTG:packet flag,packet num=%d\r\n", packet_num);
//        NRF_LOG_INFO("OTG:packet flag,number of files=%d\r\n", number_of_files);
#ifdef DEBUG
        NRF_LOG_INFO("OTG:packet flag,packet num=%d\r\n", packet_num);
        NRF_LOG_INFO("OTG:packet flag,number of files=%d\r\n", number_of_files);
#endif

        tx_temp_data[2] = (packet_num >> 24) & 0xff;
        tx_temp_data[3] = (packet_num >> 16) & 0xff;
        tx_temp_data[4] = (packet_num >> 8) & 0xff;
        tx_temp_data[5] = (packet_num)&0xff;

        tx_temp_data[6] = number_of_files; // number of files: how many hours of data

        //  flash_read(FLASH_CONFIG_FILE_ID, FLASH_CONFIG_DATE, 3);  // replace with external flash
        //////////////////// read flash user details

        static FIL file;
        uint32_t bw = 0;
        char read_str[20];
        char write_str[20];

        ff_result = f_open(&file, "user.txt", FA_READ);
        ff_result = f_read(&file, read_str, 20, &bw); // read 174 bytes of data from file
        ff_result = f_close(&file);
        ff_result = f_lseek(&file, 0);
        //////////////////// read flash user details

        tx_temp_data[7] = read_str[18]; // starting date
        tx_temp_data[8] = read_str[17]; // starting date
        tx_temp_data[9] = read_str[16]; // starting date

#ifdef DEBUG
        NRF_LOG_INFO("OTG:packet flag, year=%d, month=%d, day=%d \r\n", tx_temp_data[7], tx_temp_data[8], tx_temp_data[9]);
#endif

        tx_temp_data[10] = read_str[10]; // start recording time
        tx_temp_data[11] = read_str[11]; // start recording time
        tx_temp_data[12] = read_str[12]; // start recording time
        tx_temp_data[19] = 'Z';
#ifdef DEBUG
        NRF_LOG_INFO("OTG:packet flag, hrs=%d, mins=%d, secs=%d \r\n", tx_temp_data[10], tx_temp_data[11], tx_temp_data[12]);
#endif
        ret_code_t ret = app_usbd_cdc_acm_write(&m_app_cdc_acm,
            tx_temp_data,
            20);
        packet_flag = 0;
      }
      if (ecg_flag == 1) {
        otg_led_blink_flag = true;
        //        red_led_on = false;
        ecg_flag = 0;
        static uint16_t temp = 0;
        static uint16_t i = 0;
        uint8_t read_str_otg[4500];
        uint32_t read_size = 0;
        uint32_t fs_size_bytes = 0;
        static bool end_of_file_flag = false;
        static bool otg_upload_fail_flag = false;
        uint32_t reset_count = 0;
        uint8_t ret_val = 0;
        reset_count = 0;
        for (i = 0; i < NUMBER_OF_DATA_FILES; i++) {
          //      for (i = 0; i < 3; i++) {
#ifdef DEBUG
          NRF_LOG_INFO("file number=%d\r\n", i);
#endif
          while (app_usbd_event_queue_process()) {
          }
          idle_state_handle();
          ff_result = f_open(&usb_file, fs_file_name[i], FA_READ); // open one file at a time

          fs_size_bytes = f_size(&usb_file);
#ifdef DEBUG
          NRF_LOG_INFO("OTG:file name= %s\r\n", fs_file_name[i]);
          NRF_LOG_INFO("OTG:file size= %d\r\n", fs_size_bytes);
#endif
          if (fs_size_bytes < 2) {
#ifdef DEBUG
            NRF_LOG_INFO("OTG:empty file,size= %d\r\n", fs_size_bytes);
#endif
            end_of_file_flag = true;
            ff_result = f_close(&usb_file); // close file and break
            break;
          }
          if (fs_size_bytes < FILE_DISCARD_SIZE) {
#ifdef DEBUG
            NRF_LOG_INFO("OTG:file size less than 20Kb,size= %d\r\n", fs_size_bytes);
#endif
            ff_result = f_close(&usb_file); // close file and break
            continue;
          }
          if (ff_result == FR_OK) {
            end_of_file_flag = false;
          } else {
            end_of_file_flag = true;
            break;
          }
          // send BE and wait for acknowledgement
          ret_val = send_be();
#ifdef DEBUG
          NRF_LOG_INFO("ret value for BE= %d\r\n", ret_val);
#endif
          if (ret_val == 0) {               // no acknowledgement received
            ff_result = f_close(&usb_file); // close file and break
            otg_upload_fail_flag = true;
            break;
          }
          reset_count = 0;
          do {
            memset(read_str_otg, 0, FLASH_READ_BYTES);
            memset(tx_temp_data, 0, (FLASH_READ_BYTES + 6));
            ff_result = f_read(&usb_file, read_str_otg, FLASH_READ_BYTES, &read_size); // read 174 bytes of data from file
            if (ff_result != FR_OK) {
              end_of_file_flag = true;
              break;
            }
            uint8_t pckt_send_cnt = 0;
            do { // repeat 5 times if acknowledgement not received
              tx_temp_data[0] = (reset_count >> 24) & 0xff;
              tx_temp_data[1] = (reset_count >> 16) & 0xff;
              tx_temp_data[2] = (reset_count >> 8) & 0xff;
              tx_temp_data[3] = (reset_count)&0xff;
              /* for (temp = 0; temp < 174; temp++) {
                tx_temp_data[4 + temp] = read_str_otg[temp];
              }*/
              memcpy(tx_temp_data + 4, read_str_otg, FLASH_READ_BYTES);
              uint16_t crc_value = crc16(read_str_otg, FLASH_READ_BYTES);
              tx_temp_data[FLASH_READ_BYTES + 4] = (crc_value >> 8) & 0xff;
              tx_temp_data[FLASH_READ_BYTES + 5] = (crc_value)&0xff;
              app_usbd_cdc_acm_write(&m_app_cdc_acm, tx_temp_data, (FLASH_READ_BYTES + 6));
              
              ret_val = usb_crc_check();
              if (ret_val == 1) {
#ifdef DEBUG
                NRF_LOG_INFO("CRC check success for reset_count=%d \r\n", reset_count);
#endif
                break;
              }
              pckt_send_cnt++;
            } while (pckt_send_cnt < OTG_PCKT_RPT_COUNT);
            if (pckt_send_cnt >= OTG_PCKT_RPT_COUNT) {
              otg_upload_fail_flag = true;
              end_of_file_flag = true;
              break;
            }
            //      if (read_size < 174) {
            if (read_size < 4096) {
              end_of_file_flag = true;
            } else {
              end_of_file_flag = false;
            }
            reset_count++;
            nrf_delay_us(100);
          } while (end_of_file_flag == false);
          //              tx_temp_data[0] = 'Z';
          //              app_usbd_cdc_acm_write(&m_app_cdc_acm, tx_temp_data, 1);

          nrf_delay_ms(10);
          ret_val = send_file_terminate();
          ff_result = f_close(&usb_file);
#ifdef DEBUG
          NRF_LOG_INFO("ret value for Z= %d\r\n", ret_val);
#endif
          if (ret_val == 0) { // no acknowledgement received
            otg_upload_fail_flag = true;
            break;
          }
          nrf_delay_ms(10);
          if (m_usb_connected == false)
            break;
        } //////////========================================================= end of for loop
        tx_temp_data[0] = 'Y';
        app_usbd_cdc_acm_write(&m_app_cdc_acm, tx_temp_data, 1);
        otg_data_collection_flag = false;
        if (otg_upload_fail_flag == true) {
          otg_upload_fail_flag = false;
        } else {
          set_otg_upload_status(1);
        }

      } //end of ecg_flag
      //        nrf_gpio_pin_clear(LED_PIN_BLUE);
      //        nrf_gpio_pin_clear(LED_PIN_GREEN);
      //        nrf_gpio_pin_clear(LED_PIN_RED);
      otg_led_blink_flag = false;
      //        red_led_on = true;
    } //end of m_usb_connected=="TRUE"
    //////////////////////////////////////////////////////////otg
  }
}
void max_clear_fifo(void) {
  Max30003_Wreg(0x0A, 0x00);
  Max30003_Wreg(0x09, 0x00); // sync
}

void Max30003Setup(void) {

  uint32_t max30000_timeout = 0;
  uint32_t c;
  uint8_t sp[32];
  Max30003_Wreg(0x08, 0);

  c = Max30003_Rreg(0x10); //eeeeeeeeeeeeeeeeeeeeeeeee
  c &= 0x7C000;
  c |= 0x181117; // fmstr //0x80010   // CNFG_GEN  0x181213f // working:0x181113 0x181117
  Max30003_Wreg(0x10, c);
  nrf_delay_us(200);
  c = Max30003_Rreg(0x10); // read this to debug
  if (c) {
    max_working_flag = 1;
#ifdef DEBUG
    NRF_LOG_INFO("Max working: reg 0x10=%d\r\n", c);
#endif
  } else {
#ifdef DEBUG
    NRF_LOG_INFO("Max not working\r\n");
#endif
  }

  // CNF_EMUX
  Max30003_Wreg(0x14, 0x00); // CNF_EMUX 0x00 0x3B0000 0xB0000
  nrf_delay_us(100);
  //CNFG_ECG
  c = Max30003_Rreg(0x15);
  c &= 0x3C8FFF;          // ECG rate ,GAIN, DHPF,DLPF
  if (set_max_sps == 1) { // set rate =250sps (0x405000): 500sps (0x5000)GAIN=0,,DHPF=0.5Hz,DLPF=40Hz
    c |= 0x405000;        // without hpf   c |= 0x402000;
    one_hour_sample = ONE_HOUR_SAMPLE_COUNT_250;
#ifdef DEBUG
    NRF_LOG_INFO("Max 250 sps\r\n");
#endif

  } else {
    c |= 0x5000;
    one_hour_sample = ONE_HOUR_SAMPLE_COUNT_500;
#ifdef DEBUG
    NRF_LOG_INFO("Max 500 sps\r\n");
#endif
  }
  Max30003_Wreg(0x15, c); //0x1000 //0x5000

  //R-to-R config
  c = Max30003_Rreg(0x1D);
  c &= 0xFF7FFF;
  c |= 0x8000;
  Max30003_Wreg(0x1D, c); //rtor

  // MNGR_INT
  nrf_delay_us(200);
  c = Max30003_Rreg(0x04); //
  c &= 0x7FFCF;
  c |= 0x880010; //6= 0x280010; 18 = 0x880010              //0x10; // Clear RRint on RtoR readback 0x280010 0x080010    f80010
  Max30003_Wreg(0x04, c);
  nrf_delay_us(100);

  //Enalbe interrupt
  c = Max30003_Rreg(0x02);
  c &= 0x0FF0FC;          //
  c |= 0x800001;          //
  Max30003_Wreg(0x02, c); //0x1000 //0x5000
  nrf_delay_us(100);

  // PLL lock check
  do {

    c = Max30003_Rreg(0x01);
    c &= 0x100;
    nrf_delay_us(500);

  } while ((c == 0x100) && max30000_timeout++ <= 1000);
#ifdef DEBUG
  NRF_LOG_INFO("Max pll timeout=%d\r\n", max30000_timeout);
#endif

  max30000_timeout = 0;
  Max30003_Wreg(0x0A, 0x00);
  Max30003_Wreg(0x09, 0x00); // sync

 
}

void Max30003_Wreg(uint8_t reg, uint32_t data) {
  //see pages 40,43 of datasheet -
  uint8_t tempWriteBuf[4];
  //=============================================
  tempWriteBuf[0] = (reg << 1) & 0xfe; //read GPIO reg
  tempWriteBuf[1] = (data >> 16) & 0xff;
  tempWriteBuf[2] = (data >> 8) & 0xff;
  tempWriteBuf[3] = data & 0xff;
  transfer_success_flag = false;
  nrf_gpio_pin_clear(SPI_CS_PIN_MAX);
  APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi_max, tempWriteBuf, 4, NULL, 0)); // read reg
  while (!transfer_success_flag) {
    __WFE();
  }
  nrf_gpio_pin_set(SPI_CS_PIN_MAX);
}

int lis_rreg(int reg) {
  uint8_t out = 0;
  uint8_t tempReadBuf[4];
  uint8_t tempWriteBuf[4];
  tempWriteBuf[0] = 0x80 | reg;
  transfer_success_flag = false;
  nrf_gpio_pin_clear(SPI_CS_PIN_LIS);
  APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi_lis, tempWriteBuf, 1, tempReadBuf, 2));
  while (!transfer_success_flag) {
    __WFE();
  }
  nrf_gpio_pin_set(SPI_CS_PIN_LIS);
  return ((int)tempReadBuf[1]);
}
/**@brief Function is used for writing to the registers of lis 
   *
   * @param[in]Function use reg and val parameters to indicate the value and registers
   */

void lis_wreg(int reg, int val) {
  ret_code_t er_cod = 0;
  uint8_t tempWriteBuf[4];
  tempWriteBuf[0] = WREG | reg; //read GPIO reg
  tempWriteBuf[1] = val;
  transfer_success_flag = false;
  nrf_gpio_pin_clear(SPI_CS_PIN_LIS);
  APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi_lis, tempWriteBuf, 2, NULL, 0));
  while (!transfer_success_flag) {
    __WFE();
  }

  nrf_gpio_pin_set(SPI_CS_PIN_LIS);
}

/**@brief Function is used for filtering the values of ecg samples and remove the noise
   *
   * @param[in]Function takes parameter x
   */

float lis_filt(float x) {
  float ret_val = 0;
  static float mov[10];
  mov[0] = mov[1];
  mov[1] = mov[2];
  mov[2] = mov[3];
  mov[3] = mov[4];
  mov[4] = mov[5];
  mov[5] = (9.226197364764136648e-4 * x) + (0.09740780898628932649 * mov[0]) +
           (-0.72377700027819225070 * mov[1]) + (2.21402534418068563937 * mov[2]) +
           (-3.50279228850048696131 * mov[3]) + (2.88561230404445900888 * mov[4]);
  ret_val = (mov[0] + mov[5]) + 5 * (mov[1] + mov[4]) + 10 * (mov[2] + mov[3]);
  return ret_val;
}
void calibrate_LIS(void) {
  uint16_t len = 17;
  uint32_t x_sample, y_sample, z_sample, x_temp, y_temp, z_temp;
  x_sample = 0;
  y_sample = 0;
  z_sample = 0;
  for (int cc = 0; cc < 50; cc++) {

    x_temp = lis_rreg(OUT_X) ^ 0x80;
    y_temp = lis_rreg(OUT_Y) ^ 0x80;
    z_temp = lis_rreg(OUT_Z) ^ 0x80;

    x_sample = x_sample + x_temp;
    y_sample = y_sample + y_temp;
    z_sample = z_sample + z_temp;
    nrf_delay_ms(10);
#ifdef DEBUG
    //    NRF_LOG_INFO("x=%d y=%d z=%d",x_temp,y_temp,z_temp);
#endif
  }
  x_avg = (float)x_sample / 50;
  y_avg = (float)y_sample / 50;
  z_avg = (float)z_sample / 50;
#ifdef DEBUG
  NRF_LOG_INFO("xavg=%d yavg=%d zavg=%d", x_avg, y_avg, z_avg);
#endif
}

/**@brief Function is used for configuration of lis
   *
   * @param[in]Function takes no parameters
   */

void lis_setup(void) { //aaaaaaaaaaaaaaaaaaaaaaaaa
  uint32_t fdg = 0;
  nrf_delay_ms(100);
  lis_wreg(CTRL_REG1, 0x7f); // low power enable
  nrf_delay_ms(100);
  lis_wreg(CTRL_REG3, 0x80);
  nrf_delay_ms(100);
  lis_wreg(REFERENCE, 0x01);    // reference for interrupt
  lis_wreg(IG1_CFG, 0x70);      // interrupt when z axis enables
  lis_wreg(IG1_THS, 0x40);      // Interrupt 1 threshold.max
  lis_wreg(IG1_DURATION, 0x01); // IG1_DURATION register

  //////////////////////////////////////////////////////////////////////////////double tapping
  lis_wreg(CLICK_CFG, 0x2A); // Enables interrupt double-click on Z-axis
  lis_wreg(CLICK_THS, 0x2f); // threshold value for double click fn
  lis_wreg(TIME_LIMIT, 0xff);
  lis_wreg(TIME_LATENCY, 0x64);
  lis_wreg(TIME_WINDOW, 0x1f);
  lis_wreg(CLICK_SRC, 0x57); //DT
  fdg = lis_rreg(TIME_WINDOW);
  fdg = lis_rreg(TIME_LATENCY);
  //////////////////////////////////////////////////////////////////////////////double tapping
  if (fdg) {

    /////////////////////////////////////////////////////////bluetooth

    lis_working_flag = 1;
    /////////////////////////////////////////////////////////bluetooth
#ifdef DEBUG
    NRF_LOG_INFO("Lis working: time latency =%d\r\n", fdg);
#endif
    nrf_delay_ms(1);
  } else {
#ifdef DEBUG
    NRF_LOG_INFO("Lis not working\r\n");
#endif
  }
}

/**@brief Function for ensuring the response from lis
   *
   * @param[in] p_event is used as the parameter
   */
//void spi_event_handler_lis(nrf_drv_spi_evt_t const *p_event)
//{
//  spi_xfer_done = true;
//}

/**@brief Function is used for reading lis
   *
   *this function is used to detect the movement from the three  axis 
   *
   * @param[in]Function takes no parameters
   */

void lis_read_xyz(void) { //10 ticks

  static float acc_threshold = 12;
  static float run_threshold = 25;
  static float INTENSE_THRESHOLD = 55;
  static float MODERATE_THRESHOLD = 33;
  static float LOW_THRESHOLD = 10;

  static float cur_acc_value = 0;
  static float prev_acc_value = 0;
  static float max_acc_value = 0;
  static float min_acc_value = 99999;
  static float difference = 0;

  uint8_t ijkl[20];
  static uint16_t high_detect_count = 0;
  static uint16_t running_detect_count = 0;
  static uint16_t walkin_detect_count = 0;
  static uint16_t rest_detect_count = 0;
  static uint16_t cycling_detect_count = 0;
  static uint16_t movement_reset_count = 0;
  static uint32_t calibrate_count = 0;

  x_value = (lis_rreg(OUT_X) ^ 0x80) & 0xff;
  y_value = (lis_rreg(OUT_Y) ^ 0x80) & 0xff;
  z_value = (lis_rreg(OUT_Z) ^ 0x80) & 0xff;

  //x_value = (x_value ^ 0x80) & 0xff;
  //y_value = (y_value ^ 0x80) & 0xff;
  //z_value = (z_value ^ 0x80) & 0xff;
  movement = sqrt((((float)x_value - x_avg) * ((float)x_value - x_avg)) +
                  (((float)y_value - y_avg) * ((float)y_value - y_avg)) +
                  (((float)z_value - z_avg) * ((float)z_value - z_avg)));
  // movement = lis_filt(movement);

  timeout_count++;

  if (timeout_count < 20) { // checking 100 samples
    cur_acc_value = movement;

    if (cur_acc_value > max_acc_value) { // finding the maximum value
      max_acc_value = cur_acc_value;
    }
    if (cur_acc_value < min_acc_value) { // finding minimum value
      min_acc_value = cur_acc_value;
    }
    prev_acc_value = movement;
  } else { // if 100 values are checked
    timeout_count = 0;

    difference = (max_acc_value - min_acc_value); // finding the difference
                                                  ////NRF_LOG_INFO("difference=%d\r\n",difference);/

    if (difference > INTENSE_THRESHOLD) // if difference greater than intense threshold
    {
#ifdef DEBUG
      NRF_LOG_INFO("INTENSE");
#endif
      activity_mode = INTENSE;
    } else if (difference > MODERATE_THRESHOLD) // if intense greater than moderate threshold
    {
#ifdef DEBUG
      NRF_LOG_INFO("MODERATE");
#endif

      activity_mode = MODERATE;

    } else if (difference > LOW_THRESHOLD) // if difference greater than low threshold
    {
#ifdef DEBUG
      NRF_LOG_INFO("LOW");
#endif

      activity_mode = LOW;
    } else {
#ifdef DEBUG
      NRF_LOG_INFO("rest");
#endif

      activity_mode = REST;
    }
    min_acc_value = 999999;
    max_acc_value = 0;
  }
  if (activity_mode == REST && movement > 10) {
    calibrate_count++;
    if (calibrate_count > 500) {
      calibrate_count = 0;
      x_avg = x_value;
      y_avg = y_value;
      z_avg = z_value;
    }
  }
}

uint32_t total_sample_size(void) {
  int i = 0;
  uint32_t total_size = 0;
  uint32_t cur_file_size = 0;
  static FIL file;
  FRESULT ff_result;
  number_of_files = 0;
  total_size = 0;
  for (i = 0; i < NUMBER_OF_DATA_FILES; i++) {
    //  for (i = 0; i < 3; i++) {
    ff_result = f_open(&file, fs_file_name[i], FA_READ);

    ff_result = f_close(&file);
    if (ff_result == FR_OK) {
      cur_file_size = f_size(&file);
      if (cur_file_size < 2) {
        break;
      }
      if (cur_file_size < FILE_DISCARD_SIZE) {
        continue;
      }
      total_size += cur_file_size;
      number_of_files = number_of_files + 1;
    } else
      break;
  }
  return total_size;
}

uint8_t send_be(void) {
  uint32_t crc_ack_wait_count = 0;
  uint32_t brk_ack_count = 0;
  uint8_t tx_temp_data[10];
  crc_acknowledgment_flag = 0;
  nrf_delay_ms(10);
#ifdef DEBUG
  NRF_LOG_INFO("sending BE\r\n");
#endif
  tx_temp_data[0] = 'B';
  tx_temp_data[1] = 0x0E;
  app_usbd_cdc_acm_write(&m_app_cdc_acm, tx_temp_data, 2);
  do {
    while (app_usbd_event_queue_process()) {
      //Nothing to do
    }
    idle_state_handle();
    crc_ack_wait_count++;
    if (crc_ack_wait_count > 300000) {
#ifdef DEBUG
      NRF_LOG_INFO("sending second BE\r\n");
#endif
      tx_temp_data[0] = 'B';
      tx_temp_data[1] = 0x0E;
      app_usbd_cdc_acm_write(&m_app_cdc_acm, tx_temp_data, 2);

      crc_ack_wait_count = 0;
      brk_ack_count++;
      if (brk_ack_count > 10) {
        brk_ack_count = 0;
        return 0;
      }
    }
  } while (crc_acknowledgment_flag == 0 && m_usb_connected == true);
  crc_acknowledgment_flag = 0;
#ifdef DEBUG
  NRF_LOG_INFO(" BE OK received\r\n");
#endif
  return 1;
}

uint8_t send_file_terminate(void) {
  uint32_t crc_ack_wait_count = 0;
  uint32_t brk_ack_count = 0;
  uint8_t tx_temp_data[10];
  crc_acknowledgment_flag = 0;
#ifdef DEBUG
  NRF_LOG_INFO("sending Z\r\n");
#endif
  tx_temp_data[0] = 'Z';
  app_usbd_cdc_acm_write(&m_app_cdc_acm, tx_temp_data, 1);
  do {
    while (app_usbd_event_queue_process()) {
      //Nothing to do
    }
    idle_state_handle();
    crc_ack_wait_count++;

    if (crc_ack_wait_count > 300000) {
#ifdef DEBUG
      NRF_LOG_INFO("sending Z\r\n");
#endif
      tx_temp_data[0] = 'Z';
      app_usbd_cdc_acm_write(&m_app_cdc_acm, tx_temp_data, 1);
      crc_ack_wait_count = 0;
      brk_ack_count++;
      if (brk_ack_count > 10) {
        brk_ack_count = 0;
        return 0;
      }
    }
  } while (crc_acknowledgment_flag == 0 && m_usb_connected == true);
  crc_acknowledgment_flag = 0;
#ifdef DEBUG
  NRF_LOG_INFO(" Z OK received\r\n");
#endif
  return 1;
}

uint8_t usb_crc_check(void) {
  crc_ack_wait_count = 0;
  while (crc_acknowledgment_flag == 0 && m_usb_connected == true) {
    while (app_usbd_event_queue_process()) {
    }
    idle_state_handle();
    crc_ack_wait_count++;
    if (crc_ack_wait_count > 1500000) {
      crc_ack_wait_count = 0;
      return 0;
    }
  }
  if (crc_acknowledgment_flag == 1) {
    crc_acknowledgment_flag = 0;
#ifdef DEBUG
    NRF_LOG_INFO(" CRC OK received\r\n");
#endif
    return 1;
  }
  return 0;
}

void update_date(void) {
  if (record_day < 1 || record_day > 31)
    return;
  uint8_t leap_year = 0;
  if ((appYear + 2000) % 4 == 0) {
    leap_year = 1;
  } else {
    leap_year = 0;
  }
  record_day++;
  if (record_month == 1 || record_month == 3 || record_month == 5 || record_month == 7 || record_month == 8 || record_month == 10 || record_month == 12) {
    if (record_day > 31) {
      record_day = 1;
      if (record_month == 12) {
        record_month = 1;
        record_year++;
      } else
        record_month++;
    }
  }
  if (record_month == 2) {
    if (leap_year_flag == 1) {
      if (record_day > 29) {
        record_day = 1;
        record_month++;
      }
    } else {
      if (record_day > 28) {
        record_day = 1;
        record_month++;
      }
    }
  }
  if (record_month == 4 || record_month == 6 || record_month == 9 || record_month == 11) {
    if (record_day > 30) {
      record_day = 1;
      record_month++;
    }
  }
}

void store_sps_subscription(void) {
  static FIL file;
  uint32_t bw = 0;
  char read_str[20];
  char write_str[20];
  FRESULT ff_result;
#ifdef DEBUG
  NRF_LOG_INFO("storing sps and subscription to nand\r\n");
#endif

  ff_result = f_open(&file, "user.txt", FA_READ);
  ff_result = f_read(&file, read_str, 20, &bw); // read 174 bytes of data from file
  ff_result = f_close(&file);
  ff_result = f_lseek(&file, 0);
#ifdef DEBUG
  NRF_LOG_INFO("storing sps and subscription, read string= %s\r\n", read_str);
#endif

  for (uint32_t i = 0; i < 20; i++)
    write_str[i] = read_str[i];

  for (uint32_t i = 0; i < 3; i++)
    write_str[13 + i] = set_sps_subscription[i];
  set_max_sps = set_sps_subscription[0];
  subscription_date = ((set_sps_subscription[1] & 0xff) << 8) | (set_sps_subscription[2] & 0xff);
#ifdef DEBUG
  NRF_LOG_INFO("storing sps and subscription, sps= %d, sub date=%d\r\n", set_max_sps, subscription_date);
#endif

  ff_result = f_unlink("user.txt");
  ff_result = f_open(&file, "user.txt", FA_CREATE_NEW | FA_WRITE);
  ff_result = f_write(&file, write_str, 20, &bw);
  ff_result = f_close(&file);
#ifdef DEBUG
  NRF_LOG_INFO("storing sps and subscription, ff_result= %d\r\n", ff_result);
#endif
}

uint8_t delete_all_data(void) {
  delete_on_progress_flag = 1;
  static FIL file;
  FRESULT ff_result;
  char test_str[20];
  char read_str[20];
  uint32_t fs_size_bytes = 0;
  //char test_value[2000];
  uint32_t bw = 0;
#ifdef DEBUG
  NRF_LOG_INFO("Delete all\r\n");
#endif
  nrf_gpio_pin_set(LED_PIN_BLUE);
  deactivation_led_flag = true;

  ff_result = f_unlink("user.txt"); // delete user details
  memset(test_str, 0xff, 20);
  //memset(test_value,'a',2000);
  ff_result = f_open(&file, "user.txt", FA_CREATE_NEW | FA_WRITE);
  ff_result = f_write(&file, test_str, 20, &bw);
  ff_result = f_close(&file);

  if (ff_result == FR_OK) {
    for (uint32_t i = 0; i < NUMBER_OF_DATA_FILES; i++) { // deleting data
      ff_result = f_unlink(fs_file_name[i]);              // uncomment this line to clear the flash
      nrf_delay_us(400);
    }
    for (uint32_t i = 0; i < NUMBER_OF_DATA_FILES; i++) { // create all files if not initialised and write file number
      ff_result = f_open(&file, fs_file_name[i], FA_CREATE_NEW | FA_WRITE);
      test_str[0] = i;
      ff_result = f_write(&file, test_str, 1, &bw);
      ff_result = f_close(&file);

      nrf_delay_us(400);
    }
    fs_file_num = 0; // point to first file
    fs_file_num_record = fs_file_num;

    /// creating files
    set_max_sps = DEFAULT_MAX_SPS;
    subscription_date = DEFAULT_SUBSCRIPTION_DAYS;

  } else {
#ifdef DEBUG
    NRF_LOG_INFO("no user\r\n");
#endif

    delete_on_progress_flag = 0;
    return 0;
  }
  ff_result = f_open(&file, "user.txt", FA_READ);
  ff_result = f_read(&file, read_str, 20, &bw); // read 174 bytes of data from file
  ff_result = f_close(&file);
  ff_result = f_lseek(&file, 0);

  if (read_str[0] == 0xff) {
  #ifdef DEBUG
      NRF_LOG_INFO("User delete success\r\n");
#endif
    ff_result = f_open(&file, fs_file_name[0], FA_READ);
    ff_result = f_close(&file);
    fs_size_bytes = f_size(&file);
    if (fs_size_bytes < 2) // if file not having any data other than file number, select this file to start writing data
    {

#ifdef DEBUG
      NRF_LOG_INFO("delete success\r\n");
#endif
      deactivation_led_flag = false;
      nrf_gpio_pin_clear(LED_PIN_BLUE);
      nrf_gpio_pin_clear(LED_PIN_GREEN);
      nrf_gpio_pin_clear(LED_PIN_RED);
      delete_on_progress_flag = 0;
      return 1;

    } else
      return 0;
  } else
    return 0;
}

uint8_t delete_data(void) {
  static FIL file;
  FRESULT ff_result;
  char test_str[20];
  uint32_t bw = 0;
  delete_on_progress_flag = 1;

  nrf_gpio_pin_set(LED_PIN_BLUE);
  deactivation_led_flag = true;

  for (uint32_t i = 0; i < NUMBER_OF_DATA_FILES; i++) { // deleting data
    ff_result = f_unlink(fs_file_name[i]);              // uncomment this line to clear the flash
    nrf_delay_ms(1);
  }

  /////// creating files

  for (uint32_t i = 0; i < NUMBER_OF_DATA_FILES; i++) { // create all files if not initialised and write file number
    ff_result = f_open(&file, fs_file_name[i], FA_CREATE_NEW | FA_WRITE);
    test_str[0] = i;
    ff_result = f_write(&file, test_str, 1, &bw);
    ff_result = f_close(&file);

    nrf_delay_ms(1);
  }
  fs_file_num = 0; // point to first file
  fs_file_num_record = fs_file_num;
  deactivation_led_flag = false;
  nrf_gpio_pin_clear(LED_PIN_BLUE);
  nrf_gpio_pin_clear(LED_PIN_GREEN);
  nrf_gpio_pin_clear(LED_PIN_RED);
  delete_on_progress_flag = 0;
  return 1;
}

uint8_t find_file_number(void) {
  static FIL file;
  FRESULT ff_result;
  char test_str[20];
  uint32_t bw = 0;
  uint32_t fs_size_bytes = 0;
  uint8_t temp_num = 0;
  for (uint32_t i = 0; i < NUMBER_OF_DATA_FILES; i++) {
    ff_result = f_open(&file, fs_file_name[i], FA_READ);
    ff_result = f_close(&file);
    fs_size_bytes = f_size(&file);
    if (fs_size_bytes < 2) // if file not having any data other than file number, select this file to start writing data
    {
      temp_num = i; // set the file to write data
      break;
    }
    nrf_delay_ms(1);
  }
  return temp_num;
}

void set_otg_upload_status(uint8_t status) {
  static FIL file;
  uint32_t bw = 0;
  char read_str[20];
  char write_str[20];
  FRESULT ff_result;

#ifdef DEBUG
  NRF_LOG_INFO(" otg upload status\r\n");
#endif

  ff_result = f_open(&file, "user.txt", FA_READ);
  ff_result = f_read(&file, read_str, 20, &bw); // read 174 bytes of data from file
  ff_result = f_close(&file);
  ff_result = f_lseek(&file, 0);
#ifdef DEBUG
  NRF_LOG_INFO(" otg upload status, read string= %s\r\n", read_str);
#endif

  for (uint32_t i = 0; i < 20; i++)
    write_str[i] = read_str[i];
  if (status == 1) {
    write_str[19] = 'Y';
  } else {
    write_str[19] = 'N';
  }

#ifdef DEBUG
  NRF_LOG_INFO(" otg upload status, status= %d", write_str[19]);
#endif
  ff_result = f_unlink("user.txt");
  ff_result = f_open(&file, "user.txt", FA_CREATE_NEW | FA_WRITE);
  ff_result = f_write(&file, write_str, 20, &bw);
  ff_result = f_close(&file);
#ifdef DEBUG
  NRF_LOG_INFO(" otg upload status, ff_result= %d\r\n", ff_result);
#endif
  otg_data_upload_flag = status;
  return;
}

void led_blink(uint32_t pin_number1, uint32_t pin_number2, uint32_t times) {
  uint16_t i;
  for (i = 0; i < times; i++) {
    nrf_gpio_pin_set(pin_number1);
    nrf_gpio_pin_set(pin_number2);
    nrf_delay_ms(100);
    nrf_gpio_pin_clear(pin_number1);
    nrf_gpio_pin_clear(pin_number2);
    nrf_delay_ms(100);
  }
}