/*
  File : NAND Disk
  Author : Dvizma Sinha
  Description : File System Layer, integration with FATFS
*/

#include "diskio.h"
#include "map.h"

#define GC_RATIO 2

/* Dhara Map Structure
 * Contains dhara_nand structure, page_buffer
 */
typedef struct _dhara_map
{
    struct dhara_nand n;
    uint8_t page_buf[4096];
} dhara_handle_t;

struct dhara_map map;
dhara_handle_t dhara_nand_handle;

/* Function : Nand custom disk init
 * Description : Initialize nand disk, and dhara nand handle
 * Return : DRESULT
 */

DRESULT NAND_custom_disk_init()
{
    int status;
    status = dhara_nand_init(&(dhara_nand_handle.n));
    dhara_map_init(&map, &(dhara_nand_handle.n), dhara_nand_handle.page_buf, GC_RATIO);
    dhara_map_resume(&map, NULL);
    return RES_OK;
}

/* Function : Nand custom disk status
 * Description : Get NAND disk status
 * Return : DRESULT
 */
DRESULT NAND_custom_disk_status() 
{ 
  return RES_OK; 
}


/* Function :     Nand custom disk Read
 * Description :  Read nand disk 
 * Arguments :    buff : Buffer to store data
 *                sector : sector ID
 *                count : number of bytes to read
 * Return :       DRESULT
 */
DRESULT NAND_custom_disk_read(uint8_t * buff, uint32_t sector, uint32_t count)
{
    dhara_error_t err;
    if (dhara_map_read(&map, sector, buff, &err) < 0)
    {
        return RES_ERROR;
    }
    return RES_OK;
}

/* Function :     Nand custom disk write
 * Description :  write to nand disk 
 * Arguments :    buff : Buffer to store data
 *                sector : sector ID
 *                count : number of bytes to write
 * Return :       DRESULT
 */
DRESULT NAND_custom_disk_write(uint8_t * buff, uint32_t sector, uint32_t count)
{
    dhara_error_t err;
    if (dhara_map_write(&map, sector, buff, &err) < 0)
    {
        return RES_ERROR;
    }
    return RES_OK;
}


/* Function :     Nand custom disk ioctl
 * Description :  write to nand disk 
 * Arguments :    buff : Buffer to store data
 *                command : command to execute
 * Return :       DRESULT
 */
DRESULT NAND_custom_disk_ioctl(uint8_t command, uint32_t * buff)
{
    DRESULT result = RES_OK;
    DWORD sector;
    dhara_error_t err;

    switch (command)
    {
        case GET_SECTOR_COUNT:
            *buff = dhara_map_capacity(&map);

            break;
        case GET_SECTOR_SIZE:
            *buff = 1 << dhara_nand_handle.n.log2_page_size;
            break;
        case GET_BLOCK_SIZE:
            *buff = 1 << dhara_nand_handle.n.log2_ppb;
            break;
        case CTRL_SYNC:
            if (dhara_map_sync(&map, NULL) != 0U)
            {
                result = RES_ERROR;
            }
            else
            {
                result = RES_OK;
            }
            break;
        case CTRL_TRIM:
            for (sector = ((DWORD *)buff)[0]; sector <= ((DWORD *)buff)[1]; sector++)
            {
                if (dhara_map_trim(&map, sector, &err) < 0)
                {
                    result = RES_ERROR;
                    break;
                }
            }
            break;

        default:
            result = RES_PARERR;
            break;
    }
    return result;
}