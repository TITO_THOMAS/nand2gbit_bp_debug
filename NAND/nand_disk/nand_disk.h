/*
  File : NAND Disk
  Author : Dvizma Sinha
  Description : File System Layer, integration with FATFS
*/

#include "diskio.h"
#include "map.h"

DRESULT NAND_custom_disk_init();
DRESULT NAND_custom_disk_status();
DRESULT NAND_custom_disk_read(uint8_t * buff, uint32_t sector, uint32_t count);
DRESULT NAND_custom_disk_write(uint8_t * buff, uint32_t sector, uint32_t count);
DRESULT NAND_custom_disk_ioctl( uint8_t command,uint8_t * buff);

