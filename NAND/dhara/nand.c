/*
 *  File : nand.c
 *  Author : Dvizma Sinha
 *  Description : Dhara Flash translation layer interface to Nand hardware abstraction layer
 */

#include "nand.h"

/* Function :   Dhara NAND Init
 * Arguements : n - pointer to dhara nand structure
 * Description : Function initializes nand device and structure
 */
int dhara_nand_init(struct dhara_nand * n)
{
    if (nand_sw_init(&toshiba) == NAND_ID_NOT_MATCH)
    {
        NRF_LOG_INFO("!!!! ID Not Match !!!");
        while (1)
        {
            nrf_gpio_pin_toggle(25);
            nrf_delay_ms(100);
        }
    }
    n->log2_page_size = 12;
    n->log2_ppb = 6;
    n->num_blocks = 2048;
  //  nand_scan_bad_block(&toshiba);
}

/* Function :   Dhara NAND is bad
 * Arguements : n - pointer to dhara nand structure
 *              b - block to be checked (0 to 2047)
 * Description : Function returns bad block value for a given block from the BBT
 */
int dhara_nand_is_bad(const struct dhara_nand * n, dhara_block_t b)
{
    if (toshiba.bad_block_table[b])
        return true;
    return false;
}

/* Function :   Dhara NAND mark bad
 * Arguements : n - pointer to dhara nand structure
 *              b - block to be checked (0 to 2047)
 * Description : Function marks the given block as bad
 */
void dhara_nand_mark_bad(const struct dhara_nand * n, dhara_block_t b)
{
    toshiba.bad_block_table[b] = true;
}

/* Function :   Dhara NAND Erase
 * Arguements : n - pointer to dhara nand structure
 *              b - block to be checked (0 to 2047)
 *              err - dhara error return
 * Return     : DHARA_SUCCESS
 *              DHARA_FAILURE
 * Description : Function marks the given block as bad
 */
int dhara_nand_erase(const struct dhara_nand * n, dhara_block_t b, dhara_error_t * err)
{
    *err = DHARA_E_NONE;
    if (toshiba.bad_block_table[b])
    {
        *err = DHARA_E_BAD_BLOCK;
        return DHARA_FAILURE;
    }
    uint8_t status = nand_block_erase(&toshiba, b);
    if (status == NAND_SUCCESS)
        return DHARA_SUCCESS;
    return DHARA_FAILURE;
}


/* Function :   Dhara NAND Program
 * Arguements : n - pointer to dhara nand structure
 *              p - page to be programmed (0 to 2047)
 *              err - dhara error return
 *              data - buffer to be written
 * Return     : DHARA_SUCCESS
 *              DHARA_FAILURE
 * Description : Function programs a page in the nand flash
 */
int dhara_nand_prog(
    const struct dhara_nand * n, dhara_page_t p, uint8_t * data, dhara_error_t * err)
{
    int status;
    *err = DHARA_E_NONE;
    status = nand_write_enable(&toshiba);
    if (status != NAND_SUCCESS)
        return DHARA_FAILURE;
    int block_addr = p >> (toshiba.page_bits);
    if (toshiba.bad_block_table[block_addr])
    {
        *err = DHARA_E_BAD_BLOCK;
        return DHARA_FAILURE;
    }
    status = nand_program_load(&toshiba, toshiba.page_size, data);
    if (status != NAND_SUCCESS)
        return DHARA_FAILURE;
    status = nand_program_execute(&toshiba, p);
    return status;
}


/* Function :   Dhara NAND is free
 * Arguements : n - pointer to dhara nand structure
 *              p - page to be programmed (0 to 2047)
 * Return     : DHARA_SUCCESS
 *              DHARA_FAILURE
 * Description : Function reads a page from flash and checks if all the bytes are 0xFF.
 */

int dhara_nand_is_free(const struct dhara_nand * n, dhara_page_t p)
{
    int status;
    char buff[4096];
    status = nand_page_to_cache(&toshiba, p);
    if (status == NAND_ECC_BIT_FLIP_NCOR)
    {
        return DHARA_FAILURE;
    }
    status = nand_read_cache(&toshiba, 0, toshiba.page_size, buff);
    if (status != SUCCESS)
        return status;
    char * ptr = buff;
    for (int i = 0; i < 4096; i++)
    {
        if (*ptr != 0xFF)
            return false;
        ptr++;
    }
    return true;
}


/* Function :   Dhara NAND Read
 * Arguements : n - pointer to dhara nand structure
 *              p - page to be programmed (17 bits)
 *              err - dhara error return
 *              data - buffer in which data is to be read
 *              offset - read start address
 * Return     : DHARA_SUCCESS
 *              DHARA_FAILURE
 * Description : Function reads data from a page
 */
int dhara_nand_read(const struct dhara_nand * n, dhara_page_t p, size_t offset, size_t length,
    uint8_t * data, dhara_error_t * err)
{
    int status;
    *err = DHARA_E_NONE;
    status = nand_page_to_cache(&toshiba, p);
    if (status == NAND_ECC_BIT_FLIP_NCOR)
    {
        *err = DHARA_E_ECC;
        return DHARA_FAILURE;
    }
    status = nand_read_cache(&toshiba, offset, length, data);
    if (status != SUCCESS)
        return status;
}


/* Function :   Dhara NAND Copy
 * Arguements : n - pointer to dhara nand structure
 *              src - source page
 *              err - dhara error return
 *              dst - desitination page
 * Return     : DHARA_SUCCESS
 *              DHARA_FAILURE
 * Description : Function copies a page using internal buffer
 */
int dhara_nand_copy(
    const struct dhara_nand * n, dhara_page_t src, dhara_page_t dst, dhara_error_t * err)
{
    int status;
    uint32_t block_addr;
    *err = DHARA_E_NONE;
    block_addr = dst >> toshiba.page_bits;
    if (toshiba.bad_block_table[block_addr])
    {
        *err = DHARA_E_BAD_BLOCK;
        return DHARA_FAILURE;
    }
    status = nand_page_to_cache(&toshiba, src);
    if (status == NAND_ECC_BIT_FLIP_NCOR)
    {
        *err = DHARA_E_ECC;
        return DHARA_FAILURE;
    }
    status = nand_write_enable(&toshiba);
    if (status != NAND_SUCCESS)
    {
        return DHARA_FAILURE;
    }
    status = nand_program_execute(&toshiba, dst);
    if (status != NAND_SUCCESS)
    {
        return DHARA_FAILURE;
    }
    return DHARA_SUCCESS;
}