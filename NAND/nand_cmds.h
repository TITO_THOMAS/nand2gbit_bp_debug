/*
  File : NAND Commands
  Author : Dvizma Sinha
  Description : Flash commands header files
*/

#ifndef _FLASH_CMDS
#define _FLASH_CMDS

#include "spi_functions.h"
#include "utils.h"

#define NUM_BLOCKS        2048
/* NAND Status */
#define  NAND_SUCCESS                 0L
#define	 NAND_FAILURE                 1L
#define	 NAND_TIMEOUT                 2L
#define	 NAND_DEVICE_IS_STARTED       3L
#define	 NAND_DEVICE_IS_STOPPED       4L
#define	 NAND_ID_NOT_MATCH            5L
#define	 NAND_DEVICE_BUSY             6L	/* device is busy */
#define	 NAND_DEVICE_READY            7L	/* device is ready */
#define	 NAND_DEVICE_PROGRAM_FAILED   10L
#define	 NAND_DEVICE_ERASE_FAILED     11L
#define	 NAND_FLASH_QIO_NOT_ENABLE    12L
#define	 NAND_FLASH_QIO_ENABLE        13L
#define	 EBADMSG		      14L  /* status about ECC  */
#define	 EINVAL			      15L  /* status about ECC  */
#define	 ENOMEM			      16L
#define	 ENOSPC			      17L
#define	 EUCLEAN		      18L
#define	 NAND_BLOCK_IS_BAD	      19L
#define	 NAND_BLOCK_IS_NOT_BAD	      20L


#define NAND_ECC_NO_BIT_FLIP          21L
#define NAND_ECC_BIT_FLIP_COR         22L
#define NAND_ECC_BIT_FLIP_COR_THE     23L
#define NAND_ECC_BIT_FLIP_NCOR        24L

/* NAND Info Structure */

typedef struct _NAND_CHIP
{
    uint8_t *name;
    uint8_t  id[2];
    uint32_t chip_size;
    uint32_t block_size;
    uint32_t chip_special_flag;
    uint32_t page_size;
    uint16_t oob_size;
    uint32_t oob_available;
    uint8_t *oob_buffer_ptr;
    uint8_t page_shift;
    uint8_t ecc_mode;
    bool bad_block_table[NUM_BLOCKS];
    uint16_t num_bb;
    uint8_t block_bits;
    uint8_t page_bits;
    uint8_t column_bits;
}NAND_CHIP;


/* NAND Commands */
#define NAND_CMD_READ_ID		0x9F		/* Read Identification */
#define NAND_CMD_READ			0x13
#define NAND_CMD_READ_CACHE		0x0B
#define NAND_CMD_READ_CACHE2		0x3B
#define NAND_CMD_READ_CACHE4		0x6B
#define NAND_CMD_READ_CACHE_SEQUENTIAL  0x31
#define NAND_CMD_READ_CACHE_END		0x3F

#define NAND_CMD_GET_FEATURE		0x0F
#define NAND_CMD_SET_FEATURE		0x1F

#define NAND_CMD_WREN                   0x06
#define NAND_CMD_WRDI                   0x04
#define NAND_CMD_PP_LOAD		0x02
#define NAND_CMD_PP_RAND_LOAD		0x84
#define NAND_CMD_4PP_LOAD		0x32
#define NAND_CMD_4PP_RAND_LOAD	        0x34
#define NAND_CMD_PROGRAM_EXEC		0x10

#define NAND_CMD_BLOCK_ERASE		0xD8
#define NAND_CMD_RESET 0xFF
#define NAND_CMD_ECC_STAT_READ		0x7C


/*Feature Address*/
#define STATUS_ADDR                     0xC0
#define BL_ADDR                         0xA0

/*Status Bits*/
#define OIP_BIT                 0x01
#define WRITE_EN_BIT            0x02
#define ERASE_FAIL_BIT          0x04
#define PROGRAM_FAIL_BIT        0x08
#define ECC_S0_BIT              0x10
#define ECC_S1_BIT              0x20
#define BL_BIT_MASK             0x38
#define RESET_BL_MASK           0xC7

/*NAND Timeouts in uS*/
#define BLOCK_ERASE_TIMEOUT    1000000
#define NAND_READ_PAGE_TIMEOUT 1000000
#define NAND_PROG_EXEC_TIMEOUT 1000000

/*Function Prototypes */

int nand_sw_init(NAND_CHIP *nand_chip);
int nand_read_id(uint8_t *temp_id);
int nand_get_status(NAND_CHIP *nand_chip);
int nand_get_feature(NAND_CHIP *nand_chip, uint32_t addr, uint8_t *buff);
int nand_set_feature(NAND_CHIP *nand_chip, uint32_t addr, uint8_t *buff);
int nand_block_erase(NAND_CHIP *nand_chip, uint32_t block_addr);
void nand_scan_bad_block(NAND_CHIP *nand_chip);
int nand_write_enable(NAND_CHIP *nand_chip);
int nand_write_disable(NAND_CHIP *nand_chip);
int nand_wait_for_flash_ready(NAND_CHIP *nand_chip, uint32_t timeout);
int nand_page_to_cache(NAND_CHIP *nand_chip,uint32_t page_addr);
int nand_get_ecc_status(NAND_CHIP *nand_chip);
int nand_read_cache(NAND_CHIP *nand_chip, uint32_t column_addr, uint32_t len, uint8_t *buff);
int nand_program_load(NAND_CHIP *nand_chip, uint32_t len,const uint8_t *buff);
int nand_program_execute(NAND_CHIP *nand_chip, uint32_t page_addr);
int nand_reset(NAND_CHIP *nand_chip);
#endif