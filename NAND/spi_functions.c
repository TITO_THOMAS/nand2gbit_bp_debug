/*
   Date 20-03-2019
   File - SPI_Functions
   Description - Uniform interface file to call SPI funtions.
 */

#include "spi_functions.h"
#include "nrf_gpio.h"

/*Private Defines */

uint8_t len_addr, len_dummy, len_cmd = 1;
volatile bool spi_xfer_done;
const nrf_drv_spi_t spi_flash = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);

/*
 * Function:      addr_to_cmd
 * Arguments:	  addr,   the address to put into the send data buffer.
 *                cmd_buff, the data will be send to controller.
 * Return Value:  None.
 * Description:   This function put the value of address into the send data
 * buffer. The address is stored after the command code.
 */
static void addr_to_cmd(uint32_t addr, uint8_t * cmd_buff)
{
    int i;
    for (i = 1; i <= len_addr; i++)
        cmd_buff[i] = addr >> ((len_addr - i) * 8);
}

/*
 * Function:      spi_flash_write
 * Arguments:	  _spi,       spi instance
 *                addr,      address to be written to.
 * 		  byte_count, number of byte to write.
 *                wr_buff,     Pointer to a data buffer where the write data
 * will be stored. wr_cmd,     write command code to be written to the flash.
 * Return Value:  SUCCESS.
 *                FAILURE.
 * Description:   This function prepares the data to be written and put them
 * into data buffer, then call nrf_drv_spi_transfer function to start a write
 * data transfer.
 */

ret_code_t spi_flash_write(nrf_drv_spi_t const * const _spi, uint32_t addr, uint32_t byte_count,
    uint8_t * wr_buff, uint8_t wr_cmd)
{
    uint8_t * ptr = write_buffer;
    uint8_t iter, rem, i;
    uint32_t len_inst;
    uint32_t transfer_bytes;
    ret_code_t status;
    len_inst = len_cmd + len_addr;
    write_buffer[0] = wr_cmd;
    addr_to_cmd(addr, write_buffer);
    memcpy(write_buffer + len_inst, wr_buff, byte_count);
    transfer_bytes = byte_count + len_inst;
    iter = transfer_bytes / SPI_MAX_DATA_SIZE;
    rem = transfer_bytes % SPI_MAX_DATA_SIZE;
    //NRF_LOG_INFO("To Transfer %u Iter %u Rem %u",transfer_bytes,iter,rem);
    nrf_gpio_pin_clear(FLASH_SPI_SS_PIN);
    for (i = 0; i <= iter; i++)
    {
        spi_xfer_done = false;
        if (i == iter)
        {
            status = nrf_drv_spi_transfer(_spi, ptr, rem, read_buffer, rem);
        }
        else
        {
            status =
                nrf_drv_spi_transfer(_spi, ptr, SPI_MAX_DATA_SIZE, read_buffer, SPI_MAX_DATA_SIZE);
            ptr = ptr + SPI_MAX_DATA_SIZE;
        }
        while (!spi_xfer_done)
        {
            __WFE();
        }
        if (status != NRF_SUCCESS)
            return FAILURE;
    }
    nrf_gpio_pin_set(FLASH_SPI_SS_PIN);
    return SUCCESS;
}

/*
 * Function:      spi_flash_read
 * Arguments:	  _spi,       spi instance
 *                addr,      address to be written to.
 * 		  byte_count, number of byte to write.
 *                rd_buff,     Pointer to a data buffer where the write data
 * will be stored. rd_cmd,     write command code to be written to the flash.
 * Return Value:  SUCCESS.
 *                FAILURE.
 * Description:   This function calls nrf_drv_spi_transfer to start a read data
 * transfer and puts the data into data buffer
 */

ret_code_t spi_flash_read(nrf_drv_spi_t const * const _spi, uint32_t addr, uint32_t byte_count,
    uint8_t * rd_buff, uint8_t rd_cmd)
{
    uint32_t len_inst;
    uint32_t transfer_bytes;
    uint16_t iter, rem;
    ret_code_t status;
    uint8_t * ptr = read_buffer;
    len_inst = len_cmd + len_addr + len_dummy;
    write_buffer[0] = rd_cmd;
    addr_to_cmd(addr, write_buffer);
    if (len_dummy)
        write_buffer[1 + len_addr] = 0xFF;
    transfer_bytes = byte_count + len_inst;
    iter = transfer_bytes / SPI_MAX_DATA_SIZE;
    rem = transfer_bytes % SPI_MAX_DATA_SIZE;
    //NRF_LOG_INFO("To Transfer %u Iter %u Rem %u",transfer_bytes,iter,rem);
    nrf_gpio_pin_clear(FLASH_SPI_SS_PIN);
    for (uint8_t i = 0; i <= iter; i++)
    {
        spi_xfer_done = false;
        //NRF_LOG_INFO("Iter %d",i);
        if (i == iter)
        {
            status = nrf_drv_spi_transfer(_spi, write_buffer, rem, ptr, rem);
        }
        else
        {
            status =
                nrf_drv_spi_transfer(_spi, write_buffer, SPI_MAX_DATA_SIZE, ptr, SPI_MAX_DATA_SIZE);
            ptr = ptr + SPI_MAX_DATA_SIZE;
        }
        while (!spi_xfer_done)
        {
            __WFE();
        }
        if (status != NRF_SUCCESS)
            return FAILURE;
    }
    nrf_gpio_pin_set(FLASH_SPI_SS_PIN);
    memcpy(rd_buff, read_buffer + len_inst, byte_count);
    return SUCCESS;
}

/**
 * @brief SPI user event handler.
 * @param event
 */
void spi_event_handler(nrf_drv_spi_evt_t const * p_event, void * p_context)
{
    spi_xfer_done = true;
    //NRF_LOG_DEBUG("Transfer completed.");
}

/*
  Function  : spi_flash_init
  Description : Initializes SPI peripheral
*/

void spi_flash_init()
{
    nrf_gpio_cfg_output(FLASH_SPI_SS_PIN);
    nrf_gpio_pin_set(FLASH_SPI_SS_PIN);
    nrf_delay_ms(400);
    nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
    spi_config.ss_pin = NULL;//NULL;
    spi_config.miso_pin = FLASH_SPI_MISO_PIN;
    spi_config.mosi_pin = FLASH_SPI_MOSI_PIN;
    spi_config.sck_pin = FLASH_SPI_SCK_PIN;
    spi_config.frequency = NRF_DRV_SPI_FREQ_8M;
    APP_ERROR_CHECK(nrf_drv_spi_init(&spi_flash, &spi_config, spi_event_handler, NULL));

}