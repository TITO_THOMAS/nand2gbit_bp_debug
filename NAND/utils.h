/*
   Date 20-03-2019
   File - SPI_Functions
   Description - Utilities functions header file
 */

uint8_t ffs(uint32_t x);