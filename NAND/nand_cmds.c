/* File : Nand Commands
   Author : Dvizma Sinha
   Description : Functions for executing nand commands
*/

#include "nand_cmds.h"

/*
  Function : NAND Software Init
  Arguments : nand_chip - pointer to NAND_CHIP structure
              effective_address - Base Address
  Return    : NAND_SUCCESS
              NAND_FAILURE
  Description : This function is for initializing the variables
*/

int nand_sw_init(NAND_CHIP * nand_chip)
{
    uint8_t temp_id[2], feature;
    uint8_t status;
    nand_chip->name = "TC58CYG2S0HRAIG";
    nand_chip->id[0] = 0x98;
    nand_chip->id[1] = 0xBD;
    nand_chip->page_size = 4096;
    nand_chip->oob_size = 128;
    nand_chip->chip_size = 4096 * 64 * 2048;
    nand_chip->block_size = 4096 * 64;
    nand_chip->page_shift = ffs(nand_chip->page_size) - 1;
    nand_chip->block_bits = 11;
    nand_chip->page_bits = 6;
    nand_chip->column_bits = 13;
    status = nand_read_id(temp_id);
    if ((temp_id[0] != nand_chip->id[0]) || (temp_id[1] != nand_chip->id[1]))
    {
        NRF_LOG_INFO("Read Id 0x%x%x, Expected 0x%x%x, ID mismatch", temp_id[0], temp_id[1]);
        return NAND_ID_NOT_MATCH;
    }
    NRF_LOG_INFO("NAND ID Match");
    NRF_LOG_INFO("NAND %s, ID 0x%x%x",nand_chip->name,nand_chip->id[0], nand_chip->id[1]);
    NRF_LOG_INFO("Page size %u, OOB size %u", nand_chip->page_size, nand_chip->oob_size);
    NRF_LOG_INFO("Block Size %u, chip size %u", nand_chip->block_size, nand_chip->chip_size);
    nand_get_feature(nand_chip,BL_ADDR,&feature);
    if(feature & BL_BIT_MASK)
    {
        //Important : Reset BL bits
        feature = feature & (RESET_BL_MASK);
        nand_set_feature(nand_chip,BL_ADDR,&feature);
    }
    return NAND_SUCCESS;
}

/*
  Function : NAND Software Init
  Arguments : nand_chip - pointer to NAND_CHIP structure
              temp_id - pointer to read_id
  Return    : NAND_SUCCESS
              NAND_FAILURE
  Description : This function is reads ID from nand chip
*/
int nand_read_id(uint8_t * temp_id)
{
    len_addr = 0;
    len_dummy = 1;
    return spi_flash_read(&spi_flash, 0, 2, temp_id, NAND_CMD_READ_ID);
}

/*
  Function : NAND Get Feature
  Arguments : nand_chip - pointer to NAND_CHIP structure
              addr - Address to read
              buff - pointer to read buffer
  Return    : NAND_SUCCESS
              NAND_FAILURE
  Description : This function reads feature settings
*/
int nand_get_feature(NAND_CHIP * nand_chip, uint32_t addr, uint8_t * buff)
{
    len_addr = 1;
    len_dummy = 0;
    return spi_flash_read(&spi_flash, addr, 1, buff, NAND_CMD_GET_FEATURE);
}


/*
  Function : NAND Get Feature
  Arguments : nand_chip - pointer to NAND_CHIP structure
              addr - Address to write
              buff - pointer to write buffer
  Return    : NAND_SUCCESS
              NAND_FAILURE
  Description : This function writes feature settings
*/
int nand_set_feature(NAND_CHIP * nand_chip, uint32_t addr, uint8_t * buff)
{
    len_addr = 1;
    len_dummy = 0;
    return spi_flash_write(&spi_flash, addr, 1, buff, NAND_CMD_SET_FEATURE);
}


/*
 * Function:      nand get status
 * Arguments:	  nand_chip - pointer to NAND_CHIP structure
 * Return Value:  NAND_SUCCESS.
 *                NAND_FAILURE.
 *                NAND_DEVICE_BUSY.
 *                NAND_DEVICE_READY.
 *                NAND_DEVICE_PROGRAM_FAILED.
 *                NAND_DEVICE_ERASE_FAILED.
 * Description:   This function is for checking status register bits
 */
int nand_get_status(NAND_CHIP * nand_chip)
{
    int status;
    uint8_t status_reg;
    status = nand_get_feature(nand_chip, STATUS_ADDR, &status_reg);
    if (status != SUCCESS)
    {
        NRF_LOG_INFO("Status Fail");
        return status;
    }
    if (status_reg & OIP_BIT)
    {
      //NRF_LOG_INFO("Status Operation in progress");
        return NAND_DEVICE_BUSY;
    }
    if (status_reg & PROGRAM_FAIL_BIT)
    {
       NRF_LOG_INFO("Status Program Fail");
        return NAND_DEVICE_PROGRAM_FAILED;
    }
    if (status_reg & ERASE_FAIL_BIT)
    {
        NRF_LOG_INFO("Status Erase Fail");
        return NAND_DEVICE_ERASE_FAILED;
    }
   // NRF_LOG_INFO("Status Ready");
    return NAND_DEVICE_READY;
}

/*
 * Function:      nand write enable
 * Arguments:	  nand_chip - pointer to NAND_CHIP structure
 * Return Value:  SUCCESS.
 *                FAILURE.
 * Description:   This function is for setting write enable latch bit.
 */

int nand_write_enable(NAND_CHIP * nand_chip)
{
    len_addr = 0;
    len_dummy = 0;
    return spi_flash_write(&spi_flash, 0, 0, 0, NAND_CMD_WREN);
}

/*
 * Function:      nand erase block
 * Arguments:	  nand_chip - pointer to NAND_CHIP structure
 *                block addr, block address to erase
 * Return Value:  SUCCESS.
 *                FAILURE.
 *                TIMEOUT.
 * Description:   This function is for erasing a block
 */
int nand_block_erase(NAND_CHIP * nand_chip, uint32_t block_addr)
{
    int status;
    status = nand_write_enable(nand_chip);
    if (status != SUCCESS)
        return status;
    if(nand_chip->bad_block_table[block_addr])
    {
        //NRF_LOG_INFO("Bad Block Erase Not Attempted");
        return NAND_FAILURE;
    }
    len_addr = 3;
    len_dummy = 0;
    block_addr = block_addr << nand_chip->page_bits;
    status = spi_flash_write(&spi_flash, block_addr, 0, 0, NAND_CMD_BLOCK_ERASE);
    if (status != SUCCESS)
        return status;
    return nand_wait_for_flash_ready(nand_chip, BLOCK_ERASE_TIMEOUT);
}


/*
 * Function:      Nand wait for flash ready
 * Arguments:	  nand_chip,       pointer to an Nand device structure
 *                timeout, expected time-out value of flash operations.
 * Return Value:  NAND_SUCCESS.
 *                NAND_FAILURE.
 *                NAND_TIMEOUT.
 * Description:   If flash is ready return NAND_SUCCESS. If flash is time-out return NAND_TIMEOUT.
 */
int nand_wait_for_flash_ready(NAND_CHIP * nand_chip, uint32_t timeout)
{
    timeout = timeout / 10;
    while (nand_get_status(nand_chip) != NAND_DEVICE_READY)
    {
        nrf_delay_us(5);
        timeout--;
        if (!timeout)
        {
            NRF_LOG_INFO("Status Timeout");
            return NAND_TIMEOUT;
        }
    }
    return NAND_SUCCESS;
}


/*
 * Function:      Nand Scan for bad blocks
 * Arguments:	  nand_chip,       pointer to an Nand device structure
 * Return Value:  NAND_SUCCESS.
 *                NAND_FAILURE.
 * Description:   Scan for bad blocks and mark them in Bad block table.
 */
void nand_scan_bad_block(NAND_CHIP * nand_chip)
{
    int i;
    int ok, bad = 0;
    uint8_t buff;
  //  uint8_t page[]
    for (i = 0; i < NUM_BLOCKS; i++)
    {
        nand_chip->bad_block_table[i] = false;
        nand_page_to_cache(nand_chip, i << (nand_chip->page_bits));
        nand_read_cache(nand_chip,nand_chip->page_size, 1, &buff);
        if (buff != 0xFF)
        {
            bad++;
            nand_chip->bad_block_table[i] = true;
            //NRF_LOG_INFO("Bad Block at %d",i);
        }
       
    }
     NRF_LOG_INFO("\nBad Blocks %d", bad);
}
/*
 * Function:      Nand Read Full Page to Internal Buffer
 * Arguments:	  nand_chip,       pointer to an Nand device structure
 *                page_addr        page number to read (17 bits)
 * Return Value:  NAND_SUCCESS.
 *                NAND_FAILURE.
 *                NAND_DEVICE_BUSY
 * Description:   Read one page to program cache
 */
int nand_page_to_cache(NAND_CHIP * nand_chip, uint32_t page_addr)
{
    int status;
    if (nand_get_status(nand_chip) == NAND_DEVICE_BUSY)
        return NAND_DEVICE_BUSY;
    len_addr = 3;
    len_dummy = 0;
    status = spi_flash_write(&spi_flash, page_addr, 0, 0, NAND_CMD_READ);
    if (status != SUCCESS)
        return NAND_FAILURE;
    status = nand_wait_for_flash_ready(nand_chip, NAND_READ_PAGE_TIMEOUT);
    if (status != NAND_SUCCESS)
        return status;
    return nand_get_ecc_status(nand_chip);
}


/*
 * Function:      Nand get internal ECC status
 * Arguments:	  nand_chip,       pointer to an Nand device structure
 *                page_addr        page number to read (17 bits)
 * Return Value:  NAND_ECC_NO_BIT_FLIP.
 *                NAND_ECC_BIT_FLIP_COR.
 *                NAND_ECC_BIT_FLIP_COR_THE.
 *                NAND_ECC_BIT_FLIP_NCOR.
 * Description:   Checks ECC status bits and returns analysis.
 */
int nand_get_ecc_status(NAND_CHIP * nand_chip)
{
    uint8_t status_reg;
    int status = nand_get_feature(nand_chip, STATUS_ADDR, &status_reg);
    if (status != NAND_SUCCESS)
        return status;
    status_reg = (status & (ECC_S0_BIT | ECC_S1_BIT)) >> 4;
    switch (status)
    {
        case 0x00:
            return NAND_ECC_NO_BIT_FLIP;
        case 0x01:
            return NAND_ECC_BIT_FLIP_COR;
        case 0x10:
            return NAND_ECC_BIT_FLIP_NCOR;
        case 0x11:
            return NAND_ECC_BIT_FLIP_COR_THE;
    }
}

/*
 * Function:      Nand ReadInternal Buffer
 * Arguments:	  nand_chip,       pointer to an Nand device structure
 *                column_addr      column address to start read (13 bits)
 *                len              number of bytes to read
 *                buff             data buffer for output
 * Return Value:  NAND_SUCCESS.
 *                NAND_FAILURE.
 * Description:   Read data from cache
 */
int nand_read_cache(NAND_CHIP * nand_chip, uint32_t column_addr, uint32_t len, uint8_t * buff)
{
    len_addr = 2;
    len_dummy = 1;
    return spi_flash_read(&spi_flash, column_addr, len, buff, NAND_CMD_READ_CACHE);
}

/*
 * Function:      Nand load data in cache
 * Arguments:	  nand_chip,       pointer to an Nand device structure
 *                len              number of bytes to read
 *                buff             data buffer for output
 * Return Value:  NAND_SUCCESS.
 *                NAND_FAILURE.
 * Description:   Function loads data buffer in internal flash
 */
int nand_program_load(NAND_CHIP * nand_chip, uint32_t len,const uint8_t * buff)
{
    int status;
    if (nand_get_status(nand_chip) == NAND_DEVICE_BUSY)
        return NAND_DEVICE_BUSY;
    status = nand_write_enable(nand_chip);
    if (status != NAND_SUCCESS)
        return NAND_FAILURE;
    len_addr = 2;
    len_dummy = 0;
    return spi_flash_write(&spi_flash, 0, len, buff, NAND_CMD_PP_LOAD);
}

/*
 * Function:      Nand load data in cache
 * Arguments:	  nand_chip,       pointer to an Nand device structure
 *                len              number of bytes to read
 *                buff             data buffer for output
 * Return Value:  NAND_SUCCESS.
 *                NAND_FAILURE.
 * Description:   Function loads data buffer in internal flash
 */
int nand_program_execute(NAND_CHIP * nand_chip, uint32_t page_addr)
{
    uint8_t status;
    len_addr = 3;
    len_dummy = 0;
    status = spi_flash_write(&spi_flash, page_addr, 0, 0, NAND_CMD_PROGRAM_EXEC);
    return nand_wait_for_flash_ready(nand_chip, NAND_PROG_EXEC_TIMEOUT);
}

/*
 * Function:      Nand reset
 * Arguments:	  nand_chip, pointer to an nand_chip structure of SPI NAND device.
 * Return Value:  NAND_SUCCESS.
 *                NAND_FAILURE.
 * Description:   This function is for resetting the read/program/erase operation.
 */
int nand_reset(NAND_CHIP * nand_chip)
{
    len_addr = 0;
    len_dummy = 0;
    return spi_flash_write(&spi_flash, 0, 0, 0, NAND_CMD_RESET);
}



