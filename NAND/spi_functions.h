/*
   Date 20-03-2019
   File - SPI_Functions
   Description - Uniform interface file to call SPI funtions.
 */

#ifndef SPI_FUNCTIONS_H
#define SPI_FUNCTIONS_H

#include "app_error.h"
#include "boards.h"
#include "nrf_delay.h"
#include "nrf_drv_spi.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"


// Private defines
#define EX_SIZE 30
#define NAND_PAGE_SIZE 4224
#define NAND_BUFFER_SIZE 4254 // nand_page_size + ex_size
#define SPI_INSTANCE 0        /**< SPI instance index. */

// SPI PINS
#define FLASH_SPI_SS_PIN    10 //10
#define FLASH_SPI_MISO_PIN  8//7 //7
#define FLASH_SPI_MOSI_PIN  9 //8
#define FLASH_SPI_SCK_PIN   7//8 //9
#define LED_PIN_BLUE        23
#define FLASH_HOLD 6
#define FLASH_SO2  NRF_GPIO_PIN_MAP(1, 11)

// Private Variables
extern uint8_t len_addr, len_dummy, len_cmd;
static uint8_t read_buffer[NAND_BUFFER_SIZE];
static uint8_t write_buffer[NAND_BUFFER_SIZE];

extern const nrf_drv_spi_t spi_flash;     /**< SPI instance. */
extern volatile bool spi_xfer_done; /**< Flag used to indicate that SPI instance
                                        completed the transfer. */
#define SPI_MAX_DATA_SIZE 255
enum
{
    SUCCESS,
    FAILURE,
};

/*
  Function prototypes
 */

ret_code_t spi_flash_write(nrf_drv_spi_t const * const _spi, uint32_t addr, uint32_t byte_count,
    uint8_t * wr_buff, uint8_t wr_cmd);
ret_code_t spi_flash_read(nrf_drv_spi_t const * const _spi, uint32_t addr, uint32_t byte_count,
    uint8_t * rd_buff, uint8_t rd_cmd);
void spi_flash_init();

#endif